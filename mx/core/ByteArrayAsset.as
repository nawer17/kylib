﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package mx.core
{
	import flash.utils.ByteArray;
	import KYLib.TePo.ByteLevel.ConjuntoBytes;
	
	[ExcludeClass]
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ByteArrayAsset es usada para poder implementar la etiqueta Embed en variables.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle Grafe
	 */
	public class ByteArrayAsset extends ConjuntoBytes 
	{
		
		/**
		 * Constructor
		 */
		public function ByteArrayAsset(byteArray:ByteArray=null) 
		{
			super(byteArray);
			
		}
		
		
	}
}