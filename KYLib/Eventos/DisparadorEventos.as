﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Eventos 
{
		import KYLib.Errores.KYlibError;
		import KYLib.Interfaces.iEventoError;
		import flash.events.ErrorEvent;
		import flash.events.Event;
		import flash.events.EventDispatcher;
		import flash.events.IEventDispatcher;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase DisparadorEventos
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public class DisparadorEventos extends EventDispatcher implements IEventDispatcher 
	{
		
		/**
		 * 
		 * @param	target
		 */
		public function DisparadorEventos(target:IEventDispatcher = null) 
		{
			super(target)
		}
		
		/* INTERFACE flash.events.IEventDispatcher */
		
		
		/*public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void 
		{
			
		}*/
		
		public function dispatchErrorEvent(event:ErrorEvent):Boolean
		{
			if (hasEventListener(event.type)) 
			{
				return dispatchEvent(event);
			}
			else 
			{
				KYlibError.lanzar(new KYlibError("Evento de error no controlado.\nKYlibError#" + event.errorID +": " + event.text, event.type, 441));
				return false;
			}
		}
		
		/*public function hasEventListener(type:String):Boolean 
		{
			
		}*/
		
		/*public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void 
		{
			
		}*/
	}
}