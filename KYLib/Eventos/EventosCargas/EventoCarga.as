﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.Eventos.EventosCargas
{
	import flash.events.*;

	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase EventoCarga define un evento que es distribuido por las clases que cargan archivos externos y contiene dos constantes de tipo de eventos:
	 *
	 * <ul>
	 * <li><code>EventoCarga.CARGACOMPLETA</code>: Evento de tipo "CargaCompletada", disparado cuando se ha completado totalmente la carga de los archivos.</li>
	 * <li><code>EventoCarga.PROGRESO</code>: Evento de tipo "ProgresoCargaDeBytes", disparado cuando se carga un nuevo grupo de bytes del archivo o archivos que se estan cargando.</li>
	 * </ul>
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 */
	public class EventoCarga extends Event
	{
		/**
		 * Tipo de evento que es disparado cuando es cargado totalmente un archivo o un grupo de archivos, ya sea del dispositivo o de un servidor.
		 *
		 * <span>Se recomienda agregar un detector de este tipo de evento a las clases que cargan multiples archivos ya que cuando este es disparado significa que es posible acceder a los archivos sin ningun error.</span>
		 *
		 * <p>Cuando un evento EventoCarga es de tipo CargaCompletada la propiedad <code>bytesCargados</code> sera igual a la propiedad <code>bytesTotales</code> y la propiedad <code>porcentaje</code> es igual a 100.</p>
		 */
		public static const CARGACOMPLETA: String = "CargaCompletada";

		/**
		 * Tipo de evento que es disparado cuando es cargado un nuevo grupo de bytes del archivo o archivos que se estan cargando, ya sea del dispositivo o de un servidor.
		 *
		 * <span>Si se quiere supervisar el proceso de carga de datos es bueno agregar un detector de evento de este tipo para saber cuanta parte ha sido cargada ya y cuanta parte falta.</span>
		 */
		public static const PROGRESO: String = "ProgresoCargaDeBytes";

		/**
		 * Crea un nuevo evento de tipo EventoCarga.
		 *
		 * <span>Al crea un nuevo evento debe de especificar el tipo de evento que sera, puede escoger un tipo entre estos dos:</span>
		 *
		 * <ul>
		 * <li>"CargaCompletada": Para elegir este tipo se debe de pasar como valor al parametro type el valor de la propiedad estatica <code>CARGACOMPLETA</code>,Cuando un evento EventoCarga es de tipo CargaCompletada la propiedad <code>bytesCargados</code> sera igual a la propiedad <code>bytesTotales</code> y la propiedad <code>porcentaje</code> es igual a 100.</li>
		 * <li>"ProgresoCargaDeBytes": Para elegir este tipo se debe de pasar como valor al parametro type el valor de la propiedad estatica <code>PROGRESO</code>,Cuando un evento EventoCarga es de tipo ProgresoCargaDeBytes la propiedad <code>bytesCargados</code> sera el numero de bytes que se han cargado hasta el momento y la propiedad <code>porcentaje</code> es igual a el porcentaje de cuantos bytes han sido cargados.</li>
		 * </ul>
		 *
		 * @param type El tipo de evento que quiere crear.
		 * @param BytesTotal El numero total de bytes que seran cargados.
		 * @param BytesCargados El numero de bytes que han sido cargados hasta el momento.
		 * @param bubbles
		 * @param cancelable Indica si se puede cancelar el evento o no.
		 */
		public function EventoCarga(type: String, BytesTotal: uint, BytesCargados: uint, bubbles: Boolean = false, cancelable: Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		/**
		 * Obtiene el numero de bytes que han sido cargados del archivo.
		 *
		 * <span>Cuando el valor es igual al de la propiedad <code>bytesTotales</code> es porque ya han sido cargados correctamente todos los bytes.</span>
		 */
		public function get bytesCargados(): uint
		{
			return 0;
		}

		/**
		 * Obtiene el numero total de bytes que hay por cargar.
		 */
		public function get bytesTotales(): uint
		{
			return 0;
		}

		/**
		* Obtiene el porcentaje de carga actual.
		*
		* <span>El valor de porcentaje se consigue dividiendo el valor de <code>bytesCargados</code> entre el valor de <code>bytesTotales</code> y multiplicandolo por 100, por lo que cuando el valor es 100 es porque ya se han cargado todos los bytes.</span>
		*/
		public function get porcentaje(): uint
		{
			return 0;
		}

		/**
		* Devuelve una cadena con las propiedades del evento.
		*/
		public override function toString(): String
		{
			var Dev: String;
			Dev = String("[Event type=" + type +
				" bubbles=" + bubbles +
				" cancelable=" + cancelable +
				" eventPhase=" + eventPhase +
				" bytesCargados=" + bytesCargados +
				" bytesTotales=" + bytesTotales +
				"]");
			return Dev;
		}
	}
}