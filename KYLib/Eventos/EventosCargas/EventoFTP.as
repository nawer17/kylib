﻿/*
 * Copyright (C) 2020 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Eventos.EventosCargas 
{
	import KYLib.C.Obj.existe;
	import KYLib.TePo.Carga.Internet.FTPUtil.SesionFTP;
	import flash.events.Event;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase EventoFTP
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 32
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC 2020
	 *
	 * @author Juan Pablo Calle
	 */
	public class EventoFTP extends Event 
	{
		/**
		 * Evento disparado cuando se optiene el listado de un directorio de la sesión
		 */
		public static const DIRECTORIO_LISTADO:String = "directorioListado";
		
		/**
		 * Constructor
		 */
		public function EventoFTP(Tipo:String,Sesion:SesionFTP)
		{
			super(Tipo);
			$sesion = Sesion;
		}
		
		private var $sesion:SesionFTP;
		
		public function get sesion():SesionFTP 
		{
			return $sesion;
		}
	}
}