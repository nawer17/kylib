﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.Eventos.TePo.EventosExteriores
{
	import KYLib.TePo.Exterior.*;
	import flash.events.*;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase EventoCMD son eventos distribuidos por la clase CMD cuando ocurre una respuesta de la consola de windows, cuando esta acaba con su trabajo o cualquier cambio que ocurra.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 *
	 * @see KYLib.TePo.Exterior.CMD
	 */
	public final class EventoCMD extends Event
	{
		
		/**
		 * Crea un nuevo evento del CMD
		 */
		public function EventoCMD(tipo:String, burbujas:Boolean = false, cancelable:Boolean = false, ... parametrosExtra)
		{
			switch (tipo)
			{
				case EventoCMD.SALIDA: 
				{
					$data.code = parametrosExtra[1];
				}
				case EventoCMD.RESPUESTA: 
				{
					$data.msg = parametrosExtra[0];
					break;
				}
			}
			super(tipo, burbujas, cancelable);
		}
		
		/// Contiene la data del evento		
		private const $data:Object = {"msg": null, "code": -1};
		
		/**
		 * Contiene una cadena de texto que contiene una respuesta del CMD.
		 *
		 * <p>Cuando el evento es de tipo <code>RESPUESTA</code> esta propiedad tiene el contenido de la ultima respuesta obtenida del cmd, si el tipo de evento es <code>SALIDA</code> esta propiedad contiene una concatenacón de todas las respuestas que se obtuvieron del CMD.</p>
		 */
		public function get respuesta():String
		{
			return $data.msg;
		}
		
		/**
		 * Indica el codigo de salida que se obtuvo al acabar la ejecucion del CMD.
		 * <span>Si el codigo de salida es 0 es porque se pudieron ejecutar todos los comandos sin ningun problema cualquier otro valor superior a 0 es porque ocurrio un error al ejecutar el programa.</span>
		 *
		 * <p>Cuando el tipo de evento es distinto a <code>SALIDA</code> esta propiedad tiene el valor -1.</p>
		 */
		public function get codigoSalida():int 
		{
			return $data.code;
		}
		
		/**
		 * Tipo de evento que es disparado cuando el comando completo de la consola ha sido construido y puede ser ejecutado.
		 */
		public static const CONSTRUIDO:String = "EventoCMD.CONSTRUIDO";
		
		/**
		 * Tipo de evento que es disparado cuando se obtiene una respuesta del CMD.
		 */
		public static const RESPUESTA:String = "EventoCMD.RESPUESTA";
		
		/**
		 * Tipo de evento que es disparado cuando el CMD es cerrado.
		 */
		public static const SALIDA:String = "EventoCMD.SALIDA";
		
		/**
		 * Conevierte el evento en su representación textual.
		 */
		override public function toString():String 
		{
			switch (type) 
			{
				case EventoCMD.SALIDA:
				{
					return formatToString("EventoCMD", "type", "bubbles", "cancelable", "eventPhase", "codigoSalida", "respuesta");
					
					break;
				}
				case EventoCMD.RESPUESTA:
				{
					return formatToString("EventoCMD", "type", "bubbles", "cancelable", "eventPhase", "respuesta"); 
					break;
				}
			}
			return formatToString("EventoCMD", "type", "bubbles", "cancelable", "eventPhase");
		}
		
	}
}