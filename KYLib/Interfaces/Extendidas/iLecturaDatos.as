﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
package KYLib.Interfaces.Extendidas 
{
	import flash.utils.IDataInput;
	
	/**
	 * La interfaz iLecturaDatos contiene metodos que pueden ser implementados para la lectura de una variedad un poco mas amplia de tipos de datos y numeros que los que estan por defecto en la interfaz IDataInput, de la cual esta se extiende.
	 * 
	 * @author Tabatha(kokopai)
	 */
	public interface iLecturaDatos extends IDataInput
	{
		/**
		 * Lee unentero de 24 bit con signo del flujo de archivos, fujo de bytes o un conjunto de bytes.
		 * 
		 * @return	Un entero con signo de 24 bits en el rango de -8388608 hasta 8388607.
		 * 
		 * @throws	EOFError No hay suficientes datos disponibles para leer.
		 */
		function readMedium () : int;
		
		/**
		 * Lee unentero de 24 bit sin signo del flujo de archivos, fujo de bytes o un conjunto de bytes.
		 * 
		 * @return	Un entero sin signo de 24 bits en el rango de 0 hasta 16777215.
		 * 
		 * @throws	EOFError No hay suficientes datos disponibles para leer.
		 */
		function readUnsignedMedium () : uint;
		
		/**
		 * lee un byte del flujo de archivos, fujo de bytes o un conjunto de bytes y devuelve un conjunto con los 8 bits almacenados en el byte. 
		 * 
		 * @return Un conjunto con los 8-bits leidos
		 * 
		 * @throws	EOFError No hay suficientes datos disponibles para leer.
		 */
		function read8Bits () : Array;
	}
	
}