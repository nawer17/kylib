﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.C.Utils 
{
		import KYLib.C.Mat.num;
		import flash.display.Sprite;
		import flash.events.Event;
		import flash.events.TimerEvent;
		import flash.utils.Timer;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ContadorFPS
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class ContadorFPS
	{
		
		/**
		 * Constructor
		 */
		public function ContadorFPS(base:Sprite, fpsPromLimite:uint = 10) 
		{
			$limite = fpsPromLimite;
			base.addEventListener(Event.ENTER_FRAME, onBaseEnterFrame);
			$timer.addEventListener(TimerEvent.TIMER, on$timerTimer);
			$timer.start();
		}
		
		/// el numero de entradas de fps para sacar promedio
		private var $limite:uint = 10;
		
		/// guarda los fps
		public var fps:uint = 60;
		
		/// guarda el promedio de los ultimos fps
		public var fpsProm:Number;
		
		/// guarda los ultimos valores de fps
		private const $fpsArr:Array = [0];
		
		/// guarda el indice donde se guarda el valor actual
		private var $index:uint = 0;
		
		/// se ejecuta cada que pasa un segundo
		private function on$timerTimer(e:TimerEvent):void 
		{
			fps = $ticks;
			$ticks = 0;
			$index = $index >= $limite ? 0 : $index;
			$fpsArr[$index] = fps;
			$index++;
			fpsProm = num.fixed(num.prom.apply(null, $fpsArr), 2);
		}
		
		/**
		 * Devuelve el numero de registros de fps al que se le estan sacando promedio en el momento.
		 */
		public function get regContados():uint
		{
			return $fpsArr.length;
		}
		
		public var fpsContados:uint = 0;
		
		/// el timer que se usa para contar
		private const $timer:Timer = new Timer(1000, Infinity);
		
		/// guarda el numero de fotogramas que se dieron en un segundo
		private var $ticks:uint = 0;
		
		private function onBaseEnterFrame(e:Event):void 
		{
			$ticks++;
			fpsContados++;
		}
		
		
	}
}