﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.C.Mat 
{
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase fAlgebra
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class fAlgebra 
	{
		/**
		 * Aplica la formula general del estudiante y devuelve un arrglo con los dos valores
		 */
		public function formulaGeneral(a:Number, b:Number, c:Number):Array 
		{
			return [formulaGeneralPos(a, b, c), formulaGeneralNeg(a, b, c)];
		}
		
		/**
		 * Devuelve el resultado de tomar la parte negativa en la formula general del estuadiante
		 */
		public function formulaGeneralNeg(a:Number, b:Number, c:Number):Number 
		{
			return ((( -b) - num.raiz(num.pot(b) - (4 * a * c))) / (2 * a));
		}
		
		/**
		 * Devuelve el resultado de tomar la parte positiva en la formula general del estuadiante
		 */
		public function formulaGeneralPos(a:Number, b:Number, c:Number):Number 
		{
			return ((( -b) + num.raiz(num.pot(b) - (4 * a * c))) / (2 * a));
		}
		
		
	}
}