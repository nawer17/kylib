﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

/**
 * Version: 2.7.88
 */
package KYLib.C.Mat
{
	/**
	 * La clase Random contiene solamente un metodo y es utilizado para obtener un numero al azar en un rango especificado.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 *  @productversion Animate CC
	 */
	public final class random
	{
		/**
		* Genera un numero al azar basado en un rango especificado,ademas se puede pasar el tipo de valor que quiere que se retorne.
		*
		* @param Base El numero minimo que puede tener el valor de retorno.
		* @param Limite El numero maximo que puede llegar a ser el valor retornado.
		* @param tipo EL tipo de valor que se quiere que se devuelva,los tipos disponibles estan definidos en variables de esta clase.
		* 
		* <p>Si se especifica un tipo no valido se retorna siempre 0, y si se especifica <code>null</code> se establece el valor de tipo a <a href="./APROXIMADO">APROXIMADO</a>.</p>
		*
		* @return El numero al azar del tipo especificado.
		*/
		public static function generar(Base:int = -2147483646,Limite:Number = 2147483646,tipo:String = null):Number
		{
			//variable que sera devuelta
			var Dev:Number;
			
			//variable de la diferencia de base/limite
			var L:Number = Limite-Base;
			
			//retorna un numero aproximado
			if(tipo == APROXIMADO || tipo == null)
			{
				
				var Floor:Number = Math.floor((Math.random()*(L+1))+Base);
				
				Dev = Floor;
			}
			else if (tipo == random.INT)//retorna un entero
			{
				var INT:int = (Math.random()*(L+1))+Base;
				
				Dev = INT;
			}
			else if(tipo == NUMBER)//retorna un numero de punto flotante
			{
				var Num:Number = (Math.random()*(L+1))+Base;
				
				Dev = Num;
			}
			else//si el tipo es desconocido retorna 0
			{
				Dev = 0;
			}
			
			if(Dev > Limite)
			{
				Dev = Limite;
			}
			return Dev;
		}
		
		/**
		* Define que la función <a href="./Get()">Get()</a> retornara un valor aproximado al numero entero mas cercano.
		*
		* <p>Suponga que el numero al azar es 3.41, este numero es aproximado al numero mas cercano que en este ejemplo seria el 3; Siempre que los decimales sean mayores a .5 el numero sera llevado al numero siguiente,es decir, si el numero al azar es 6.58 entonces el valor devuelto sera 7 y asi con todos los resultados.</p>
		*/
		public static function get APROXIMADO():String { return "_get:Aprox" }
		
		/**
		* Define que la función <a href="./Get()">Get()</a> retornara un valor aproximado al numero entero mas cercano hacia abajo.
		*
		* <p>Suponga que el numero al azar es 4.25, el numero es llevado hacia el numero mas cercano hacia abajo entonces en este ejemplo seria el 4; Sin importar los decimales el numero siempre sera llevado hacia abajo,es decir, si el numero al azar es 6.89 entonces el valor devuelto sera 6 y asi con todos los resultados.</p>
		*/
		public static function get INT():String { return "_get:Int" }
		
		/**
		* Define que la función <a href="./Get()">Get()</a> retornara un valor de punto flotante.
		*
		* <p>En este tipo de resultado el numero no es aproximado de ninguna forma, por lo tanto se devuelve el valor tal cual es obtenido al azar,es decir, se devuelve un numero con decimales.</p>
		*/
		public static function get NUMBER():String { return "_get:Num" }
		
}}