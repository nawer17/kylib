﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.C.Mat 
{
	import KYLib.C.Utils.Opciones;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase vector representa un vector numerico usado para hacer operaciones entre vectores
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public dynamic final class vector extends Array 
	{
		private static var $defaultOpciones:Opciones = new Opciones({
			"usarMenor":false
		});
		
		private var $dimension:uint = 2;
		/**
		 * Devuelve la dimension del vector
		 */
		public function get dimension():uint 
		{
			return $dimension;
		}
		
		/**
		 * crea un nuevo vector con ciertos parametros
		 */
		public function vector(Dimension:uint,...componentes) 
		{
			$dimension = Dimension;
			for each (var i:Number in componentes) 
			{
				push(i);
			}
			while (length < $dimension) 
			{
				push(0);
			}
		}
		
		public function sum(B:vector):vector
		{
			return this;
		}
		
		public static function sum(...vectores):vector
		{
			var $opciones:Opciones = new Opciones(vectores[vectores.length - 1]);
			
			vectores.push($opciones.dimension)
			
			var $dimension:uint = getDimension(vectores);
			
			var $dev:vector = new vector($dimension);
			
			var $vectores:Vector.<vector> = new Vector.<vector>();
			
			for each (var $vect in vectores)
			{
				if ($vect is vector)
				{
					if ($opciones.completar) 
					{
						$vectores.push($vect.completar($dimension));
					}
					else 
					{
						if ($vect.dimension >= $dimension) 
						{
							$vectores.push($vect);
						}
					}
				}
			} 
			
			trace($vectores);
			trace($dev);
			trace($dimension);
			trace($opciones);
			
			return $dev;
		}
		
		///obtiene la dimension como el ultimo parametro o el tamaño del vector mas grande
		private static function getDimension(vectores:Array):uint 
		{
			var $dev:uint = 0;
			
			if (vectores[vectores.length-1] is Opciones) 
			{
				return vectores[vectores.length - 1].obt("dimension");
			}
			
			for each (var $vect in vectores) 
			{
				if ($vect is vector) 
				{
					$dev = $vect.dimension <= $dev ? $dev : $vect.dimension;
				}
			} 
			
			return $dev;
		}
		
		public static function max(...vectores):uint 
		{
			
		}
		
		/*
		 * 
		 * Metodos sobreescritos
		 * 
		 */
		
		/**
		 * Metodo no implementado por esta clase
		 */
		override AS3 function concat(...rest):Array 
		{
			return [];
		}
	}
}