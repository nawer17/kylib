﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.C.Mat
{
	import KYLib.C.*;
 
 	import KYLib.C.Mat.*;	
 	import KYLib.Errores.*;	
	import KYLib.Errores.ErroresClases.ErrorClaseAbstracta;
 	import KYLib.Eventos.EventosCargas.*;	
 	import KYLib.Interfaces.Extendidas.*;	
 	import KYLib.TePo.Archivos.*;	
 	import KYLib.TePo.ByteLevel.*;	
 	import KYLib.TePo.ByteLevel.Codificacion.*;	
 	import KYLib.TePo.ByteLevel.Constantes.*;	
 	import KYLib.TePo.Carga.*;	
 	import KYLib.TePo.Exterior.*;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase num ofrece metodos matematicos que se pueden usar para travajar con numeros simples.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 *  @productversion Animate CC
	 */
	public class num
	{
		/**
		 * @private
		 */
		public function num():void 
		{
			ErrorClaseAbstracta.agregar(this);
			super();
		}
		
		public static var fixFactor:uint = 8;
		
		include "C:/Users/usuario/Desktop/Mis clases As3/KYLib/C/Mat/Numeros Primos.as";
		
		/**
		 * 
		 * @param	...numeros
		 * @return
		 */
		public static function prom(...numeros):Number 
		{
			var Dev:Number = 0;
			for each (var i:Number in numeros) 
			{
				Dev += Number(i);
			}
			Dev /= numeros.length;
			return Dev;
		}
		
		/**
		 * Devuelve la logaritmación con base 10 de un numero.
		 *
		 * @param numero El numero del cual se quiere obtener la potencia diez.
		 *
		 * @return La potencia diez del numero dado.
		 */
		public static function potenciaDiez(numero: Number): int
		{
			//la variable que guarda el valor que se devolvera
			var Dev: int;

			//primero se mira si el numero esta en el intervalo (-10,-0.1)U(0.1,10) en cuyo caso el el valor devuelto se pondra en 0
			if ((numero > -10 && numero < -0.1) || (numero >= 0.1 && numero < 10))
			{
				Dev = 0;
			}
			else //esto pasa si el numero no esta en el intervalo (-10,10)
			{
				//se mira si el numero esta exponenciado
				if (numero > 1e+21 || numero < -1e+21)
				{
					//en caso de estar reducido el numero se separa en dos partes: el valor base y el valor de exponente, y se le pone el valor del exponente a la variable de devolucion
					Dev = String(numero).split("e+")[1];
				}
				//se mira si el numero esta exponenciado
				else if (numero > -1e-6 && numero < 1e-6)
				{
					//en caso de estar reducido el numero se separa en dos partes: el valor base y el valor de exponente, y se le pone el valor del exponente a la variable de devolucion
					Dev = -String(numero).split("e-")[1];
				}
				else //esto pasa si el numero no esta reducido
				{
					//si el numero es mayor o igual a 10 y menor a 1e+21
					if ((numero >= 10 && numero < 1e+21) || (numero > -1e+21 && numero <= -10))
					{
						//se mira la longitud de caracteres que contiene el numero si establece ese numero menos 1 a la variable de devolucion
						Dev = String(valorAbsoluto(fixed(numero, 0))).length - 1;
					}
					else if (numero >= -0.1 && numero <= 0.1) //si el numero es mayor a 1e-6 y menor o igual a 0.1
					{
						if (numero >= 0.000001)
						{
							Dev = -6;
						}
						if (numero >= 0.00001)
						{
							Dev = -5;
						}
						if (numero >= 0.0001)
						{
							Dev = -4;
						}
						if (numero >= 0.001)
						{
							Dev = -3;
						}
						if (numero >= 0.01)
						{
							Dev = -2;
						}
						if (numero >= 0.1)
						{
							Dev = -1;
						}
					}
				}
			}
			//se devuelve el valor
			return Dev;
		}

		/**
		 * Disminuye la cantidad de decimales de un numero reducido.
		 *
		 * <span>La cantidad de decimales se disminuye y el ultimo decimal que queda se acerca al valor mas cercano. Para que esta función sirva el numero pasado debe de estar en el intervalo (-infinito,-1e+21]U(-1e-6,1e-6)U[1e+21,infinito) ya que este intervalo es que el compilador de flash reduce los numeros con decimales en el valor de la base.</span>
		 *
		 * @param numero El numero al que se le quieren disminuir los decimales.
		 * @param Fix La cantidad de decimales que quiere que el numero tenga, si valor dado es mayor la cantidad de decimales que tiene actualmente el numero la función no tendra efecto.
		 *
		 * @return El numero reducido con los decimales disminuidos.
		 */
		public static function fixedEx(numero: Number, Fix: uint = 5): Number
		{
			//variable a devolver
			var Dev: Number;
			//se mira si el numero esta exponenciado positivamente
			if (numero >= 1e+21 || numero <= -1e+21)
			{
				//se separa el numero convertido en cadena en 2 partes:la base y la exponencia, luego se fixea solo la parte de la base y se le agrega la parte de exponencia
				Dev = Number(fixed(Number(String(valorAbsoluto(numero)).split("e+")[0]), Fix) + "e+" + String(numero).split("e+")[1]);
			}
			else if (numero > -1e-6 && numero < 1e-6) //se mira si el numero esta exponenciado negativamente
			{
				//se separa el numero convertido en cadena en 2 partes:la base y la exponencia, luego se fixea solo la parte de la base y se le agrega la parte de exponencia
				Dev = Number(fixed(Number(String(valorAbsoluto(numero)).split("e-")[0]), Fix) + "e-" + String(numero).split("e-")[1]);
			}
			else
			{
				Dev = numero;
			}
			//se devuelve el valor
			return numero < 0 ? Dev * -1 : Dev;
		}

		/**
		 * Disminuye la cantidad de decimales de un numero.
		 *
		 * <span>La cantidad de decimales se disminuye y el ultimo decimal que queda se acerca al valor mas cercano.</span>
		 *
		 * @param numero El numero al que se le quieren disminuir los decimales.
		 * @param Fix La cantidad de decimales que quiere que el numero tenga, si valor dado es mayor la cantidad de decimales que tiene actualmente el numero la función no tendra efecto.
		 *
		 * @return El numero con los decimales disminuidos.
		 */
		public static function fixed(numero: Number, Fix: uint = 5): Number
		{
			return Fix != 0 ? Number(numero.toFixed(Fix)) : numero;
		}
		
		/**
		 * Devuelve la cadena resultante al reducir un numero.
		 *
		 * <p>Para reducir un numero este debe de estar en el intervalo de (-1e+21,-1e-6]u[1e-6,1e+21) debido a que si esta fuera de este intervalo el compilador de flash lo reduce automaticamente.</p>
		 *
		 * @param numero El numero que quiere ser reducido.
		 * @param factorReductor Indica a partir de que numero se reducira, por ejemplo si el factor es 7 se reduce el numero si su potencia diez es mayor o igual a 7, pero si el numero es -3 se reduce el numero si su potencia diez es menor o igual a -3, es decir, si se pasa un valor positivo se mira la potencia diez mayor pero si es negativo se pasa la potencia diez negativa. El rango de valores que puede tener este parametro es de -6 hasta 20.
		 * @param Fix La cantidad de decimales que quiere que tenga el numero reducido
		 */
		public static function reducir(numero: Number, factorReductor: int = 0, Fix: uint = 5): String
		{
			//variable a devolver
			var Dev: String;

			//se mira si el numero esta exponenciado
			if (String(numero).search("e") > -1)
			{
				//en caso de ya estar exponenciado solo toca fixear
				Dev = String(fixedEx(numero, Fix));
			}
			else //en caso de no estar exponenciado se tiene que exponenciar
			{
				//se mira que cumpla con el factor de reduccion
				if (factorReductor > 0 && factorReductor <= 20)
				{
					if (potenciaDiez(numero) >= factorReductor)
					{
						//si se cumple el factor de reduccion entonces se reduce el numero y se fixea
						Dev = fixedExStr(numero.toExponential(Fix), Fix);
					}
					else //si no se cumple el factor de reduccion se devuelve el mismo numero
					{
						Dev = String(numero);
					}
				}
				else if (factorReductor < 0 && factorReductor >= -6) //se mira que cumpla con el factor de reduccion
				{
					if (potenciaDiez(numero) <= factorReductor)
					{
						//si se cumple el facotr de reduccion entonces se reduce el numero y se fixea
						Dev = fixedExStr(numero.toExponential(Fix), Fix);
					}
					else //si no se cumple el factor de reduccion se devuelve el mismo numero
					{
						Dev = String(numero);
					}
				}
				else //en caso de no haber factor de reduccion se reduce el numero fixeado
				{
					Dev = fixedExStr(numero.toExponential(Fix), Fix);
				}
			}

			//devuelve la variable
			return Dev;
		}

		/**
		 * @private
		 * Fixea un numero reducido pero en cadena.
		 */
		private static function fixedExStr(numero: String, Fix: int): String
		{
			//la variable que se devolvera
			var Dev: String;

			//se fixea el numero de la base para que no tenga mas ceros de los que debe
			var N: Number = Number(numero) >= 1 ? fixed(numero.split("e+")[0], Fix) : fixed(numero.split("e-")[0], Fix);
			
			//se mira cual sera el separador
			var sep: String = Number(numero) >= 1 ? "e+" : "e-";
			
			//se guarda el valor de exponencia
			var exp: String = Number(numero) >= 1 ? numero.split("e+")[1] : numero.split("e-")[1];
			
			//se une todo
			Dev = String(N + sep + exp);
			
			//se devuelve la variable
			return Dev;
		}
		
		/**
		 * Devuelve el valor absoluto de un numero.
		 *
		 * <span>Practicamente esta funcion es igual a la funcion estatica <code>abs</code> de la clase Math, solo que esta es la función que se usa en la libreria KYLib ya que esta registrada como una clase del paquete Esencial.</span>
		 *
		 * @param numero El numero del cual se quiere obtener el valor absoluto.
		 *
		 * @return El valor absoluto del numero dado.
		 */
		public static function valorAbsoluto(numero: Number): Number
		{
			return Math.abs(numero);
		}
		
		/**
		 *	Devuelve un numero elevado a una potencia.
		 * 
		 * @param	numero El numero a elevar.
		 * @param	potencia La potencia a la que se eleva el numero.
		 */
		public static function pot(numero:Number, potencia:Number = 2):Number 
		{
			return Math.pow(numero, potencia);
		}
		
		/**
		 * Devuelve la raiz con un radical indicado de un numero.
		 *
		 * @param base El numero al que se le quiere sacar raiz.
		 * @param radical El radical es la raiz que se quiere sacar, si es 2 entonces sera raiz cuadrada, si es 3 entonces sera raiz cubica y asi sucesivamente. <span>Si se le pasa el valor 0 a este parametro entonces se devolvera tambien 0.</span>
		 */
		public static function raiz(base: Number, radical: Number = 2): Number
		{
			//si el radical es 0 entonces se devuelve 0
			if (radical == 0 || base == 0) { return 0; }
			
			if (radical == 2) { return Math.sqrt(base); }
			
			//se guarda el valor del inverso del radical
			var Rad: Number = 1 / radical;
			
			//si eleva el numero al inverso del radical, de esta forma se saca la raiz y luego se retorna
			return Math.pow(base, Rad);
		}
		
		/**
		 * Convierte un angulo en grados a radianes.
		 */
		public static function deg2rad(degs:Number):Number 
		{
			return (Math.PI * degs) / 180;
		}
		
		/**
		 * Convierte un angulo en radianes a grados.
		 */
		public static function rad2deg(rads:Number):Number 
		{
			return (180 * rads) / Math.PI;
		}
		
		public static function par(numero:int):Boolean 
		{
			return (numero % 2) == 0;
		}
		
		/**
		 * 
		 * @param	numero
		 * @return
		 */
		public static function esPrimo(numero:uint):Boolean 
		{
			for each (var j:uint in $primos) 
			{
				trace(65537 % j);
			}
			return false;
			var N:Number;
			if ($primos.indexOf(numero) >-1) 
			{
				return true;
			}
			if (numero % 2 == 0 || numero % 3 == 0 || numero % 5 == 0 || numero % 7 == 0 || numero % 11 == 0) 
			{
				return false;
			}
			else if ((N = num.raiz(numero)).toString().indexOf(".") == -1)
			{
				return false;
			}
			else 
			{
				for (var i:int = 5; i < 1000; i++) 
				{
					if (numero % $primos[i] == 0) 
					{
						return false;
					}
				}
				if (numero / primoMasCercano(Math.floor(N)) < N) 
				{
					return true;
				}
			}
			return false;
		}
		
		static private function primoMasCercano(numero:uint):uint 
		{
			if (numero > 65537) 
			{
				return 65537;
			}
			while ($primos.indexOf(numero) == -1)
			{
				numero--;
			}
			trace("c: " + numero);
			return numero;
		}
	}
}