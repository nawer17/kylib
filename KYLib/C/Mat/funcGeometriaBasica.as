﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.C.Mat 
{
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase GeometriaBasica
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class funcGeometriaBasica 
	{
		
		/**
		 * Haya el area de un triangulocon conociedo una de sus bases y la altura de esa base.
		 */
		public function areaTrianguloBA(Base:Number,Altura:Number):Number 
		{
			return (Base * Altura) / 2;
		}
		
		/**
		 * Halla el area de un triangulo del cual se conocen los tres lados con la formula de Herom.
		 * 
		 * @param	a Lado 1
		 * @param	b Lado 2
		 * @param	c Lado 3
		 * */
		public function areaTrinaguloH(a:Number,b:Number,c:Number):Number 
		{
			var s:Number = (a + b + c) / 2;
			
			return num.raiz(s * (s - a) * (s - b) * (s - c));
		}
		
		/**
		 * Calcula el area de un trapecio.
		 * 
		 * @param	B Base mayor
		 * @param	b Base menor
		 * @param	h Altura
		 */
		public function areaTrapecio(B:Number,b:Number,h:Number):Number 
		{
			return (B * h + b * h) / 2;
		}
		
		/**
		 * Aplica el teorema de pitagoras.
		 * 
		 * @param	a Si h es <code>false</code>: el valor de uno de los catetos; Si h es <code>true</code>: el valor de la hipotenusa.
		 * @param	b Si h es <code>false</code>: el valor del otro cateto; Si h es <code>true</code>: el valor del cateto que se conoce.
		 * @param	h Indica si se se esta hayando la hipotenusa(<code>false</code>) o no(<code>true</code>). En caso de no estar buscando la hipotenusa se esta buscando un cateto.
		 * 
		 * @return
		 */
		 
		public function teoPitagoras(a:Number, b:Number,h:Boolean = false):Number
		{
			if (h) 
			{
				return num.raiz(num.pot(a) - num.pot(b));
			}
			return num.raiz(num.pot(a) + num.pot(b));
		}
		
		
		
		public function teoCoseno(alpha:Number, b:Number, c:Number):Number 
		{
			return num.raiz(num.pot(b) + num.pot(c) - (2 * b * c * Math.cos(num.deg2rad(alpha))));
		}
		
		/*public function regla3Simple(a:Number, p:Number, q:Number, inversa:Boolean = false):Number 
		{
			if (inversa) 
			{
				return (q * p) / a;
			}
			return (p * a) / q;
		}*/
		
	}
}