﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.C.Mat 
{
	import KYLib.C.Obj.existe;
	
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase vects
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class vects 
	{
		/// El vector canonico i
		public const iV:Vector.<Number> = new <Number>[1,0,0];
		/// El vector canonico j
		public const jV:Vector.<Number> = new <Number>[0,1,0];
		/// El vector canonico k
		public const kV:Vector.<Number> = new <Number>[0,0,1];
		
		///vector con una respuesta 
		private var $v:Vector.<Number>;
		
		public function get v():Vector.<Number>
		{
			var $dev = $v;
			$v = null;
			return $dev;
		}
		
		public function vects():void
		{
			iV.fixed = true;
			jV.fixed = true;
			kV.fixed = true;
		}
		
		public function pXa(vect:Vector.<Number>):vects 
		{
			$v = pX($v, vect).v;
			
			return this;
		}
		
		public function pX(vect1:Vector.<Number>,vect2:Vector.<Number> = null):vects 
		{
			vect1 = completarVector(3, vect1);
			vect2 = completarVector(3, existe(vect2) ? vect2 : $v);
			
			$v = nVector(3,
			(vect1[1] * vect2[2]) - (vect1[2] * vect2[1]),
			(vect1[2] * vect2[0]) - (vect1[0] * vect2[2]),
			(vect1[0] * vect2[1]) - (vect1[1] * vect2[0])
			);
			
			return this;
		}
		
		public function proyA(B:Vector.<Number>):vects 
		{
			$v = ppue((pPunto($v, B)) / (pPunto(B, B)), B).v;
			
			return this;
		}
		
		public function proyB(A:Vector.<Number>, B:Vector.<Number> = null):vects 
		{
			var vecB = existe(B) ? B : $v;
			
			$v = ppue((pPunto(A, vecB)) / (pPunto(vecB, vecB)), vecB).v;
			
			return this;
		}
		
		public function ang(vect1:Vector.<Number>,vect2:Vector.<Number>= null, dimension:uint = 0):Number
		{
			var $dimension:uint = getDimension([vect1, existe(vect2) ? vect2 : $v, (dimension == 0 ? null : dimension)]);
			
			vect1 = completarVector($dimension, vect1);
			vect2 = completarVector($dimension, existe(vect2) ? vect2 : $v);
			
			return num.rad2deg(Math.acos(pPunto(vect1, vect2) / (norma(vect1) * norma(vect2))));
		}
		
		/**
		 * Calcula la distancia punto entre 2 vectores
		 */
		public function d(vect1:Vector.<Number>, vect2:Vector.<Number> = null, dimension:uint = 0):Number
		{
			$v = existe(vect2) ? vect2 : $v;
			
			var $dimension:uint = getDimension([vect1, $v, (dimension == 0 ? null : dimension)]);
			
			vect1 = completarVector($dimension, vect1);
			$v = completarVector($dimension, $v);
			
			return norma(sumres(false, [vect1, $v, dimension]), dimension);
		}
		
		/**
		 * Encuentra la magnitud o norma de un vector
		 */
		public function norma(vector:Vector.<Number> = null, dimension:uint = 0):Number 
		{
			return num.raiz(pPunto(existe(vector) ? vector : $v, existe(vector) ? vector : $v, dimension));
		}
		
		/**
		 * Calcula el producto punto entre 2 vectores
		 */
		public function pPunto(vect1:Vector.<Number>, vect2:Vector.<Number> = null, dimension:uint = 0):Number
		{
			var $dev:Number = 0;
			
			var $dimension:uint = getDimension([vect1, existe(vect2) ? vect2 : $v, (dimension == 0 ? null : dimension)]);
			
			vect1 = completarVector($dimension, vect1);
			vect2 = completarVector($dimension, existe(vect2) ? vect2 : $v);
			
			for (var i:int = 0; i < $dimension; i++) 
			{
				$dev += vect2[i] * vect1[i];
			}
			
			return $dev;
		}
		
		public function completarVector(dimension:uint, i:Vector.<Number>):Vector.<Number>
		{
			if (i.length >= dimension) 
			{
				return i;
			}
			i.fixed = false;
			i.length = dimension;
			i.fixed = true;
			return i;
		}
		
		/**
		 * Hace el producto por un escalar entre un vector y un escalar dado
		 * 
		 * @param	vector El vector a escalar
		 * @param	escalar El escalar
		 * @return El vector escalado
		 */
		public function ppue(escalar:Number = 1,vector:Vector.<Number> = null):vects
		{
			$v = existe(vector) ? vector : $v;
			
			for (var i in $v)
			{
				$v[i] *= escalar;
			}
			
			return this;
		}
		
		/**
		 * Mira si dos vectores son iguales.
		 */
		public function eq(vect1:Vector.<Number>, vect2:Vector.<Number> = null):Boolean 
		{
			$v = existe(vect2) ? vect2 : $v;
			
			if (vect1.length == $v.length) 
			{
				for (var i in vect1) 
				{
					if (!obj.iguales(vect1[i], $v[i]))
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}
		
		/**
		 * Suma un conjunto de vectores pasados.
		 * 
		 * @param	...vectores Un arreglo que contiene los vectores por sumar, los vectores o objetos no validos seran ignorados. Si el ultimo objeto pasado es un numero natural entonces este sera tomadas como la dimension del vector resultante.
		 * @return El vector resultante
		 */
		public function sum(...vectores):vects
		{
			vectores.unshift($v);
			$v = sumres(true, vectores);
			return this;
		}
		
		/**
		 * Resta un conjunto de vectores pasados.
		 * 
		 * @param	...vectores Un arreglo que contiene los vectores por restar, los vectores o objetos no validos seran ignorados. Si el ultimo objeto pasado es un numero natural entonces este sera tomadas como la dimension del vector resultante.
		 * @return El vector resultante
		 */
		public function res(...vectores):vects
		{
			if (existe($v)) 
			{
				vectores.unshift($v);
			}
			$v = sumres(false, vectores);
			return this;
		}
		
		/**
		 * Funcion interna para la suma y resta de vectores.
		 * 
		 * @param	signo es true si se va a sumar y false si se va a restar
		 * @param	vectores el arreglo de vectores por sumar
		 * @return el vector resultante
		 */
		private function sumres(signo:Boolean, vectores:Array):Vector.<Number> 
		{
			var $vectores:Vector.<Vector.<Number>> = new Vector.<Vector.<Number>>();
			
			var $dimension:uint = getDimension(vectores);
			
			var $dev:Vector.<Number> = new Vector.<Number>($dimension, true);
			
			for each (var $vect in vectores)
			{
				if ($vect is Vector.<Number>)
				{
					$vectores.push($vect);
				}
			} 
			
			$vectores = completarVectores($dimension, $vectores);
			
			if (!signo) 
			{
				for (var j:uint = 0; j < $dimension;j++ ) 
				{
					$dev[j] = 2 * $vectores[0][j];
				}
			}
			
			for (var i:int = 0; i < $dimension; i++) 
			{
				for each (var $componente in $vectores)
				{
					$dev[i] = signo ? $dev[i] + $componente[i] : $dev[i] - $componente[i];
				} 
			}
			
			return $dev;
		}
		
		private function completarVectores($dimension:uint, $vectores:Vector.<Vector.<Number>>):Vector.<Vector.<Number>> 
		{
			var $dev:Vector.<Vector.<Number>> = $vectores;
			for (var i in $vectores) 
			{
				$dev[i] = completarVector($dimension,$vectores[i]);
			}
			return $dev;
		}
		
		///obtiene la dimension como el ultimo parametro o el tamaño del vector mas grande
		private function getDimension(vectores:Array):uint 
		{
			var $dev:uint = 0;
			
			if (vectores[vectores.length-1] is uint) 
			{
				return vectores[vectores.length - 1];
			}
			
			for each (var $vect in vectores) 
			{
				if ($vect is Vector.<Number>) 
				{
					$dev = $vect.length <= $dev ? $dev : $vect.length;
				}
			} 
			
			return $dev;
		}
		
		public function toString():String 
		{
			return $v.toString();
		}
	}
}