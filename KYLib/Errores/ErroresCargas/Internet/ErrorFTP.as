﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Errores.ErroresCargas.Internet 
{
		import KYLib.Errores.KYlibError;
	import KYLib.Interfaces.iEventoError;
	import flash.events.ErrorEvent;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ErrorFTP
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class ErrorFTP extends KYlibError implements iEventoError 
	{
		
		/**
		 * 
		 * @param	Nombre
		 * @param	...parametrosExtra
		 */
		public function ErrorFTP(Nombre:String = "ErrorFTP", ...parametrosExtra) 
		{
			var Mensaje:String = "Ha ocurrido un error en la clase FTP";
			var id:uint = 73120;
			switch (Nombre) 
			{
				case ErrorFTP.SESION_CONEXIONFALLIDA:
				{
					Mensaje = String('No ha sido posible conectarse al servidor "' + parametrosExtra[0] + '" de la SesionFTP "' + parametrosExtra[1] + '", verifique que es posible conectarse al host y vuelva a intentarlo');
					id = 73121;
					break;
				}
				case ErrorFTP.SESION_ACCESODENEGADO:
				{
					Mensaje = String('No se ha podido acceder al servidor "' + parametrosExtra[0] + '" de la SesionFTP "' + parametrosExtra[1] + '" debido a que se ha denegado el acceso, verifique su usuario y contraseña antes de reintentarlo');
					id = 73122;
					break;
				}
			}
			
			super(Mensaje, Nombre, id);
		}
		
		/**
		 * Este tipo de error es un evento de error y es disparado cuando no es posible conectarse con el servidor FTP especificado en una SesionFTP.
		 */
		public static const SESION_CONEXIONFALLIDA:String = "ErrorConexionSesion";
		
		/**
		 * Este tipo de error es un evento de error y es disparado cuando el usuario y contraseña para iniciar en el servidor ftp no coinciden o no existe el usuario.
		 */
		public static const SESION_ACCESODENEGADO:String = "ErrorSesionDenegada";
		
		/* INTERFACE KYLib.Interfaces.iEventoError */
		
		/**
		 *  @copy KYLib.Interfaces.iEventoError#aEvento()
		 */
		public function aEvento():ErrorEvent 
		{
			return new ErrorEvent($tipo, false, false, message, errorID);
		}
		
		
		
	}
}