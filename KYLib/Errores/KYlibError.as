﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.Errores
{
	import KYLib.C.*;

	import KYLib.C.Mat.*;
	import KYLib.C.Utils.clases;
	import KYLib.Errores.*;
	import KYLib.Eventos.EventosCargas.*;
	import KYLib.Interfaces.Extendidas.*;
	import KYLib.TePo.Archivos.*;
	import KYLib.TePo.ByteLevel.*;
	import KYLib.TePo.ByteLevel.Codificacion.*;
	import KYLib.TePo.ByteLevel.Constantes.*;
	import KYLib.TePo.Carga.*;
	import KYLib.TePo.Exterior.*;
	import flash.events.*;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase KYlibError es usada como una clase base de todos los errores distribuidos por la libreria KYLib.
	 *
	 * <span>Puede crear un nuevo error y lanzarlo usando su metodo <code>Lanzar()</code> o pasando como parametro al metodo estatico de la clase KYlibError <code>Lanzar()</code>.</span>
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 */
	public class KYlibError extends Error
	{
		
		/**
		 * Crea una nueva instancia de error.
		 * <span>Para lanzar el error llame al metodo estatico KYlibError<code>.Lanzar()</code>.</span>
		 * 
		 * <p>Los parametros Nombre y id son proporcionados por las clases heredades en sus constructores.</p>
		 * 
		 * @param	Mensaje El mensaje que describe el error, este valor debe ser descriptivo de cada error.
		 * @param	Nombre El nombre del error, si extiende la clase KYlibError entonces debe ser el nombre la clase extendida.
		 * @param	id La id del error producido.
		 */
		public function KYlibError(Mensaje: String, Nombre: String, id: uint = 0)
		{
			super(Mensaje, parseInt(id.toString(), 8));
			
			$tipo = Nombre;
		}
		
		/**
		 * Dispara un error o algun otro objeto, es como usar la sentencia <code>throw</code>.
		 *
		 * @param objeto El error u objeto que quiere ser lanzado.
		 */
		public static function lanzar(objeto: *): void
		{
			throw objeto;
		}
		
		/**
		 * Llame a este metodo en el constructor de una clase singleton, es decir, que no puede ser instanciada porque solo puede tener una instancia.
		 * 
		 * @param	instancia La pseudo variable <code>this</code> de la clase desde la que se llama a este metodo.
		 */
		public static function ClaseSingleton(instancia:*):void 
		{
			lanzar(new KYlibError("La clase " + clases.nombreClaseObjeto(instancia) + " es una clase singleton y no puede ser instanciada", "ErrorClaseSingleton", 333));
		}
		
		/**
		 * El tipo de error.
		 * @private
		 */
		protected var $tipo: String;
		
		/**
		 * Convierte el error en una cadena que lo describe.
		 *
		 * @return El error convertido en cadena de texto.
		 */
		public function toString(): String
		{
			var Dev: String = String($tipo + ": " + "KYlibError#" + errorID + ": " + message + ".");
			return Dev;
		}
	}
}