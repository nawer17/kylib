﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Errores.TePo.ErroresExteriores 
{
		import KYLib.Errores.KYlibError;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ErrorCMD es la que tiene los errores distribuidos por la clase CMD.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 * 
	 * @see KYLib.TePo.Exterior.CMD Clase CMD
	 */
	public final class ErrorCMD extends KYlibError 
	{
		
		/**
		 * Crea un nuevo error del CMD.
		 */
		public function ErrorCMD(Tipo:String = "ErrorCMD") 
		{			
			var id:uint = 7421;
			var mensaje:String = "Ha ocurrido un error con un objeto CMD";
			
			switch (Tipo) 
			{
				case ErrorCMD.EJECUTANDO:
				{
					id = 7422;
					mensaje = "No se puede ejecutar un objeto CMD que ya se esta ejecutando en el momento";
					break;
				}
				case ErrorCMD.NODISPONIBLE:
				{
					id = 7423;
					mensaje = "No se puede ejecutar un objeto CMD que aun no esta construido";
					break;
				}
			}
			
			super(mensaje, Tipo, id);
		}
		
		/**
		 * Este tipo de error ocurre cuando se intenta llamar el metodo <code>ejecutar()</code> de una instancia de la clase CMD pero ya se habia llamadao antes y aun no se ha terminado de ejecutar.
		 * 
		 * <p>Para evitar que se produzca este error debe de asegurarse de que el proceso del CMD no se esta ejecutando en el momento que intenta llamarlo, esto lo hace con la propiedad <code>ejecutando</code> del objeto CMD</p>
		 * 
		 * @see KYLib.TePo.Exterior.CMD#ejecutar()
		 * @see KYLib.TePo.Exterior.CMD#ejecutando
		 */
		public static const EJECUTANDO:String = "ErrorEjecutandoCMD";
		
		/**
		 * Este tipo de error ocurre cuando se intenta ejecutar un CMD que no esta disponible aun, es decir, aun no esta construido o se esta construyendo en el momento.
		 * 
		 * <p>Para evitar que se produzca este error primero construya el CMD antes de intentar ejecutarlo y uses controladores de eventos para saber ene que momento ya se termino de construir el CMD, para verificiar mire la propiedad <code>disponible</code></p>
		 */
		public static const NODISPONIBLE:String = "ErrorCMDNoConstruido";
	}
}