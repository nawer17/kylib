﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Errores.ErroresClases 
{
		import KYLib.C.Utils.clases;
		import KYLib.Errores.KYlibError;
	
		import KYLib.C.esencial;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ErrorClaseAIR es usada para generar un error cuando se intenta instanciar una clase que debe ser ejecutada en AIR y que no esta siendo ejecutada en este entorno.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class ErrorClaseAIR extends KYlibError 
	{
		/**
		 * Constructor
		 */
		public function ErrorClaseAIR(Mensaje:String, Nombre:String, id:uint=0) 
		{
			super(Mensaje, Nombre, id);
		}
		
		/**
		 * Use este metodo en el constructor de una clase para indicar que es de AIR y que se produzca un error al intentar instanciarla.
		 * 
		 * @param	objeto La pseudo variable <code>this</code>.
		 */
		public static function agregar(objeto:*):void
		{
			if (esencial.enAIR) 
			{
				// si la aplicación se esta ejecutando en AIR entonces retorna.
				return;
			}
			KYlibError.lanzar(new KYlibError("La clase " + clases.nombreClaseObjeto(objeto) + " es una clase de uso exclusivo de AIR", "ErrorClaseAIR", 332));
		}
	}
}