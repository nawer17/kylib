﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Errores.ErroresClases 
{
		import KYLib.C.Utils.clases;
		import KYLib.Errores.KYlibError;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ErrorClaseAbstracta se usa para generar un error cuando se intente crear una clase que en realidad es abstracta.
	 * 
	 * <p>La manera correcto de generar un error es llamando al metodo estatico <code>agregar()</code> de esta clase dentro del constructor de la clase que se quiere que sea abstracta y pasar como parametro la variable <code>this</code> como se puede ver en este ejemplo:</p>
	 * 
	 * 
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class ErrorClaseAbstracta extends KYlibError 
	{
		/**
		 * @private
		 */
		public function ErrorClaseAbstracta()
		{
			ErrorClaseAbstracta.agregar(this);
			super("Clase Abstracta", "ErrorClaseAbstracta",331);
		}
		
		/**
		 * Use este metodo en el constructor de una clase para indicar que es abstracta y que se produzca un error al intentar instanciarla.
		 * 
		 * @param	objeto La pseudo variable <code>this</code>.
		 */
		public static function agregar(objeto:*):void
		{
			KYlibError.lanzar(new KYlibError("La clase " + clases.nombreClaseObjeto(objeto) + " es una clase abstracta y no puede ser instanciada", "ErrorClaseAbstracta", 331));
		}
		
	}
}