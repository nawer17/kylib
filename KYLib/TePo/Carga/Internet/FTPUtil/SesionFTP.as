﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Carga.Internet.FTPUtil 
{

	import KYLib.C.Obj.existe;
	import KYLib.C.Utils.GLOBAL;
	import KYLib.C.esencial;
	import KYLib.Eventos.DisparadorEventos;
	import KYLib.TePo.Carga.Internet.FTPUtil.*;
	import flash.events.*;
	
	/**
	 * Este evento se distribuye cuando no se puede encontrar el host del servidor.
	 */
	[Event(name = "ErrorConexionSesion", type = "KYLib.Errores.ErroresCargas.Internet.ErrorFTP")]
	
	/**
	 * Disparado cuando se obtiene la lista de archivos de un directorio
	 */
	[Event(name = "directorioListado", type = "KYLib.Eventos.EventosCargas.EventoFTP")]
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase SesionFTP provee metodos para crear un conjunto de intrucciones que se le pasaran a la clase ftp, con esta clase puede usar metodos para crear intrucciones de cambiar el directorio de trabajo remoto/local, subir archivos al servidor, descargar archivos, etc.
	 *
	 * <p>Se recomienda el siguiente orden de instrucciones aunque no es obligatorio:</p>
	 * 
	 * <ol>
	 * <li><code>cambiarDirectorio</code>: se recomienda primero que nada cambiar el directorio de trabajo remoto al cual se quiere trabajar.</li>
	 * </ol>
	 * 
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class SesionFTP extends DisparadorEventos
	{
		
		/// Esta cadena almacena todo el bloque de instrucciones
		private const $comandos:Vector.<String> = new Vector.<String>();
		
		/// Una concatenacion de todos los comandos
		private var $comando:String = "";
		
		/// Almacena el nombre de sesion
		private var $nombre:String;
		
		/// Almacena el servidor
		private var $servidor:ServidorFTP;
		
		/// Directorio
		public var listadoDirectorio:Array = [];
		
		/**
		 * 
		 * @param	servidor
		 * @param	nombre
		 */
		public function SesionFTP(Servidor:ServidorFTP, Nombre:String) 
		{
			//se guarda el nombre
			$nombre = Nombre;
			// se guarda el servidor
			$servidor = Servidor;
			// esto se usa para saber a que sesion se esta registrando
			anexarComando('echo KYLIBFTP={"Sesion":"' + $nombre + '","Comando":"open"}');
			// se agrega el comando que se conecta al servidor
			anexarComando(String("open " + Servidor.url));
		}
		
		/**
		 * Devuelve el servidor al que se conectara esta sesion
		 */
		public function get servidor():ServidorFTP
		{
			return $servidor;
		}
		
		/**
		 * Devuelve el nombre de esta sesion.
		 */
		public function get nombre():String
		{
			return $nombre;
		}
		
		/**
		 * Se usa para obtener los comandos desde la clase FTP
		 * @private
		 */
		public function getVector(plantilla:Object):Object
		{
			if (GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTPUtil.SesionFTP.plantilla === plantilla) 
			{
				$comando = "";
				for (var i:int = 0; i < $comandos.length; i++) 
				{
					$comando += $comandos[i];
					$comando += "\n";
				}
				return $comando;
			}
			return null;
		}
		
		public function comando(index:uint):String
		{
			
		}
		
		/**
		 * Agrega un comando a la lista
		 */
		private function anexarComando(Comando:String):void 
		{
			// se agrega el comando al vector
			$comandos.push(Comando);
		}
		
		/**
		 * Obtiene el listado de archivos y carpetas de un directorio remoto
		 * 
		 * @param	directorio	El directorio remoto, debe empezar con / y se puede usar el comodin *.
		 * Ejemplo:
		 * <code>SesionFTP.listarDirectorio("/public_html");</code> //lista el directorio public_html
		 * <code>SesionFTP.listarDirectorio("/public_html/webroot");</code> //lista el directorio webroot de la carpeta public_html
		 * <code>SesionFTP.listarDirectorio("/public_html/*.php");</code> //lista solo los archivos .php de la carpeta public_html
		 */
		public function listarDirectorio(directorio:String = "/"):void 
		{
			// esto se usa ara saber que comando se ejecuto en caso de ocurri un error
			anexarComando('echo KYLIBFTP={"Sesion":"' + $nombre + '","Comando":"ls","remote":"'+directorio+'"}');
			// se anexa el comando de ls
			anexarComando("ls " + directorio);
		}
		
		/// guarda el ultimo comando de duplicar archivo
		private var $duplicarArchivos:String;
		
		/**
		 * Duplica uno mas archivos remotos. 
		 * <span>Destination directory or new name or both must be specified. Operation mask can be used instead of new name. Filename can be replaced with wildcard to select multiple files.</span>
		 * 
		 * <p>Ejemplos:</p>
		 * <pre>
		 * instanciaSesionFTP.duplicarArchivo("public_html/", "index.html");
		 * instanciaSesionFTP.duplicarArchivo("about.*", "index.html");
		 * instanciaSesionFTP.duplicarArchivo("public_html/about.*", "index.html"); 
		 * instanciaSesionFTP.duplicarArchivo("/home/martin/*.bak", "public_html/index.html", "public_html/about.html");
		 * instanciaSesionFTP.duplicarArchivo("/home/backup/*.bak", "*.html");
		 * </pre>
		 * 
		 * @param	Destino
		 * @param	...Archivos
		 */
		private/* ES privada por el momento, aun no es soportado el comando cp*/ function duplicarArchivos(Destino:String = null,...Archivos):void
		{
			// se empieza a crear la nueva cadena
			$duplicarArchivos = "cp ";
			// se recorre cada elemento de los parametros extra
			for each (var archivo:Object in Archivos) 
			{
				// se mira si el parametro es una cadena de texto
				if (archivo is String) 
				{
					// se agrega la cadena a la lista
					$duplicarArchivos += String('"' + archivo + '" ');
				}
			}
			// se agrega el destino, en caso de no especificar uno se agrega ./
			$duplicarArchivos += existe(Destino) ? '"' + Destino + '"' : "./";
			// se anexa el comando
			anexarComando($duplicarArchivos);
			// esto se anexa para poder saber cuando se terminadoron de subir los archivos
			anexarComando(String('echo {comando:"cp","info":"listo"}'));
		}
		
		/// guarda el ultimo comando de descargar archivo
		private var $descargarUnica :String;
		
		public function descargaUnica(Origen:String, Destino:String = ""):void
		{
			// esto se usa ara saber que comando se ejecuto en caso de ocurri un error
			anexarComando('echo KYLIBFTP={"Sesion":"' + $nombre + '","Comando":"get","Origen":"' + Origen + '","Destino":"' + Destino + '"}');
			// se reinicia la cadena del comando
			$descargarUnica = String("get " + Origen + " " + Destino);
			//se anexa el comando
			anexarComando($descargarUnica);
		}
		
	}
}