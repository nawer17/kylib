﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Carga.Internet.FTPUtil 
{
	import KYLib.C.Obj.*;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ServidorFTP sirve para almacenar las credenciales de un servidor FTP, esto se usa para indicarle a la clase FTP a que servidor debe conectarse.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class ServidorFTP 
	{
		/// La experesion regular para verificar si esta todo en uno ("ftp://user:1234@ftp.example.com")
		private static const $regExp:RegExp = /\w{1,4}:\/\/\w+:\w+@\w+(\.\w+){1,5}(:\d+)?/;
		
		/**
		 * Crea una nueva credencial de un servidor ftp.
		 * <span>Las credenciales que pase al constructor no pueden ser cambiadas en el futuro, asegurese de que estan bien escritas para evitar errores.</span>
		 * 
		 * <p>En el primer parametro puede pasar una cadena de texto que contenga una url con el formato de acceso ftp ("ftp://user:1234@ftp.example.com"),  si si lo hace de este modo los demas parametros seran ignorados</p>
		 * 
		 * @param	TipoServidor Es el protocolo de internet utilizado para conectarse al servidor, los protocolos aceptados son: ftp, sftp y scp (se agregaran parametros adicionales para los protocolos scp y sftp).
		 * @param	Servidor El servidor al que se conectara, en este mismo parametro se debe ingresar un puerto si es necesario, esto se hace de la forma "servidor:puerto".
		 * @param	Usuario El nombre de usuario de una cuenta del servidor.
		 * @param	Contraseña La contraseña de la cuenta.
		 */
		public function ServidorFTP(TipoServidor:String = "ftp", Servidor:String = "ftp.example.com", Usuario:String = null, Contraseña:String = null)
		{
			if (TipoServidor.search($regExp) >= 0)
			{
				$url = $regExp.exec(TipoServidor)[0];
			}
			else
			{
				var credencial:String = existe(Usuario) ? Usuario : "";
				credencial += existen(Usuario, Contraseña) ? String(":" + Contraseña + "@") : existe(Usuario) ? "@" : "";
				$url = String(TipoServidor + "://" + credencial + Servidor);
			}
			$host = /@(\w+(\.\w+){1,5}):?/.exec($url)[1];
		}
		
		///guarda el host al que se conecta
		private var $host:String;
		
		/**
		 * Es el host al que se conectara.
		 */
		public function get host():String 
		{
			return $host;
		}
		
		/// guarda la url
		private var $url:String = "";
		
		/**
		 * Devuelve la url de coxeción al servidor
		 */
		public function get url():String
		{
			return $url;
		}
	}
}