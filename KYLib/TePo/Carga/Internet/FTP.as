﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Carga.Internet 
{
	import KYLib.C.*;
	import KYLib.C.Obj.*;
	import KYLib.Eventos.EventosCargas.EventoFTP;
	import KYLib.TePo.ByteLevel.hex;

	import KYLib.C.Utils.GLOBAL;
	
	import KYLib.Errores.ErroresCargas.Internet.ErrorFTP;
	import KYLib.Errores.ErroresClases.*;
	import KYLib.Errores.KYlibError;
	import KYLib.Eventos.TePo.EventosExteriores.EventoCMD;
	import KYLib.Interfaces.iEventoError;
	import KYLib.TePo.Carga.Internet.FTPUtil.*;
	import KYLib.TePo.Exterior.CMD;
	import flash.filesystem.*;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase FTP se usa para establecer comunicacion con un servidor FTP. Esta clase permite descargar, subir, sincronizar o eliminar archivos de un servidor FTP.
	 * 
	 * <p>Para poder empezar a trabajar con esta clase primero debe de inicializarla con el metodo estatico <code>init()</code>, sin embargo no es necesario inicializarla si en su clase principal llamo al metodo <code>inicializar()</code> de la clase esencial.</p>
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 * 
	 * @see KYLib.C.esencial#inicializar();
	 */
	public final class FTP 
	{
		/// plantilla a pasar
		private static var $plantilla:String;
		
		[Embed(source="../../../../exes KYLib/KTCI.FTP.com", mimeType="application/octet-stream")]
		/// guarda los datos del archivo .com
		private static const $claseCom:Class;
		
		[Embed(source="../../../../exes KYLib/KTCI.FTP.exe", mimeType="application/octet-stream")]
		/// guarda los datos del ejecutable de WinSCP
		private static const $claseEXE:Class;
		
		///Directorio temporalde la clase
		private static var $FTPDir:File;
		
		/// el directorio del script
		static private var $ScripDir:File;
		
		/// File stream para escribir los ejecutables
		private static const $fs:FileStream = new FileStream();
		
		/// El CMD que se usa para comunicarse con el WinSCP
		private static var $cmd:CMD;
		
		/// la expresion regular para buscar entre las respuestas
		private static const $regExp:RegExp = /KYLIBFTP=(\{"Sesion":"\w+"(,"\w+":".*")+\})/;
		
		/**
		 * @private
		 */
		public function FTP() 
		{
			ErrorClaseAIR.agregar(this);
			ErrorClaseAbstracta.agregar(this);
		}
		
		/**
		 * Inicializa la clase para poder utilizarla.
		 * <span>Llamar este metodo es obligatorio si quiere utilizar la clase ya que define algunas variables iniciales que deben usarse luego.</span>
		 * 
		 * <p>Tenga en cuenta que primero debe de haber inicializado la clase esencial.</p>
		 * 
		 * @param callback Debido a que la clase FTP solo tiene metodos estaticos y no se puede instanciar entonces no es posible que distribuya eventos, es por eso que en este parametro debe de pasar una funcion que quiere que se ejecute cuando se termine de inicializar la clase.
		 * 
		 * @return Devuelve <code>true</code> si la clase se logro iniciar y devuelve <code>false</code> si la clase no se pudo iniciar ya sea por un error o porque ya habia sido iniciada antes.
		 * 
		 * @see KYLib.C.esencial Clase esencial
		 */
		public static function init(callback:Function = null):Boolean 
		{
			//si estos objetos exites es porque la clase ya ha sido inicializada
			if (existen($plantilla, $FTPDir, $cmd)) 
			{
				return false;
			}
			try 
			{
				//se guarda el callback
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.InitCallback = callback;
				//la plantilla que se pasara a los objetos SesionFTP
				$plantilla = GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTPUtil.SesionFTP.plantilla;
				// se guarda el callback
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.InitCallback = callback;
				// se apunta el directorio principal
				$FTPDir = GLOBAL.tempDir.resolvePath("KYLib/TePo/Carga/Internet/FTP.as/WS/bin/");
				// se apunta al directorio del ejecutable
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.mainDir = $FTPDir.resolvePath("KTCI.FTP.EXE");
				//se abre el archivo del ejecutable
				$fs.open(GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.mainDir, FileMode.WRITE);
				//se escriben los datos del WinSCP en una variable temporal
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes = new $claseEXE()
				//se escriben los datos del ejecutable
				$fs.writeBytes(GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes);
				//se cierra el stream
				$fs.close();
				//se elimina la variable temporal para liberar memoria
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes.clear();
				delete GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes;
				//ahora se crea el apuntador de la consola del WinSCP
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.mainDir = $FTPDir.resolvePath("KTCI.FTP.COM");
				//se abre el archivo
				$fs.open(GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.mainDir, FileMode.WRITE);
				//se escriben los datos de la consola del WinSCP en una variable temporal
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes = new $claseCom()
				// se escriben los datos
				$fs.writeBytes(GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes);
				//se cierra el stream
				$fs.close();
				//se elimina la variable temporal para liberar memoria
				GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes.clear();
				delete GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.tempBytes;
				//se crea el CMD que se usara para abrir WinSCP
				$cmd = new CMD($FTPDir.nativePath);
				//se agrega la linea de llamado
				$cmd.agregar("KTCI.FTP.COM /ini=nul /script=s.txt");
				//se agrega un listener para saber cuando esta construido
				$cmd.addEventListener(EventoCMD.CONSTRUIDO, on$cmdConstruido);
				//se construye el CMD
				$cmd.construir();
			} 
			catch (err:Error) 
			{
				//si ocurre algun error se devuelve false
				trace(err.message);
				return false;
			}
			//si todo sale bien se devuelve true
			return true;
		}
		
		/// se ejecuta cuando se construye el cmd
		static private function on$cmdConstruido(e:EventoCMD):void 
		{
			//se se cammbia la direccion del $FTPDir a la del archivo que contiene los scripts
			$ScripDir = $FTPDir.resolvePath("s.txt");
			//se abre el archivo de scripts
			$fs.open($ScripDir, FileMode.WRITE);
			//se agrega el comando exit para que cuando se ejecute se cierre
			$fs.writeUTFBytes("exit");
			//se cierra el stream
			$fs.close();
			//se agrega un listener para saber cuando acaba de trabajar el proceso
			$cmd.addEventListener(EventoCMD.SALIDA, on$cmdSalida);
			//al principio se llama al cmd para que ejecute el WinSCP y cree el archivo .ini
			$cmd.ejecutar();
		}
		
		/// se usa para indicar si es la primera vez que se ejecuta el cmd
		private static var $cmdSalida:Boolean = false;
		
		/// se ejecuta cuando se cierra el cmd
		static private function on$cmdSalida(e:EventoCMD):void 
		{
			trace("Codigo salida ftp: " + e.codigoSalida);
			if (e.codigoSalida != 0) 
			{
				trace(e.respuesta);
			}
			// se mira si es la primera vez que se ejecuta el cmd
			if (!$cmdSalida) 
			{
				// se pone en true la variable
				$cmdSalida = true;
				// se mira si se definio un callback
				if (existe(GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.InitCallback)) 
				{
					//se llama al callback
					GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.InitCallback();
					//se borra la variable para liberar espacio
					delete GLOBAL.runtime.KYLib.TePo.Carga.Internet.FTP.InitCallback;
				}
				//se agrega un listener de las respuestas
				$cmd.addEventListener(EventoCMD.RESPUESTA, on$cmdRespuesta);
				return;
			}
		}
		
		/// guarda el objeto que se obtuvo en una respuesta
		private static var $ultimaRespuesta:Array;
		
		/// conjunto de respuestas
		private static var $respuesta:String = "";
		
		/// guarda el contenido del echo
		private static var $echo:Object;
		
		/// se ejecuta cuando se obtiene una respuesta del CMD
		static private function on$cmdRespuesta(e:EventoCMD):void 
		{
			$ultimaRespuesta = $regExp.exec(e.respuesta);
			
			if (existe($ultimaRespuesta)) 
			{
				if (existe($echo)) 
				{
					parseData();
				}
				$echo = JSON.parse($ultimaRespuesta[1]);
				$respuesta = "";
				return;
			}
			$respuesta += e.respuesta;
			if (existe($echo)) 
			{
				if (existe($error = verificarErroresDeEvento(e.respuesta)))
				{
					$sesionActual.dispatchErrorEvent($error.aEvento());
				}
			}
		}
		
		private static const $disListReg:RegExp = /([-D])[-\w]{9}(\s+\d+){1,3}\s+(\d+)\s+([\w: ]+\d{4})\s+([\w\- .]+)/;
		
		static private function parseData():void 
		{
			switch ($echo.Comando) 
			{
				case "ls":
				{
					var Dev:Array = [];
					var res:Object;
					var texto:String = hex.aCadena(hex.deCadena($respuesta).replace(/0d0a/g, "7c"));
					while ($disListReg.test(texto))
					{
						res = $disListReg.exec(texto);
						texto = texto.replace(res[0], "");
						Dev.push({
							tipo:(res[1] == "D" ? "D" : "F"),
							owner:res[2],
							size:(res[1] == "D" ? 0 : res[3]),
							modificado:res[4],
							nombre:res[5]
						});
					}
					
					Dev.unshift($echo.remote);
					$sesionActual.listadoDirectorio = Dev;
					$sesionActual.dispatchEvent(new EventoFTP(EventoFTP.DIRECTORIO_LISTADO,$sesionActual));
					break;
				}
			}
		}
		
		/// guarda el ultimo error que se obtuvo
		private static var $error:ErrorFTP;
		
		/**
		 * Busca si ocurrio algun error en la ejecucion de la sesion actual
		 */
		private static function verificarErroresDeEvento(Respuesta:String):ErrorFTP
		{
			var Dev:ErrorFTP;
			
			switch ($echo.Comando)
			{
				case "open":
				{
					Dev = /Connection failed./.test(Respuesta) ? new ErrorFTP(ErrorFTP.SESION_CONEXIONFALLIDA, $sesionActual.servidor.host , $sesionActual.nombre) : Dev;
					Dev = /Access denied./.test(Respuesta) ? new ErrorFTP(ErrorFTP.SESION_ACCESODENEGADO, $sesionActual.servidor.host , $sesionActual.nombre) : Dev;
					break;
				}
			}
			
			return Dev;
		}
		
		/// apunta a la sesion que se esta ejecutando actualmente
		private static var $sesionActual:SesionFTP;
		
		/**
		 * Ejecuta un conjunto de instrucciones definidas en una instancia de la clase SesionFTP.
		 * 
		 * <span>Debe tener en cuenta que para saber cuando se termino de trabajar con esta sesion debe agregar controladores de eventos a la instancia de SesionFTP.</span>
		 * 
		 * @param	sesion La SesionFTP que se quiere ejecutar.
		 * 
		 * @return Devuelve <code>true</code> si es posible ejecutar la sesion en cuyo caso se empieza a ejecutar, devuelve <code>false</code> si no se puede ejecutar la sesion debido a que ya se esta ejecutando una en el momento.
		 */
		public static function ejecutarSesion(Sesion:SesionFTP):Boolean
		{
			if ($cmd.ejecutando) 
			{
				return false;
			}
			$sesionActual = Sesion;
			if ($ScripDir.exists) 
			{
				$ScripDir.deleteFile();
			}
			$fs.open($ScripDir, FileMode.WRITE);
			$fs.writeUTFBytes(
			"option confirm off\noption batch continue\n" + 
			String($sesionActual.getVector($plantilla) + 
			'echo KYLIBFTP={"Sesion":"none","Comando":"close"}\n' +
			"close\nexit"));
			$fs.close();
			$cmd.ejecutar();
			return true;
		}
		
		private static function ejecutarSesiones(sesiones:Vector.<SesionFTP>):Boolean
		{
			return false;
		}
		
	}
}