﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

 package KYLib.TePo.Carga
{
	import flash.display.*;
	import flash.net.*;
	import flash.system.*;
	import flash.events.*;
	import KYLib.Eventos.EventosCargas.EventoCarga;
	//import flash.filesystem.File; //Reservado

	/**
	 * @eventType Eventos.EventosCargas.EventoCarga.CARGACOMPLETA
	 */
	[Event(name = "CargaCompletada", type = "KYLib.Eventos.EventosCargas.EventoCarga")]

	/**
	 * @eventType Eventos.EventosCargas.EventoCarga.PROGRESO
	 */
	[Event(name = "ProgresoCargaDeBytes", type = "KYLib.Eventos.EventosCargas.EventoCarga")]

	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase cargadorSWF es utilizada para cargar archivos swf que se encuentran en la carpeta raiz de la aplicación o en alguna de sus subcarpetas.
	 *
	 * <span>Los archivos swf cargados deben de tener como clase principal una que tenga el mismo nombre del swf, es decir, si el archivo swf se llama ejemplo.swf entonces la clase principal de ese archivo debe de llamarse <code>ejemplo</code> para poder obtenerla y almacenarla en el objeto cargadorSWF.</span>
	 *
	 * <p>Por cada archivo swf se guardan tres objetos: el objeto Loader que lo cargo, el objeto swf cargado como un objeto de visualización y la clase del objeto cargado. Para acceder a cada uno de estos valores llame a los metodos <code>obtenerLoader()</code>,<code>obtenerSWF()</code> y <code>obtenerClase</code> respectivamente ya pase como parametro nombreSWF el nombre del SWF del cual se quiere obtener información.</p>
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 */
	public class cargadorSWF extends EventDispatcher
	{

		/**
		 * contiene todos los loaders
		 */
		private var $loadersArray: Object = {
			"0": 0
		};

		/**
		 * contiene los contenidos de los swf
		 */
		private var $objetos: Object = {
			"0": 0
		};

		/**
		 * numero de archivos a ser cargados
		 */
		private var $NoSwfs: int;

		/**
		 * archivo que esta siendo cargado actualmente
		 */
		private var $swfActual: Array = [0, ""];

		/**
		 * carpeta de destino donde se encuentran los archivos
		 */
		private var $carpeta: String = "";

		/**
		 * contiene los nombres de los archivos a ser cargados
		 */
		private var $SWFs: Array = new Array();

		/**
		* url que apunta a un archivo
		*/
		private var $req: URLRequest;

		/**
		 * contexto que ajusta todos los archivos cargados al dominio actual
		 */
		private const $contexto: LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);

		/**
		 * Clase principal del swf actual
		 */
		private var $claseActual: Class;

		/**
		 * indica si ya estan o no cargados todos los archivos
		 */
		private var $todoCargado: Boolean = false;

		/**
		 * indica que los archivos se iniciaran a cargar automaticamente despues de haberse creado el objeto.
		 */
		private var $autoCargar: Boolean;

		/*
		 * peso total, es decir, la suma del peso de todos los archivos
		 */
		private var $bytesTotales: Number = 0;

		/**
		 * file que apunta a un archivo para ver su peso
		 */
		//var $archivoActual:File;

		/**
		 * indica si ya se cargaron los archivos para no cargarlos dos veces
		 */
		private var $cargado: Boolean = false;

		/**
		 * Crea un nuevo cargador de archivos .swf externos.
		 *
		 * <span>Al crear un cargador de archivos swf debe de especificar los nombres de los archivos .swf sin exención y la carpeta en la que se encuentran.</span>
		 *
		 * @param ListaDeSwfs Un arreglo con cadenas de texto que son los nombres de los archivos que seran cargados, los nombres deben de ser sin extención, es decir, si quiere cargar un archivo llamado "ejemplo.swf" solo debe de pasar la cadena "ejemplo".
		 * @param Carpeta La subcarpeta de la carpeta raiz de la aplicación en la que se encuentran los archivos a cargar, el nombre de la carpeta no debe de tener barra inclinida ni al principio ni al final, es decir, si los archivos se encuentran en una carpeta llamada "/Contenido/Interactivo/Botones/" debe de pasar como valor la cadena "Contenido/Interactivo/Botones".
		 * @param AutoCargar Indica si se deben de empezar a cargar los archivos cuando se crea el objeto.
		 */
		public function cargadorSWF(ListaDeSwfs: Array, Carpeta: String = "", AutoCargar: Boolean = false)
		{
			$carpeta = Carpeta;

			$SWFs = ListaDeSwfs;

			$autoCargar = AutoCargar;

			if (ListaDeSwfs == null)
			{
				$NoSwfs = 0;
			}
			else
			{
				$NoSwfs = ListaDeSwfs.length;
				obtenerPesoTotal();
			}

		}


		/**
		 * Inicia la carga de los archivos.
		 *
		 * <span>Si los archivos ya han sido cargados antes entonces esta función ya no hara nada.</span>
		 */
		public function cargar(): void
		{
			//se mira que no hayan sido cargados los archivos
			if ($cargado == false)
			{
				//si no han sido cargados se pone que si
				$cargado = true;
				
				if ($NoSwfs > 0)
				{
					//Consola.log("Iniciando proceso de carga de los archivos [" + $SWFs + "] en la carpeta /" + $carpeta, Consola.COMENTARIO, true);
					cargarSiguiente();
				}
				else
				{
					//Consola.log("No se pueden cargar los archivos porque no hay archivos para cargar", Consola.ERROR, false);
				}
			}
			else
			{
				//Consola.log("No se pueden cargar los archivos porque ya han sido cargados", Consola.ERROR, false);
			}
		}
		
		/**
		 * Carga el siguiente archivo en la cola de carga.
		 */
		private function cargarSiguiente(): void
		{
			$swfActual[1] = $SWFs[$swfActual[0]];

			cargarSWF($SWFs[$swfActual[0]]);

			$swfActual[0] += 1;
		}


		/**
		 * Carga un archivo swf especifico
		 */
		private function cargarSWF(swf: String): void
		{
			//Consola.log("swf cargandose actualmente: " + $swfActual, Consola.COMENTARIO, true);

			$loadersArray[0]++;


			$loadersArray[$swfActual[1]] = new Loader();

			var swfname: String = String($carpeta + "/" + swf + ".kylib");

			$req = new URLRequest(swfname);

			$loadersArray[$swfActual[1]].contentLoaderInfo.addEventListener(Event.COMPLETE, LdrCompleto);

			$loadersArray[$swfActual[1]].contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);

			$loadersArray[$swfActual[1]].load($req, $contexto);
		}

		/**
		 * se ejecuta en el progreso de carga de cada swf
		 */
		private function onProgress(e: ProgressEvent): void
		{

		}


		/**
		 * funcion que se ejecuta al cargarse un swf
		 */
		private function LdrCompleto(e: Event): void
		{
			//Consola.log("Conectando el dominio de " + $swfActual[1] + ".swf al dominio de la aplicación", Consola.COMENTARIO, false);
			try//se intenta obtener la clase del archivo que debe de estar en el dominio de la aplicación
			{
				$claseActual = ApplicationDomain.currentDomain.getDefinition($swfActual[1]) as Class;
				//Consola.log("Se ha conectado exitosamente el dominio de " + $swfActual[1] + ".swf al dominio de la aplicació", Consola.PROCESO,false);
			}
			catch (er: Error)
			{
				//Consola.log("No ha sido posible conectar el dominio de " + $swfActual[1] + ".swf al dominio de la aplicació :: " + er.message, Consola.ERROR,false)
			}

			//Consola.log("Descomprimiendo el contenido de " + $swfActual[1] + ".swf", Consola.COMENTARIO,true);

			$objetos[$swfActual[1]] = e.target.content;

			//Consola.log("Cargado correctamente:" + $swfActual[1] + ".swf" + " con contenido de tipo : " + $objetos[$swfActual[1]], Consola.PROCESO,true);

			//se mira si hay mas archivos por cargar
			if ($swfActual[0] < $NoSwfs)
			{
				cargarSiguiente();
			}
			else if ($swfActual[0] == $NoSwfs)//en caso de que hayan sido cargados todos los archivos
			{
				$todoCargado = true;

				var eventoCargaCompleta: EventoCarga = new EventoCarga(EventoCarga.CARGACOMPLETA,150,150);

				dispatchEvent(eventoCargaCompleta);
			}
		}

		/**
		 * Devuelve el numero de archivos que van a ser cargados.
		 */
		public function get numeroSWFs(): int
		{
			return $NoSwfs;
		}

		/**
		 * Obtiene el peso total de todos los archivos.
		 */
		private function obtenerPesoTotal(): void
		{
			/*for (var i:int = 0; i<$NoSwfs; i++)
			{
				private var g:URLRequest;
				private var patch:String = String(carpeta + SWFs[i] + ".swf");
				CurrentFile = File.applicationDirectory.resolvePath(patch);
				$bytesTotales +=  CurrentFile.size;
				if (i == $NoSwfs-1)
				{
					Console.log("Peso total de los archivos que seran cargados: " + $bytesTotales,Console.COMENTARIO);
					if ($autoCargar)
					{
						cargar();
					}
				}
			}*/
		}

		/**
		 * Devuelve el loader que cargo al archivo indicado.
		 *
		 * @param nombreSWF El nombre del archivo del que se quiere obtener el Loader.
		 *
		 * @return El loader que cargo al archivo especificado.
		*/
		public function obtenerLoader(nombreSWF: String): Loader
		{
			return $loadersArray[nombreSWF];
		}

		/**
		 * Devuelve el contenido del archivo swf indicado.
		 *
		 * @param nombreSWF El nombre del archivo del que se quiere obtener el contenido.
		 *
		 * @return Un objeto de visualización que tiene el contenido del archivo especificado.
		 */
		public function obtenerSWF(nombreSWF: String): *
		{
			return $objetos[nombreSWF];
		}

		/**
		 * Devuelve la clase principal del archivo indicado.
		 *
		 * @param nombreSWF El nombre del archivo del que se quiere obtener la clase principal.
		 *
		 * @return La clase principal del archivo especificado.
		*/
		public function obtenerClase(nombreSWF: String): *
		{
			return $objetos[nombreSWF];
		}

		/**
		 * Indica si ya han sido cargados todos los archivos.
		 */
		public function get todoCargado(): Boolean
		{
			return $todoCargado;
		}

	}
}