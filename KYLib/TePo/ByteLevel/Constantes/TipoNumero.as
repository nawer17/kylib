﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.ByteLevel.Constantes 
{
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase TipoNumero es usada para obtener los valores usados por algunas funciones para identificar el tipo de numero con el que se va a trabajar.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Tabatha(kokopai)
	 */
	public final class TipoNumero 
	{
		/**
		 * Tipo automatico: cuando se pasa este valor a un metodo el intentara identificar el tipo de número.
		 */
		public static function get Auto():String { return "auto"; }
		
		/**
		 * Indica que el número es un entero con signo de 8 bits.
		 * <span>El rango de este número es de -128 hasta 127.</span>
		 */
		public static function get Byte():String { return "byte"; }
		
		/**
		 * Indica que el número es un entero sin signo de 8 bits.
		 * <span>El rango de este número es de 0 hasta 127.</span>
		 */
		public static function get UnsignedByte():String { return "ubyte"; }
		
		/**
		 * Indica que el número es un entero con signo de 16 bits.
		 * <span>El rango de este número es de -32768 hasta 32767.</span>
		 */
		public static function get Short():String { return "short"; }
		
		/**
		 * Indica que el número es un entero sin signo de 16 bits.
		 * <span>El rango de este número es de 0 hasta 65535.</span>
		 */
		public static function get UnsignedShort():String { return "ushort"; }
		
		/**
		 * Indica que el número es un entero con signo de 24 bits.
		 * <span>El rango de este número es de -8388608 hasta 8388607.</span>
		 */
		public static function get Medium():String { return "medium"; }
		
		/**
		 * Indica que el número es un entero sin signo de 24 bits.
		 * <span>El rango de este número es de 0 hasta 16777215.</span>
		 */
		public static function get UnsignedMedium():String { return "umedium"; }
		
		/**
		 * Indica que el número es un entero con signo de 32 bits.
		 * <span>El rango de este número es de -2147483648 hasta 2147483647.</span>
		 */
		public static function get Int():String { return "int"; }
		
		/**
		 * Indica que el número es un entero sin signo de 32 bits.
		 * <span>El rango de este número es de 0 hasta 4294967295.</span>
		 */
		public static function get UnsignedInt():String { return "uint"; }
		
		/**
		 * Indica que el número es un entero positivo de una longitud dinamica de bits(depende de cada numero).
		 * <span>El rango de este número es de 0 hasta 1e+308.</span>
		 */
		public static function get IntPositivo():String { return "+int"; }
		
		/**
		 * Indica que el número es un entero negativo de una longitud dinamica de bits(depende de cada numero).
		 * <span>El rango de este número es de -1e+308 hasta 0.</span>
		 */
		public static function get IntNegativo():String { return "-int"; }
		
		/**
		 * Indica que es número de coma flotante de precisión simple de 32 bits.
		 * <span>El rango de este número es de -3.402823466E+38 hasta -1.175494351E-38, 0 y de 1.175494351E-38 hasta 3.402823466E+38.</span>
		 */
		public static function get Float():String { return "float"; }
		
		/**
		 * Indica que es número de coma flotante de doble precisión de 64 bits.
		 * <span>El rango de este número es de -1.7976931348623157E+308 hasta -2.2250738585072014E-308, 0 y de 2.2250738585072014E-308 hasta 1.7976931348623157E+308.</span>
		 */
		public static function get Double():String { return "double"; }
		
	}
}