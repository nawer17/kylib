﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.ByteLevel 
{
	import KYLib.Errores.ErroresClases.ErrorClaseAbstracta;
	import KYLib.Errores.KYlibError;
	import KYLib.TePo.ByteLevel.*;
	import KYLib.TePo.ByteLevel.Constantes.*;
	import KYLib.Errores.ErroresClases.*;
	
	[ExcludeClass]
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase Util se usa para utilidades en las funciones a nivel de bytes. Esta clase es interna por lo que no puede ser usada fuera de este paquete.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Tabatha(kokopai)
	 */
	internal final class ByteLevelUtil 
	{
		
		/**
		 * @private
		 */
		public function ByteLevelUtil() 
		{
			ErrorClaseAbstracta.agregar(this);
			super();
		}
		
		/**
		 * Detecta el tipo de numero pasado.
		 * 
		 * @param	numero El numero del cual se quiere saber el tipo.
		 * @return El tipo de numero.
		 */
		internal static function detectarTipoNumero(value:Number):String 
		{
			var Str:String = String(value);
			//se mira si es un numero de coma flotante o si esta reducido, para esto se mira si la representación textual del numero tiene el caracter ".", tambien se mira si el numero esta por debajo del valor minimo que puede tener un int.
			if (Str.indexOf(".") != -1 && Str.lastIndexOf("e") == -1) 
			{
				// Se mira si tiene mas de dos decimales o de si esta reducido
				if (Str.indexOf(".") < Str.length - 3) 
				{
					// en caso de que se tengan mas de dos decimales es un numero double
					return TipoNumero.Double;
				}
				// se mira si el numero esta en el rango de un numero flotante aceptable.
				if ((value > -1e+12 && value < -1e-2) || (value > 1e-2 && value < 1e+12)) 
				{
					// se es un numero flotante aceptable se devuelve ese tipo
					return TipoNumero.Float;
				}
				// en caso de no ser un flotante entonces sera un double
				return TipoNumero.Double;
			}
			//se mira si el numero esta en el rango soportado por un byte
			if (value <= 255 && value >= -128) 
			{
				// se mira si el nnumero esta en el rango de un byte sin signo
				if (value >= 128 && value <= 255) 
				{
					// se devuelve el tipo byte sin signo
					return TipoNumero.UnsignedByte;
				}
				// en caso de no ser un byte sin signo entonces es un byte con signo
				return TipoNumero.Byte;
			}
			//se mira si el numero esta en el rango soportado por un short
			if (value <= 65535 && value >= -32768) 
			{
				// se mira si el nnumero esta en el rango de un short sin signo
				if (value >= 32768 && value <= 65535) 
				{
					// se devuelve el tipo short sin signo
					return TipoNumero.UnsignedShort;
				}
				// en caso de no ser un short sin signo entonces es un short con signo
				return TipoNumero.Short;
			}
			//se mira si el numero esta en el rango soportado por un medium
			if (value <= 16777215 && value >= -8388608) 
			{
				// se mira si el nnumero esta en el rango de un medium sin signo
				if (value >= 8388608 && value <= 16777215) 
				{
					// se devuelve el tipo medium sin signo
					return TipoNumero.UnsignedMedium;
				}
				// en caso de no ser un medium sin signo entonces es un medium con signo
				return TipoNumero.Medium;
			}
			//se mira si el numero esta en el rango soportado por un int con signo
			if (value <= 4294967295 && value >= -2147483648) 
			{
				// se mira si el nnumero esta en el rango de un int sin signo
				if (value >= 2147483648 && value <= 4294967295) 
				{
					// se devuelve el tipo int sin signo
					return TipoNumero.UnsignedInt;
				}
				// en caso de no ser un int sin signo entonces es un int con signo
				return TipoNumero.Int;
			}
			// en este punto ya sabemos que el numero es un entero
			
			// se mira que el valor no sea negativo
			if (value < 0) 
			{
				// si el valor es negativo entonces se devuelve el tipo entero negativo
				return TipoNumero.IntNegativo;
			}
			// si el valor es un positivo entonces se el tipo entero positivo
			return TipoNumero.IntPositivo;
		}
		
		/**
		 * Escribe un numero, de un tipo especificado dentro de un cojunto de bytes.
		 * 
		 * @param	value El numero que se escribira.
		 * @param	tipo El tipo de numero a ser escrito.
		 * @param	bytes El conjunto de bytes donde sera escrito el numero.
		 */
		public static function escribirNumero(value:Number, tipo:String, bytes:ConjuntoBytes):void 
		{
			switch (tipo) 
			{
				case TipoNumero.Byte:
				case TipoNumero.UnsignedByte:
				{
					bytes.writeByte(value);
					break;
				}
				case TipoNumero.Short:
				case TipoNumero.UnsignedShort:
				{
					bytes.writeShort(value);
					break;
				}
				case TipoNumero.Medium:
				case TipoNumero.UnsignedMedium:
				{
					bytes.writeMedium(value);
					break;
				}
				case TipoNumero.Int:
				{
					bytes.writeInt(value);
					break;
				}
				case TipoNumero.UnsignedInt:
				{
					bytes.writeUnsignedInt(value);
					break;
				}
				case TipoNumero.Float:
				{
					bytes.writeFloat(value);
					break;
				}
				case TipoNumero.Double:
				{
					bytes.writeDouble(value);
					break;
				}
				default:
				{
					KYlibError.lanzar(new ArgumentError("El tipo de numero \"" + tipo + "\" no es valido.", 1327));
				}
			}
		}
		
	}
}