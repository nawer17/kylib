﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.ByteLevel.Codificacion 
{
	import KYLib.C.Mat.random;
	import KYLib.TePo.ByteLevel.ConjuntoBytes;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase TW3code tienen 2 metodos estaticos para codificar o decodificar conjuntos de bytes con un mecanismo de sumas de trios de bytes.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Tabatha(kokopai)
	 */
	public final class TW3code
	{
		
		/**
		 * Codifica un conjunto de bytes en TW3, el conjunto de bytes resultante tendra 3 bytes extra que el conjunto original.
		 * 
		 * @param	value El conjunto de bytes que se quiere codificar. Este conjunto tiene que tener un numero de bytes divisible entre 3, en caso de que el numero de bytes no sea divisible en 3 se agregara un byte <code>0x00</code> al final del conjunto de bytes hasta que sea divisible entre 3.
		 * @param	codigoCodificacion El codigo de codificación usado para la suma de bytes, cada codigo distinto genera una codificación distinta. Se recomienda usar un codigo al azar cada vez que se vaya a codificar.
		 * <span>El rango de este codigo es desde <code>0x0002</code> hasta <code>0xffff</code>. Si el valor pasado es igual a 0 entonces se generara un numero aleatorio para codificar.</span>
		 * @return El cojunto de bytes codificado.
		 */
		public static function codificar(value:ConjuntoBytes, codigoCodificacion:uint = 0x0000):ConjuntoBytes
		{
			/// La variable devuelta
			var Dev:ConjuntoBytes = new ConjuntoBytes
			/// El codigo de utilizado
			var codigo:uint = codigoCodificacion == 0 || codigoCodificacion > 0xffff ? random.generar(0x1000, 0xffff) : codigoCodificacion;
			/// El numero de bytes agregados al final
			var resto:uint = 0;
			///La copia del conjunto de bytes
			var copia:ConjuntoBytes = new ConjuntoBytes();
			copia.deDataInput(value);
			
			// Se pone la posición del conjunto original en el ultimo
			copia.position = copia.length;
			// se mira si tiene un numero par de bytes
			while ((copia.length % 3) != 0) 
			{
				// si no se tiene un numero par de bytes se agrega uno al final
				copia.writeBoolean(false);
				resto += 3;
			}
			copia.position = 0;
			
			// se escribe el codigo usado en el conjunto de bytes
			Dev.writeShort(codigo);
			//se escriben el numero de bytes agregados al final
			Dev.writeByte(resto + 7);
			// para cada par de bytes
			while (copia.bytesAvailable) 
			{
				// es escribe el par de bytes con la suma del codigos
				Dev.writeMedium(Number(copia.readUnsignedMedium() + (codigo * 222)));
			}
			Dev.position = 0;
			return Dev;
		}
		
		/**
		 * Decodifica un conjunto de bytes que haya sido sido codificado con TW2.
		 * 
		 * @param	value Un conjunto de bytes codificado con TW2.
		 * @return El conjunto de bytes decodificado.
		 */
		public static function decodificar(value:ConjuntoBytes):ConjuntoBytes
		{
			// si el conjunto de bytes pasado no tiene un numero divisible entre tres bytes no esta codificado con tw3 entonces se devuelve null
			if ((value.length % 3) != 0) 
			{
				return null;
			}
			
			/// Esta variable almacena el cojunto de bytes decodificado temporal
			var bytes:ConjuntoBytes = new ConjuntoBytes();
			/// La variable devuelta
			var Dev:ConjuntoBytes = new ConjuntoBytes();
			/// El codigo de codificación usado
			var codigo:uint = 0;
			/// El numero de bytes agregados al final
			var resto:uint = 0;
			
			value.position = 0;
			// se leen los primeros 2 bytes que equivalen al codigo
			codigo = value.readUnsignedShort();
			//se lee el resto
			resto = Number((value.readUnsignedByte() - 7) / 3);
			// se empiezan a leer los bytes
			while (value.bytesAvailable) 
			{
				// se decodifica restando la suma de bytes
				bytes.writeMedium(value.readUnsignedMedium() - (codigo * 222));
			}
			bytes.position = 0;
			bytes.readBytes(Dev, 0, bytes.length - resto);
			
			Dev.position = 0;
			return Dev;
		}
		
	}
}