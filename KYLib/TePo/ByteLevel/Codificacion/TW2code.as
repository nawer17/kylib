﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.ByteLevel.Codificacion 
{
	import KYLib.C.Mat.random;
	import KYLib.TePo.ByteLevel.ConjuntoBytes;
	import KYLib.TePo.ByteLevel.hex;
	import flash.utils.ByteArray;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase TW2code tienen 2 metodos estaticos para codificar o decodificar conjuntos de bytes con un mecanismo de sumas de pares de bytes.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Tabatha(kokopai)
	 */
	public final class TW2code
	{
		/// Conjunto de bytes usado para operaciones internas
		private static const $cb:ConjuntoBytes = new ConjuntoBytes();
		
		public static function codificarTexto(value:*, codigoCodificacion:uint = 0x0000,isHex:Boolean = false):String
		{
			return hex.deBytes(codificar(value, codigoCodificacion, isHex));
		}
		
		/**
		 * Codifica un objeto en TW2, el conjunto de bytes resultante tendra 2 bytes extra que el conjunto original.
		 * 
		 * @param	value Algun objeto a codificar, puede ser una cadena, alguna clase que extienda de ByteArray o cualquier otro objeto.
		 * @param	codigoCodificacion El codigo de codificación usado para la suma de bytes, cada codigo distinto genera una codificación distinta. Se recomienda usar un codigo al azar cada vez que se vaya a codificar.
		 * <span>El rango de este codigo es desde <code>0x0001</code> hasta <code>0xffff</code>. Si el valor pasado es igual a 0 entonces se generara un numero aleatorio para codificar.</span>
		 * @return El cojunto de bytes codificado.
		 */
		public static function codificar(value:*, codigoCodificacion:uint = 0x0000,isHex:Boolean = false):ConjuntoBytes
		{
			var codigo:uint = codigoCodificacion == 0 || codigoCodificacion > 0xffff ? random.generar(0x0012, 0xffff) : codigoCodificacion;
			
			if (value is ByteArray) 
			{
				return codificarBytes(value,codigo);
			}
			else if (value is String) 
			{
				var text:String = isHex ? hex.aCadena(value) : value;
				return codificarHex(text,codigo);
			}
			else 
			{
				//luego agregar soporte de objetos
			}
			return null;
		}
		
		static private function codificarHex(value:String, codigo:uint):ConjuntoBytes 
		{
			///La copia del conjunto de bytes
			var Dev:ConjuntoBytes = new ConjuntoBytes();
			Dev.writeUTFBytes(value);
			return codificarBytes(Dev, codigo); 
		}
		
		static private function codificarBytes(value:ByteArray,codigo:uint):ConjuntoBytes 
		{
			/// La variable devuelta
			var Dev:ConjuntoBytes = new ConjuntoBytes
			///La copia del conjunto de bytes
			$cb.clear();
			$cb.deDataInput(value);
			
			// Se pone la posición del conjunto original en el ultimo
			$cb.position = $cb.length;
			// se mira si tiene un numero par de bytes
			while (($cb.length % 2) != 0) 
			{
				// si no se tiene un numero par de bytes se agrega uno al final
				$cb.writeBoolean(false);
			}
			$cb.position = 0;
			// se escribe el codigo usado en el conjunto de bytes
			Dev.writeShort(codigo);
			// para cada par de bytes
			while ($cb.bytesAvailable) 
			{
				// es escribe el par de bytes con la suma del codigos
				Dev.writeShort($cb.readUnsignedShort() + codigo);
			}
			Dev.position = 0;
			return Dev;
		}
		
		
		/*public static function decodificarTexto(value:String,ishex:Boolean = false):String
		{
			
		}*/
		
		/**
		 * Decodifica un conjunto de bytes que haya sido sido codificado con TW2.
		 * 
		 * @param	value Un conjunto de bytes codificado con TW2.
		 * @return El conjunto de bytes decodificado.
		 */
		public static function decodificar(value:*,isHex:Boolean = true):ConjuntoBytes
		{	
			if (value is ByteArray) 
			{
				return decodificarBytes(value);
			}
			else if (value is String) 
			{
				return decodificarHex(value,isHex);
			}
			else 
			{
				//luego agregar soporte de objetos
			}
			return null;
		}
		
		static private function decodificarHex(value:String, isHex:Boolean):ConjuntoBytes 
		{
			var Dev:ConjuntoBytes = new ConjuntoBytes();
			if (isHex) 
			{
				Dev.deDataInput(hex.aBytes(value));
			}
			else 
			{
				Dev.writeUTFBytes(value);
			}
			return decodificarBytes(Dev);
		}
		
		public static function decodificarBytes(value:ConjuntoBytes):ConjuntoBytes
		{
			// si el conjunto de bytes pasado no tiene un numero par de bytes no esta codificado con tw2 entonces se devuelve null
			if ((value.length % 2) != 0) 
			{
				return null;
			}
			
			/// Esta variable almacena el cojunto de bytes decodificado temporal
			var bytes:ConjuntoBytes = new ConjuntoBytes();
			/// La variable devuelta
			var Dev:ConjuntoBytes = new ConjuntoBytes();
			/// El codigo de codificación usado
			var codigo:uint = 0;
			
			value.position = 0;
			// se leen los primeros 2 bytes que equivalen al codigo
			codigo = value.readUnsignedShort();
			// se empiezan a leer los bytes
			while (value.bytesAvailable) 
			{
				// se decodifica restando la suma de bytes
				bytes.writeShort(value.readUnsignedShort() - codigo);
			}
			bytes.position = 0;
			//se mira si el ultimo valor es 0
			if (bytes[bytes.length-1] == 0)
			{
				// en caso de que el ultimo valor sea 0x00 entonces no se lee
				bytes.readBytes(Dev, 0, bytes.length - 1);
			}
			else 
			{
				bytes.readBytes(Dev);
			}
			Dev.position = 0;
			return Dev;
		}
		
	}
}