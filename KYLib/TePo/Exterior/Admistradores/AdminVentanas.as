﻿/*
 * Copyright (C) 2020 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Exterior.Admistradores 
{
	import KYLib.C.Obj.existe;
	
	import KYLib.Errores.ErroresClases.*;
	import KYLib.Eventos.*;
	import KYLib.TePo.Exterior.*;
	import flash.display.*;
	import flash.events.Event;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase AdminVentanas
	 *
	 * @langversion 3.0
	 *
	 *  @playerversion AIR 32
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC 2020
	 *
	 * @author Juan Pablo Calle
	 */
	public class AdminVentanas extends DisparadorEventos 
	{
		
		private static var $ventanas:Vector.<Ventana> = new Vector.<Ventana>();
		
		private static var $nativas:Vector.<NativeWindow> = new Vector.<NativeWindow>();
		
		private static var $init:Boolean = false;
		
		public function AdminVentanas() 
		{
			ErrorClaseAIR.agregar(this);
			ErrorClaseAbstracta.agregar(this);
			super();
		}
		
		public static function init():void
		{
			if ($init) 
			{
				return;
			}
			$nativas[0] = Aplicacion.ventanasAbiertas[0];
			$ventanas[0] = new Ventana(null, $nativas[0]);
			
			$init = true;
		}
		
		public static function get ventanaPrincipal():Ventana
		{
			init();
			return $ventanas[0];
		}
		
		public static function deNativa(ventana:NativeWindow):Ventana 
		{
			if (existe(ventana))
			{
				var i:int = $nativas.indexOf(ventana);
				if (i == -1) 
				{
					return crear(null, ventana);
				}
				else 
				{
					return $ventanas[i];
				}
			}
			return null;
		}
		
		public static function cerrar(ventana:Ventana,nativeClose:Boolean=true):void 
		{
			var i:int = $ventanas.indexOf(ventana);
			if (i >= 0) 
			{
				$ventanas.removeAt(i);
				$nativas.removeAt(i);
			}
			$onCerrada(ventana);
			ventana.original.close();
		}
		
		
		
		public static function crear(opciones:OpcionesVentana,nativa:NativeWindow = null):Ventana
		{
			init();
			var w:Ventana = new Ventana(opciones, nativa);
			$ventanas.push(w);
			$nativas.push(w.original);
			w.addEventListener(Event.CLOSING, onWClosing);
			return w;
		}
		
		static private function onWClosing(e:Event):void 
		{
			trace(Aplicacion.ventanasAbiertas);
			e.preventDefault();
			e.stopPropagation();
		}
		
		//
		// Callbacks
		//
		private static var $onCerrada:Function = function (e:Ventana):void 
		{
			trace('ventana "' + e.titulo + '" ha sido cerrada');
		}
		
		static public function set onCerrada(value:Function):void
		{
			$onCerrada = existe(value) ? value : $onCerrada;
		}
	}
}