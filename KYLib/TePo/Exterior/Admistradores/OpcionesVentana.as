﻿/*
 * Copyright (C) 2020 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.TePo.Exterior.Admistradores
{
	
	import KYLib.C.Obj.existe;
	import KYLib.C.Utils.*;
	import KYLib.TePo.Exterior.*;
	import flash.display.*;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase OpcionesVentana
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 32
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC 2020
	 *
	 * @author Juan Pablo Calle
	 */
	public final class OpcionesVentana
	{
		
		/// Opciones iniciales de ventana nativa
		private var $inits:NativeWindowInitOptions;
		
		///configuracion por defecto
		private const $default:Opciones = new Opciones({
			//config del objeto NativeWindowInitOptions
			maximizable: true,
			minimizable: true,
			padre: null,
			render: NativeWindowRenderMode.AUTO,
			resizable: true,
			aspecto: NativeWindowSystemChrome.STANDARD,
			transparente: false,
			tipo: NativeWindowType.NORMAL,
			
			//configs de la ventana
			ancho: 1000, alto: 750,
			siempreAlfrente: false,
			titulo: "ventana",
			
			//config del stage
			clase: Sprite,
			contenido: null,
			modo: StageDisplayState.NORMAL,
			color: 0xffffff,
			fotogramas: 24,
			calidad: StageQuality.MEDIUM,
			escala: StageScaleMode.NO_SCALE,
			alineamiento: StageAlign.TOP_LEFT
			
		});
		
		/**
		 * Crea unas nuevas configuraciones de ventana, puede pasar un objeto con propiedades iniciales para sobreescribir las por defecto.
		 */
		public function OpcionesVentana(InitConfig:Object = null)
		{
			$inits = new NativeWindowInitOptions();
			$default.aplicar(InitConfig);
			
			$default.aplicarA(this);
		}
		
		/* variables del stage */
		
		private var $clase:Class;
		
		/**
		 * Indica la clase principal que sera usada en la ventana.
		 *
		 * @default Sprite
		 */
		public function get clase():Class
		{
			return $clase;
		}
		
		public function set clase(value:Class):void
		{
			$clase = existe(value) ? value : Sprite;
		}
		
		private var $contenido:DisplayObjectContainer;
		
		/**
		 * En caso de no querer que se represente en la ventana un objeto ya creado entonces establezcalo en esta propiedad, si esta propiedad es nula entonces se creara un nuevo objeto con la propeidad clase
		 *
		 * @default null
		 */
		public function get contenido():DisplayObjectContainer
		{
			return $contenido;
		}
		
		public function set contenido(value:DisplayObjectContainer):void
		{
			$contenido = value;
		}
		
		private var $modo:String;
		
		/**
		 * Indica el modo en que estara la pantalla, los valores soportados estan en la clase StageDisplayState.
		 *
		 * @default normal
		 * @param	value normal | fullScreen | fullScreenInteractive
		 */
		public function get modo():String
		{
			return $modo;
		}
		
		public function set modo(value:String):void
		{
			$modo = value;
		}
		
		private var $color:uint = 0;
		
		/**
		 * Indica el color de fondo del contenido de la ventana
		 */
		public function get color():uint
		{
			return $color;
		}
		
		public function set color(value:uint):void
		{
			$color = value;
		}
		
		private var $fotogramas:Number;
		
		/**
		 * Obtiene y establece la velocidad de fotogramas del escenario. La velocidad de fotogramas se expresa en fotogramas por segundo. De manera predeterminada, la velocidad se establece con la velocidad de fotogramas del primer archivo SWF cargado. El intervalo válido de velocidades de fotogramas es de 0,01 a 1000 fotogramas por segundo. 
		 * 
		 * <p><b>Nota:</b> es posible que una aplicación no pueda seguir configuraciones altas de velocidad de fotogramas, bien porque la plataforma de destino no sea lo suficientemente rápida o porque el reproductor esté sincronizado con la temporización en blanco vertical del dispositivo de visualización (normalmente 60 Hz en dispositivos LCD). En algunos casos, una plataforma de destino puede también optar por reducir la velocidad de fotogramas máxima si prevé un uso intensivo de la CPU.</p>
		 * 
		 * <p>Para contenido ejecutado en Adobe AIR, configurar la propiedad <code>frameRate</code> de un objeto Stage cambia la velocidad de fotogramas de todos los objetos Stage (utilizados por distintos objetos NativeWindow).</p>
		 */
		public function get fotogramas():Number
		{
			return $fotogramas;
		}
		
		public function set fotogramas(value:Number):void
		{
			$fotogramas = value;
		}
		
		private var $calidad:String;
		/**
		 * Valor de la clase StageQuality que especifica la calidad de representación que se utiliza. Los valores siguientes son válidos:
		 *
		 * <ul><li><code>StageQuality.LOW</code>: calidad de representación baja. No se suavizan ni los gráficos ni los mapas de bits, pero los motores de ejecución siguen utilizando la asignación MIP. </li>
		 * <li><code>StageQuality.MEDIUM</code>: calidad de representación media. Los gráficos se suavizan empleando una cuadrícula de 2 x 2 píxeles; el suavizado de mapas de bits depende del <code>Bitmap.smoothing</code>. Los motores de ejecución utilizan la asignación MIP. Este ajuste resulta adecuado para películas que no incluyan texto. </li>
		 * <li><code>StageQuality.HIGH</code>: calidad de representación alta. Los gráficos se suavizan empleando una cuadrícula de 4 x 4 píxeles; el suavizado de mapas de bits depende del ajuste <code>Bitmap.smoothing</code>. Los motores de ejecución utilizan la asignación MIP. Esta es la calidad de representación predeterminada que utiliza Flash Player.</li>
		 * <li><code>StageQuality.BEST</code>: calidad de representación muy alta. Los gráficos se suavizan empleando una cuadrícula de 4 x 4 píxeles. Si <code>Bitmap.smoothing</code> es <code>true</code>, el motor de ejecución utiliza un algoritmo de reducción de escala de alta calidad que produce menos defectos (sin embargo, si se utiliza <code>StageQuality.BEST</code> con <code>Bitmap.smoothing</code> establecido en <code>true</code>, se reduce considerablemente el rendimiento y no es un ajuste recomendado).</li></ul>
		 *
		 * <p>Una configuración de mayor calidad permite representar mejor mapas de bits a escala. Sin embargo, obtene más calidad implica consumir más recursos del equipo. En concreto, al representar vídeo a escala, el uso de una configuración de mayor calidad puede reducir la velocidad de fotogramas.</p>
		 *
		 * <p>El método <code>BitmapData.draw()</code> utiliza el valor de la propiedad <code>Stage.quality</code>. Como alternativa, se puede utilizar el método <code>BitmapData.drawWithQuality()</code>, que permite especificar el parámetro <code>quality</code> en el método, ignorando el valor actual de <code>Stage.quality</code>.</p>
		 *
		 * <p>En el perfil de escritorio de Adobe AIR, <code>quality</code> se puede establecer como <code>StageQuality.BEST</code> o <code>StageQuality.HIGH</code> (el valor predeterminado es <code>StageQuality.HIGH</code>). Intentar establecer cualquier otro valor no produce ningún efecto (y la propiedad no cambia). En perfil móvil de AIR, los cuatro ajustes de calidad están disponibles. El valor predeterminado en dispositivos móviles es <code>StageQuality.MEDIUM</code>.</p>
		 *
		 * <p>Para contenido ejecutado en Adobe AIR, configurar la propiedad <code>quality</code> de un objeto Stage cambia la calidad de representación de todos los objetos Stage (utilizados por distintos objetos NativeWindow).</p>
		 *
		 * <b><i>Nota:</i></b> el sistema operativo dibuja las fuentes de dispositivo, que, por consiguiente, no se ven afectadas por la propiedad <code>quality</code>.
		*/
		public function get calidad():String
		{
			return $calidad;
		}
		public function set calidad(value:String):void
		{
			$calidad = value;
		}
		
		private var $escala:String;
		/**
		 * Un valor de la clase StageScaleMode que especifica el modo de escala que debe utilizarse. Los valores siguientes son válidos:
		 *
		 * <ul><li><code>StageScaleMode.EXACT_FIT</code>: hace que esté visible toda la aplicación en el área especificada sin intentar mantener la proporción original. Puede producirse cierta distorsión y la aplicación tal vez se visualice estirada o comprimida.
		 * </li><li><code>StageScaleMode.SHOW_ALL</code>: permite ver toda la aplicación en el área especificada sin distorsión, al tiempo que mantiene la proporción original de la aplicación. Es posible que aparezcan bordes a ambos lados de la aplicación.   
		 * </li><li><code>StageScaleMode.NO_BORDER</code>: toda la aplicación completa ocupa el área especificada, sin distorsión pero quizá con algún recorte, mientras se mantiene la proporción original de la aplicación.
		 * </li><li><code>StageScaleMode.NO_SCALE</code>: la aplicación completa queda fija, de manera que permanezca sin cambios aunque cambie el tamaño de la ventana del reproductor. Puede producirse un recorte si la ventana del reproductor es más pequeña que el contenido.
		 * </li></ul>
		 */
		public function get escala():String
		{
			return $escala;
		}
		public function set escala(value:String):void
		{
			$escala = value;
		}
		
		private var $alineamiento:String;
		/**
		 * Un valor de la clase StageAlign que especifica la alineación del escenario en Flash Player o el navegador.
		 * 
		 * <p>La propiedad <code>alineamiento</code> sólo está disponible para un objeto que esté en el mismo entorno limitado de seguridad que el propietario del objeto Stage (el archivo SWF principal). Para evitar esto, el propietario de Stage puede conceder permiso al dominio del objeto que origina la llamada llamando al método <code>Security.allowDomain()</code> o <code>Security.alowInsecureDomain()</code>.</p>
		*/
		public function get alineamiento():String
		{
			return $alineamiento;
		}
		
		public function set alineamiento(value:String):void
		{
			$alineamiento = value;
		}
		
		/* Variables de ventana */
		
		private var $titulo:String;
		/**
		 * Título de la ventana.
		 * 
		 * <p>El título aparece en los controles de aspecto del sistema de la ventana, en caso de mostrarse, y en otras ubicaciones que dependen del sistema (por ejemplo, la barra de tareas).</p>
		 */
		public function get titulo():String
		{
			return $titulo;
		}
		public function set titulo(value:String):void
		{
			$titulo = value;
		}
		
		private var $ancho:Number;
		/**
		 * Anchura de la ventana, en píxeles.
		 * 
		 * <p>Las dimensiones de una ventana nativa incluyen los controles de aspecto de la ventana del sistema que aparece. La anchura del área de visualizacíon utilizable dentro de una ventana está disponible en la propiedad <code>Stage.stageWidth</code>.</p>
		 * 
		 * <p>Cambiar la propiedad <code>width</code> de una ventana equivale a cambiar la anchura mediante la propiedad <code>bounds</code>.</p>
		 * <p>Si la anchura especificada es menor o mayor que el valor de anchura máximo permitido, la anchura de la ventana se fija en el tamaño permitido más cercano. Los factores que determinan la anchura máxima y mínima son los siguientes:</p>
		 * <ul>
		 *     <li>Las propiedades <code>minSize.y</code> y <code>maxSize.y</code> del objeto Ventana</li>
		 *     <li>Los límites máximo y mínimo del sistema operativo, que son los valores de <code>NativeWindow.systemMinSize.y</code> y <code>	NativeWindow.systemMaxSize.y</code>.</li>
		 *     <li>La anchura máxima de una ventana en Adobe AIR, que es 4.095 píxeles (2.880 píxeles en AIR 1.5 y versiones anteriores).</li>
		 * </ul>
		 * <p>En Linux, establecer la propiedad <code>ancho</code> es una operación asíncrona.</p>
		 * 
		 * <p>Para detectar si se ha completado el cambio de anchura, detecte el evento <code>resize</code> que se distribuye en todas las plataformas.</p>
		 * 
		 * <p>Los valores de píxeles se redondean al entero más próximo cuando cambia el ancho de la ventana.</p>
		 */
		public function get ancho():Number
		{
			return $ancho;
		}
		public function set ancho(value:Number):void
		{
			$ancho = value;
		}
		
		private var $alto:Number;
		/**
		 * Altura de la ventana, en píxeles.
		 * 
		 * <p>Las dimensiones de una ventana nativa incluyen los controles de aspecto de la ventana del sistema que aparece. La altura del área de visualizacíon utilizable dentro de una ventana está disponible en la propiedad <code>Stage.stageHeight</code>.</p>
		 * 
		 * <p>Cambiar la propiedad <code>width</code> de una ventana equivale a cambiar la altura mediante la propiedad <code>bounds</code>.</p>
		 * <p>Si la altura especificada es menor o mayor que el valor de altura máximo permitido, la altura de la ventana se fija en el tamaño permitido más cercano. Los factores que determinan la altura máxima y mínima son los siguientes:</p>
		 * <ul>
		 *     <li>Las propiedades <code>minSize.y</code> y <code>maxSize.y</code> del objeto Ventana</li>
		 *     <li>Los límites máximo y mínimo del sistema operativo, que son los valores de <code>NativeWindow.systemMinSize.y</code> y <code>	NativeWindow.systemMaxSize.y</code>.</li>
		 *     <li>La altura máxima de una ventana en Adobe AIR, que es 4.095 píxeles (2.880 píxeles en AIR 1.5 y versiones anteriores).</li>
		 * </ul>
		 * <p>En Linux, establecer la propiedad <code>alto</code> es una operación asíncrona.</p>
		 * 
		 * <p>Para detectar si se ha completado el cambio de altura, detecte el evento <code>resize</code> que se distribuye en todas las plataformas.</p>
		 * 
		 * <p>Los valores de píxeles se redondean al entero más próximo cuando cambia el alto de la ventana.</p>
		 */
		public function get alto():Number
		{
			return $alto;
		}
		public function set alto(value:Number):void
		{
			$alto = value;
		}
		
		private var $siempreAlfrente:Boolean;
		/**
		 * Indica si la ventana siempre está delante del resto de ventanas (incluidas las de otras aplicaciones).
		 *
		 * <p>Existen dos grupos de ventanas en el orden de profundidad del sistema. Las ventanas del grupo <code>siempreAlfrente</code> siempre se muestran por delante del resto de ventanas. Se determina normalmente el orden de profundidad de las ventanas dentro del mismo grupo. Dicho de otro modo, al activar una ventana coloca delante del resto de ventanas de su grupo.</p>
		 *
		 * <p>Si cambia <code>alwaysInFront</code> de <code>false</code> a<code> true</code>, la ventana se coloca delante de todas las demás ventanas. Si cambia la propiedad de <code>true</code> a <code>false</code>, la ventana se envía detrás de todas las ventanas con estado "alwaysInFront", pero sigue delante del resto de ventanas. Si se configura la propiedad en su valor actual, no cambia el orden de profundidad de las ventanas. La configuración de la propiedad <code>alwaysInFront</code> de una ventana que tiene un propietario no tiene ningún efecto.</p>
		 *
		 * <p>La propiedad <code>siempreAlfrente</code> debe establecerse como <code>true</code> en contadas ocasiones, ya que las ventanas con esta configuración se muestran por delante de otras aplicaciones, incluso si éstas están activas.</p>
		 *
		 *<p><b>Notas sobre el comportamiento del SO:</b></p>
		 * <ul>
		 *	<li>Algunos gestores de ventanas de Linux no muestran las ventanas que tienen la propiedad <code>alwaysInFront</code> establecida en <code></code> por delante de ventanas a pantalla completa.</li>
		 * 	<li>En Mac<sup>&reg;</sup> OS X, establecer <code>alwaysInFront</code> como <code>true</code> no produce ningún efecto si la propiedad <code>displayState</code> del escenario de la ventana es <code>fullScreen</code> o <code>fullScreenInteractive</code>.</li>
		 * </ul>
		 */
		public function get siempreAlfrente():Boolean
		{
			return $siempreAlfrente;
		}
		public function set siempreAlfrente(value:Boolean):void
		{
			$siempreAlfrente = value;
		}
		
		/* DELEGATE flash.display.NativeWindowInitOptions */
		
		/**
		 * Indica si el usuario puede maximizar la ventana.
		 * 
		 * <p>En ventanas con fondo cromático del sistema, la configuración afecta a la apariencia del botón Minimizar de la ventana. También afecta a otras partes de la interfaz de usuario administrada por el sistema, como los menús de las ventanas de Microsoft Windows.</p>
		 * 
		 * <p>Si se establece como <code>false</code>, el usuario no puede maximizar la ventana. Llamar directamente al método <code>maximizar()</code> de Ventanar minimizará la ventana.</p>
		 * 
		 * <p><b>Nota:</b> algunos gestores de ventanas de Linux permiten que el usuario minimice las ventanas incluso cuando la propiedad <code>minimizable</code> se establece en <code>false</code>.</p>
		 * 
		 * <p> El valor predeterminado es <code>true.</code></p>
		*/
		public function get maximizable():Boolean
		{
			return $inits.maximizable;
		}
		public function set maximizable(value:Boolean):void
		{
			$inits.maximizable = value;
		}

		/**
		 * Indica si el usuario puede minimizar la ventana.
		 * 
		 * <p>En ventanas con fondo cromático del sistema, la configuración afecta a la apariencia del botón Minimizar de la ventana. También afecta a otras partes de la interfaz de usuario administrada por el sistema, como los menús de las ventanas de Microsoft Windows.</p>
		 * 
		 * <p>Si se establece como <code>false</code>, el usuario no puede minimizar la ventana. Llamar directamente al método <code>minimizar()</code> de Ventanar minimizará la ventana.</p>
		 * 
		 * <p><b>Nota:</b> algunos gestores de ventanas de Linux permiten que el usuario minimice las ventanas incluso cuando la propiedad <code>minimizable</code> se establece en <code>false</code>.</p>
		 * 
		 * <p> El valor predeterminado es <code>true.</code></p>
		*/
		public function get minimizable():Boolean
		{
			return $inits.minimizable;
		}
		public function set minimizable(value:Boolean):void
		{
			$inits.minimizable = value;
		}
		
		/**
		 * Especifica el objeto Ventana al que debe pertenecer cualquier ventana creada con este OpcionesVentana.
		 *
		 * Cuando una ventana tiene propietario, dicha ventana siempre se muestra delante de su propietario, está minimizada, se oculta junto con su propietario, y se cierra cuando se cierra su propietario.
		 *
		 * El valor predeterminado es null.
		 */
		public function get padre():Ventana
		{
			return AdminVentanas.deNativa($inits.owner);
		}
		
		public function set padre(value:Ventana):void
		{
			$inits.owner = existe(value) ? value.original : null;
		}
		
		/**
		 * Especifica el modo de procesamiento del objeto NativeWindow creado con este NativeWindowInitOptions.
		 * 
		 * <p>Las constantes de los valores válidos de esta propiedad se definen en la clase NativeWindowRenderMode:</p>
		 * 
		 * <p>Si no se especifica ningún valor, el valor predeterminado de <code>renderMode</code> es <code>NativeWindowRenderMode.AUTO</code>.</p>
		 * <p>El valor predeterminado es <code>NativeWindowRenderMode.AUTO.</code></p>
		 * 
		 * @param	value auto | cpu | gpu | direct
		 */
		public function get render():String
		{
			return $inits.renderMode;
		}
		
		public function set render(value:String):void
		{
			$inits.renderMode = value;
		}
		
		/**
		 * Indica si el usuario puede cambiar el tamaño de la ventana.
		 *  
		 * <p>Si se establece en <code>false</code>, el usuario no puede cambiar el tamaño de la ventana con el fondo cromático del sistema. Llamar al método <code>startResize()</code> de NativeWindow como respuesta a un evento de ratón permitirá que el usuario pueda cambiar el tamaño de la ventana. Establecer los límites de la ventana directamente también cambia el tamaño de la ventana.</p>
		 *  
		 * <p><b>Notas sobre el comportamiento del SO:</b></p><ul>
		 * <li>En sistemas operativos como Mac OS X, donde maximizar ventanas es una operación de cambio de tamaño, tanto <code>maximizable</code> como <code>resizable</code> se deben establecer en <code>false</code> para impedir que la ventana se pueda ampliar o cambiar de tamaño.</li>
		 * <li>Algunos gestores de ventanas de Linux permiten que el usuario cambie el tamaño de las ventanas incluso cuando la propiedad <code>resizable</code> se establece en <code>false</code>.</li>
		 * </ul></p>
		 * <p> El valor predeterminado es <code>true.</code></p>
		*/
		public function get resizable():Boolean
		{
			return $inits.resizable;
		}
		public function set resizable(value:Boolean):void
		{
			$inits.resizable = value;
		}
		
		/**
		 * Especifica si se suministra el fondo cromático del sistema para la ventana.
		 * 
		 * <p> Entendemos por control de aspecto los controles de la ventana que permiten al usuario definir las propiedades de escritorio de una ventana. Los controles de aspecto del sistema utilizan los controles estándar del entorno del escritorio en el que se ejecuta la aplicación de AIR y se ajustan a la apariencia del sistema operativo nativo.</p>
		 * 
		 * <p>Para utilizar los controles de aspecto del sistema proporcionados por un marco de trabajo (por ejemplo, Flex), o para crear controles de aspecto propios de la ventana, establezca <code>systemChrome</code> como <code>NativeWindowSystemChrome.NONE</code>.</p>
		 * <p>Las constantes de los valores válidos de esta propiedad se definen en la clase NativeWindowSystemChrome:</p>
		 * <ul>
		 * <li><code>NativeWindowSystemChrome.NONE</code></li>
		 * <li><code>NativeWindowSystemChrome.STANDARD</code></li>
		 * </ul>
		 * <p>Si no especifica, el valor predeterminado de <code>aspecto</code> es <code>NativeWindowSystemChrome.STANDARD</code>.</p>
		 * <p>No se puede establecer la propiedad <code>transparente</code> como <code>true</code> en ventanas con fondo cromático del sistema.</p>
		 * <p> El valor predeterminado es <code>NativeWindowSystemChrome.STANDARD.</code></p>
		 * 
		 * @param	value standart | none
		*/
		public function get aspecto():String
		{
			return $inits.systemChrome;
		}
		
		public function set aspecto(value:String):void
		{
			$inits.systemChrome = value;
		}
		
		/**
		 * Indica si la ventana admite transparencia y mezcla alfa en el escritorio.
		 * 
		 * <p>Si es <code>true</code>, la ventana se muestra superpuesta al escritorio. Las áreas de la ventana no cubiertas por un objeto de visualización, o cubiertas por objetos de visualización con configuración de alfa cercana a cero, se comportan como invisibles y no detectan eventos de ratón (que recibiría el objeto de escritorio situado debajo de la ventana). El valor alfa en el que un objeto deja de detectar eventos de ratón oscila entre 0,06 y 0,01, en función del sistema operativo.</p>
		 * 
		 * <p>No se puede establecer la propiedad <code>transparent</code> como <code>true</code> en ventanas con fondo cromático del sistema.</p>
		 * 
		 * <p><b>Nota:</b> no todos los gestores de ventanas de Linux admiten transparencia. En dichos sistemas, las áreas transparentes de una ventana se visualizan en negro. </p>
		 * 
		 * <p> El valor predeterminado es <code>false.</code></p>
		 */
		public function get transparente():Boolean
		{
			return $inits.transparent;
		}
		
		public function set transparente(value:Boolean):void
		{
			$inits.transparent = value;
		}
		
		/**
		 * Especifica el tipo de ventana que se va a crear.
		 *
		 * Las constantes de los valores válidos de esta propiedad se definen en la clase NativeWindowType:
		 * <ul>
		 * <li>NativeWindowType.NORMAL: una ventana normal. Las ventanas normales utilizan el fondo cromático completo y se muestran en la barra de tareas de de Windows o de Linux.</li>
		 * <li>NativeWindowType.UTILITY: paleta de herramientas. Las ventanas de utilidades utilizan una versión más ligera de los controles de aspecto del sistema y no se muestran en la barra de tareas de Windows.</li>
		 *<li> NativeWindowType.LIGHTWEIGHT: las ventanas ligeras no pueden tener fondo cromático del sistema y no aparecen en la barra de tareas de Windows o de Linux. Además, las ventanas ligeras no disponen de menú Sistema (Alt-Barra espaciadora) en Windows. El uso de ventanas ligeras está recomendado para mensajes emergentes de notificación o controles como cuadros emergentes áreas de visualización temporal. Si utiliza ventanas ligeras, <code>apariencia</code> debe establecerse en NativeWindowSystemChrome.NONE.</li>
		 * </ul>
		 *
		 * Si no se especifica ningún valor, el valor predeterminado de type es NativeWindowType.NORMAL.
		 *
		 * El valor predeterminado es NativeWindowType.NORMAL.
		 *
		 * @param	value normal | utility | lightweight
		 */
		public function get tipo():String
		{
			return $inits.type;
		}
		
		public function set tipo(value:String):void
		{
			$inits.type = value;
		}
		
		/**
		 * Objeto NativeWindowInitOptions creado con la configuración dada
		 */
		public function get inits():NativeWindowInitOptions
		{
			return $inits;
		}
		
		public function toString():String
		{
			return "[OpcionesVentana clase=" + clase + " contenido=" + contenido + " modo=" + modo + " color=" + color + " fotogramas=" + fotogramas + " calidad=" + calidad + " escala=" + escala + " alineamiento=" + alineamiento + " titulo=" + titulo + " ancho=" + ancho + " alto=" + alto + " siempreAlfrente=" + siempreAlfrente + " maximizable=" + maximizable + " minimizable=" + minimizable + " padre=" + padre + " render=" + render + " resizable=" + resizable + " aspecto=" + aspecto + " transparente=" + transparente + " tipo=" + tipo + "]";
		}
	}
}