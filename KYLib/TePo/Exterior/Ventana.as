﻿/*
 * Copyright (C) 2020 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita. En ningún caso, los autores serán responsables de los daños que se deriven del uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y redistribuirlo libremente, sujeto a las
 * siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar que escribiste el software original. Si utiliza este software en un producto, se agradecería un reconocimiento en la
 * documentación del producto, pero no es obligatorio. 2. Las versiones fuente modificadas deben estar claramente marcadas como tales y no deben tergiversarse como si fueran el software original. 3.
 * Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.TePo.Exterior
{
	import KYLib.C.Obj.existe;
	import KYLib.C.Utils.Opciones;
	import flash.display.StageScaleMode;
	
	import KYLib.TePo.Exterior.Admistradores.AdminVentanas;
	import flash.display.Sprite;
	import flash.display.Stage;
	import KYLib.TePo.Exterior.Admistradores.OpcionesVentana;
	import flash.display.NativeMenu;
	import flash.display.NativeWindow;
	import flash.events.IEventDispatcher;
	import KYLib.Eventos.DisparadorEventos;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Dispatched by this NativeWindow object after the window has been deactivated.
	 * @eventType	flash.events.Event.DEACTIVATE
	 */
	[Event(name = "deactivate", type = "flash.events.Event")]
	
	/**
	 * Dispatched by this NativeWindow object after the window has been activated.
	 * @eventType	flash.events.Event.ACTIVATE
	 */
	[Event(name = "activate", type = "flash.events.Event")]
	
	/**
	 * Dispatched by this NativeWindow object after the window has been closed.
	 * @eventType	flash.events.Event.CLOSE
	 */
	[Event(name = "close", type = "flash.events.Event")]
	
	/**
	 * Dispatched by this NativeWindow object immediately before the window is to be closed.
	 * @eventType	flash.events.Event.CLOSING
	 */
	[Event(name = "closing", type = "flash.events.Event")]
	
	/**
	 * Dispatched by this NativeWindow object after the window's displayState property has changed.
	 * @eventType	flash.events.NativeWindowDisplayStateEvent.DISPLAY_STATE_CHANGE
	 */
	[Event(name = "displayStateChange", type = "flash.events.NativeWindowDisplayStateEvent")]
	
	/**
	 * Dispatched by this NativeWindow object immediately before the window changes its display state.
	 * @eventType	flash.events.NativeWindowDisplayStateEvent.DISPLAY_STATE_CHANGING
	 */
	[Event(name = "displayStateChanging", type = "flash.events.NativeWindowDisplayStateEvent")]
	
	/**
	 * Dispatched by this NativeWindow object after the window has been resized.
	 * @eventType	flash.events.NativeWindowBoundsEvent.RESIZE
	 */
	[Event(name = "resize", type = "flash.events.NativeWindowBoundsEvent")]
	
	/**
	 * Dispatched by this NativeWindow object immediately before the window is to be resized on the desktop.
	 * @eventType	flash.events.NativeWindowBoundsEvent.RESIZING
	 */
	[Event(name = "resizing", type = "flash.events.NativeWindowBoundsEvent")]
	
	/**
	 * Dispatched by this NativeWindow object after the window has been moved on the desktop.
	 * @eventType	flash.events.NativeWindowBoundsEvent.MOVE
	 */
	[Event(name = "move", type = "flash.events.NativeWindowBoundsEvent")]
	
	/**
	 * Dispatched by the NativeWindow object immediately before the window is to be moved on the desktop.
	 * @eventType	flash.events.NativeWindowBoundsEvent.MOVING
	 */
	[Event(name = "moving", type = "flash.events.NativeWindowBoundsEvent")]
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase Ventana
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 * @playerversion AIR 32
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC 2020
	 *
	 * @author Juan Pablo Calle
	 */
	public class Ventana extends DisparadorEventos
	{
		
		public function get original():NativeWindow
		{
			return $ventana;
		}
		
		private var $ventana:NativeWindow;
		
		/**
		 * Constructor
		 */
		public function Ventana(opciones:OpcionesVentana, nativa:NativeWindow = null)
		{
			if (existe(nativa))
			{
				$ventana = nativa;
				return;
			}
			$ventana = new NativeWindow(opciones.inits);
			configVentana(opciones);
		}
		
		private function configVentana(opciones:OpcionesVentana):void
		{
			$ventana.alwaysInFront = opciones.siempreAlfrente;
			$ventana.stage.displayState = opciones.modo;
			$ventana.stage.quality = opciones.calidad;
			$ventana.stage.align = opciones.alineamiento;
			$ventana.stage.scaleMode = opciones.escala;
			$ventana.width = opciones.ancho;
			$ventana.height = opciones.alto;
			/* $ventana.stage.stageHeight = opciones.alto; $ventana.stage.stageWidth = opciones.ancho; */
			$ventana.stage.addChild(existe(opciones.contenido) ? opciones.contenido : new opciones.clase());
			$ventana.title = opciones.titulo;
		}
		
		/* DELEGATE flash.display.NativeWindow */
		
		public function activar():void
		{
			$ventana.activate();
		}
		
		public function get activa():Boolean
		{
			return $ventana.active;
		}
		
		public function get siempreAlfrente():Boolean
		{
			return $ventana.alwaysInFront;
		}
		
		public function set siempreAlfrente(value:Boolean):void
		{
			$ventana.alwaysInFront = value;
		}
		
		public function get limites():Rectangle
		{
			return $ventana.bounds;
		}
		
		public function set limites(value:Rectangle):void
		{
			$ventana.bounds = value;
		}
		
		public function cerrar():void
		{
			AdminVentanas.cerrar(this);
		}
		
		public function get cerrada():Boolean
		{
			return $ventana.closed;
		}
		
		public function get display():String
		{
			return $ventana.displayState;
		}
		
		public function get alto():Number
		{
			return $ventana.height;
		}
		
		public function set alto(value:Number):void
		{
			$ventana.height = value;
		}
		
		public function ventanas():Vector.<Ventana>
		{
			var dev:Vector.<Ventana> = new Vector.<Ventana>();
			var o:Vector.<NativeWindow> = $ventana.listOwnedWindows();
			
			for each (var w:NativeWindow in o)
			{
				dev.push(AdminVentanas.deNativa(w));
			}
			return dev;
		}
		
		public function get maximizable():Boolean
		{
			return $ventana.maximizable;
		}
		
		public function maximizar():void
		{
			$ventana.maximize();
		}
		
		public function get maxSize():Point
		{
			return $ventana.maxSize;
		}
		
		public function set maxSize(value:Point):void
		{
			$ventana.maxSize = value;
		}
		
		public function get menu():NativeMenu
		{
			return $ventana.menu;
		}
		
		public function set menu(value:NativeMenu):void
		{
			$ventana.menu = value;
		}
		
		public function get minimizable():Boolean
		{
			return $ventana.minimizable;
		}
		
		public function minimizar():void
		{
			$ventana.minimize();
		}
		
		public function get minSize():Point
		{
			return $ventana.minSize;
		}
		
		public function set minSize(value:Point):void
		{
			$ventana.minSize = value;
		}
		
		public function notificarUsuario(type:String):void
		{
			$ventana.notifyUser(type);
		}
		
		public function ponerDetrasDe(window:Ventana):Boolean
		{
			return $ventana.orderInBackOf(window.$ventana);
		}
		
		public function ponerDelanteDe(window:Ventana):Boolean
		{
			return $ventana.orderInFrontOf(window.$ventana);
		}
		
		public function ponerDetras():Boolean
		{
			return $ventana.orderToBack();
		}
		
		public function ponerDelante():Boolean
		{
			return $ventana.orderToFront();
		}
		
		public function get padre():Ventana
		{
			return existe($ventana.owner) ? AdminVentanas.deNativa($ventana.owner) : null;
		}
		
		public function get render():String
		{
			return $ventana.renderMode;
		}
		
		public function get resizable():Boolean
		{
			return $ventana.resizable;
		}
		
		public function restaurar():void
		{
			$ventana.restore();
		}
		
		public function get stage():Stage
		{
			return $ventana.stage;
		}
		
		public function mover():Boolean
		{
			return $ventana.startMove();
		}
		
		public function dimensionar(edgeOrCorner:String = "BR"):Boolean
		{
			return $ventana.startResize(edgeOrCorner);
		}
		
		public function get aspecto():String
		{
			return $ventana.systemChrome;
		}
		
		public function get titulo():String
		{
			return $ventana.title;
		}
		
		public function set titulo(value:String):void
		{
			$ventana.title = value;
		}
		
		public function get transparente():Boolean
		{
			return $ventana.transparent;
		}
		
		public function get tipo():String
		{
			return $ventana.type;
		}
		
		public function get visible():Boolean
		{
			return $ventana.visible;
		}
		public function set visible(value:Boolean):void
		{
			$ventana.visible = value;
		}
		
		public function get ancho():Number
		{
			return $ventana.width;
		}
		public function set ancho(value:Number):void
		{
			if ($ventana.stage.scaleMode == StageScaleMode.NO_SCALE) 
			{
				$ventana.stage.stageWidth == value;
				return;
			}
			$ventana.width = value;
		}
		
		/**
		 * Coordenada del eje horizontal de la esquina superior izquierda de la ventana con relación al origen del escritorio del sistema operativo.
		 *
		 * En sistemas con más de un monitor, x puede ser un valor negativo. Si guarda el valor, tal vez para que una ventana regrese a su posición anterior, siempre debe verificar que la ventana se
		 * coloca en una ubicación válida al recolocarla. Los cambios de resolución de pantalla o la disposición de los monitores puede hacer que la ventana quede fuera de la pantalla. Utilice la
		 * clase Screen para obtener información sobre la geometría del escritorio.
		 *
		 * Cambiar la propiedad x de una ventana equivale a cambiar la ubicación mediante la propiedad bounds.
		 *
		 * En Linux, establecer la propiedad x es una operación asíncrona.
		 *
		 * Para detectar si se ha completado el cambio de posición, detecte el evento move que se distribuye en todas las plataformas.
		 *
		 * Los valores de píxeles se redondean al entero más próximo cuando cambia la coordenada x de la ventana.
		 */
		public function get x():Number
		{
			return $ventana.x;
		}
		
		public function set x(value:Number):void
		{
			$ventana.x = value;
		}
		
		/**
		 * Coordenada del eje vertical de la esquina superior izquierda de la ventana con relación a la esquina superior izquierda del escritorio del sistema operativo.
		 *
		 * En sistemas con más de un monitor, y puede ser un valor negativo. Si guarda el valor, tal vez para que una ventana regrese a su posición anterior, siempre debe verificar que la ventana se
		 * coloca en una ubicación válida al recolocarla. Los cambios de resolución de pantalla o la disposición de los monitores puede hacer que la ventana quede fuera de la pantalla. Utilice la
		 * clase Screen para obtener información sobre la geometría del escritorio.
		 *
		 * Cambiar la propiedad y de una ventana equivale a cambiar la ubicación mediante la propiedad bounds.
		 *
		 * En Linux, establecer la propiedad y es una operación asíncrona.
		 *
		 * Para detectar si se ha completado el cambio de posición, detecte el evento move que se distribuye en todas las plataformas.
		 *
		 * Los valores de píxeles se redondean al entero más próximo cuando cambia la coordenada y de la ventana.
		 */
		public function get y():Number
		{
			return $ventana.y;
		}
		
		public function set y(value:Number):void
		{
			$ventana.y = value;
		}
		
		override public function toString():String
		{
			return "[Ventana" + ' titulo="' + titulo + '" visible="' + visible + "]";
		}
	
	}
}