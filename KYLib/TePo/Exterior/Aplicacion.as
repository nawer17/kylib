﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
package KYLib.TePo.Exterior
{
	import KYLib.C.Obj.existe;
	
	import flash.display.NativeMenu;
	import KYLib.C.esencial;
	import KYLib.Errores.ErroresClases.ErrorClaseAbstracta;
	import KYLib.Errores.KYlibError;
	import KYLib.Errores.ErroresClases.*;
	import flash.desktop.InteractiveIcon;
	import flash.desktop.NativeApplication;
	import flash.display.NativeWindow;
	import flash.events.InvokeEvent;

	//--------------------------------------
	//	Descripción de la clase
	//--------------------------------------
	/**
	 * La clase Aplicacion
	 * 
	 * @langversion 3.0
	 *
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 *  @productversion Animate CC
	 * 
	 * @author Tabatha(kokopai)
	 */
	public final class Aplicacion
	{
		
		/**
		 * @private
		 */
		public function Aplicacion()
		{
			ErrorClaseAbstracta.agregar(this);
			super();
		}
		
		/// La instancia de aplicación
		private static var $instancia:NativeApplication;
		
		/**
		 * Crea el objeto Aplicacion que contiene las propiedades de la aplicación de AIR.
		 *
		 * @return Devuelve <code>true</code> si se crea la instancia de aplicación o <code>false</code> si no se crea.
		 */
		public static function crearInstancia(): Boolean
		{
			if (existe($instancia)) 
			{
				return false;
			}
			if (esencial.enAIR)
			{
				$instancia = NativeApplication.nativeApplication;
				return true;
			}
			
			return false;
		}
		
		public static function onInvoke(f:Function):Boolean 
		{
			if (!$instancia.hasEventListener(InvokeEvent.INVOKE)) 
			{
				$instancia.addEventListener(InvokeEvent.INVOKE, f, false, 100);
				return true;
			}
			return false
		}
		
		/* DELEGATE flash.desktop.NativeApplication */
		
		static public function activate(window:NativeWindow = null):void 
		{
			$instancia.activate(window);
		}
		
		static public function get activeWindow():NativeWindow 
		{
			return $instancia.activeWindow;
		}
		
		static public function get applicationDescriptor():XML 
		{
			return $instancia.applicationDescriptor;
		}
		
		static public function get ID():String 
		{
			return $instancia.applicationID;
		}
		
		static public function get autoExit():Boolean 
		{
			return $instancia.autoExit;
		}
		
		static public function set autoExit(value:Boolean):void 
		{
			$instancia.autoExit = value;
		}
		
		static public function clear():Boolean 
		{
			return $instancia.clear();
		}
		
		static public function copy():Boolean 
		{
			return $instancia.copy();
		}
		
		static public function cut():Boolean 
		{
			return $instancia.cut();
		}
		
		static public function get executeInBackground():Boolean 
		{
			return $instancia.executeInBackground;
		}
		
		static public function set executeInBackground(value:Boolean):void 
		{
			$instancia.executeInBackground = value;
		}
		
		static public function exit(errorCode:int = 0):void 
		{
			$instancia.exit(errorCode);
		}
		
		static public function getDefaultApplication(extension:String):String 
		{
			return $instancia.getDefaultApplication(extension);
		}
		
		static public function get icon():InteractiveIcon 
		{
			return $instancia.icon;
		}
		
		static public function get idleThreshold():int 
		{
			return $instancia.idleThreshold;
		}
		
		static public function set idleThreshold(value:int):void 
		{
			$instancia.idleThreshold = value;
		}
		
		static public function get isCompiledAOT():Boolean 
		{
			return $instancia.isCompiledAOT;
		}
		
		static public function isSetAsDefaultApplication(extension:String):Boolean 
		{
			return $instancia.isSetAsDefaultApplication(extension);
		}
		
		static public function get menu():NativeMenu 
		{
			return $instancia.menu;
		}
		
		static public function set menu(value:NativeMenu):void 
		{
			$instancia.menu = value;
		}
		
		static public function get ventanasAbiertas():Array 
		{
			return $instancia.openedWindows;
		}
		
		static public function paste():Boolean 
		{
			return $instancia.paste();
		}
		
		static public function get publisherID():String 
		{
			return $instancia.publisherID;
		}
		
		static public function removeAsDefaultApplication(extension:String):void 
		{
			$instancia.removeAsDefaultApplication(extension);
		}
		
		static public function get runtimePatchLevel():uint 
		{
			return $instancia.runtimePatchLevel;
		}
		
		static public function get runtimeVersion():String 
		{
			return $instancia.runtimeVersion;
		}
		
		static public function selectAll():Boolean 
		{
			return $instancia.selectAll();
		}
		
		static public function setAsDefaultApplication(extension:String):void 
		{
			$instancia.setAsDefaultApplication(extension);
		}
		
		static public function get startAtLogin():Boolean 
		{
			return $instancia.startAtLogin;
		}
		
		static public function set startAtLogin(value:Boolean):void 
		{
			$instancia.startAtLogin = value;
		}
		
		static public function get systemIdleMode():String 
		{
			return $instancia.systemIdleMode;
		}
		
		static public function set systemIdleMode(value:String):void 
		{
			$instancia.systemIdleMode = value;
		}
		
		static public function get timeSinceLastUserInput():int 
		{
			return $instancia.timeSinceLastUserInput;
		}
		
		
		
	}
}