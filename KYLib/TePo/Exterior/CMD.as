﻿ /*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Exterior 
{

	import KYLib.C.Mat.*;
	import KYLib.C.Obj.existe;
	import KYLib.C.Utils.GLOBAL;
	import KYLib.C.Utils.clases;
	import KYLib.C.Utils.fecha;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	
	import KYLib.Errores.*;
	import KYLib.Errores.ErroresClases.*;
	import KYLib.Errores.TePo.ErroresExteriores.*;
	import KYLib.Eventos.TePo.EventosExteriores.*;
	import KYLib.TePo.ByteLevel.*;
	import flash.desktop.*;
	import flash.events.*;
	//import flash.filesystem.*;
	import flash.utils.*;
		
	/**
	 * Disparado cuando se termina de construir el archivo del comando completo.
	 */
	[Event(name = "EventoCMD.CONSTRUIDO", type = "KYLib.Eventos.TePo.EventosExteriores.EventoCMD")]
	
	/**
	 * Disparado cuando se obtiene una respuesta del proceso del CMD.
	 */
	[Event(name = "EventoCMD.RESPUESTA", type = "KYLib.Eventos.TePo.EventosExteriores.EventoCMD")]
	
	/**
	 * Disparado cuando se termina la ejecución del CMD.
	 */
	[Event(name = "EventoCMD.SALIDA", type = "KYLib.Eventos.TePo.EventosExteriores.EventoCMD")]
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase CMD es exclusiva de windows y se usa para ejecutar comandos de la consola de windows, los comandos pueden ser ejecutados en una conola visible pero tambien se puede poner invisible con la propiedad estatica <code>visible</code>.
	 * 
	 * 
	 *
	 * @langversion 3.0
	 *
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Kokopai
	 */
	public final class CMD extends EventDispatcher
	{
		[Embed(source="../../../exes KYLib/KTE.CMD.exe", mimeType="application/octet-stream")]
		/// Este es el ejecutable que se usa para cominucarse con los procesos.
		private static const $executableClass:Class;
		
		/// El numero de cmd creado
		private static var $cmdNo:uint = 1;
		
		//
		// Sesion de instancias
		//
		
		/// El directorio del ejecutable
		private var $executableDir:File;
		
		/// apunta al directorio en el que se trabaja actualmente
		private var $trabajoDir:File;
		
		/// Este archivo file apunta al archivo ejecutable
		private var $batchDir:File;
		
		/// El nombre del archivo ejecutable
		private var $nombre:String = "";
		
		/// el file stream que se usa para escribir
		private const $fs:FileStream = new FileStream();
		
		/// Almacena la data que sera escrita en el archivo bat
		private const $data:ConjuntoBytes = new ConjuntoBytes();
		
		/**
		 * Crea una nueva instancia de CMD.
		 * <span>El archivo se crea originalmente con dos sentencias:</span>
		 * <ul>
		 * <li>Una sentencia "@echo off" para cancelar el eco.</li>
		 * <li>Una sentencia "CD" con el directorio de trabajo inicial.</li>
		 * </ul>
		 * 
		 * @param DirectorioTrabajo Es el directorio en el cual se ejecutara archivo de comandos.
		*/
		public function CMD(DirectorioTrabajo:String = null) 
		{
			ErrorClaseAIR.agregar(this);
			super();
			
			$nombre += String(fecha().date);
			$nombre += String(fecha().month+1);
			$nombre += String(fecha().fullYear);
			$nombre += "-";
			$nombre += String(fecha().hours);
			$nombre += String(fecha().minutes);
			$nombre += String(fecha().seconds);
			$nombre += String(fecha().milliseconds);
			$nombre += "-";
			$nombre += String(random.generar(12, 1216));
			
			//se contruyen los apuntadores
			try 
			{
				$trabajoDir = new File(DirectorioTrabajo);
				if ($trabajoDir.exists)
				{
					if (!$trabajoDir.isDirectory)
					{
						$trabajoDir = File.applicationDirectory;
					}
				}
				else 
				{
					$trabajoDir = File.applicationDirectory;
				}
			}
			catch (err:Error)
			{
				$trabajoDir = File.applicationDirectory;
			}
			
			$executableDir = GLOBAL.runtime.KYLib.TePo.Exterior.CMD.tempDir.resolvePath(String($nombre + "/KTE.CMD." + CMD.$cmdNo++ + ".exe"));
			$fs.open($executableDir, "write");
			$fs.writeBytes(new CMD.$executableClass());
			$fs.close();
			$batchDir = GLOBAL.runtime.KYLib.TePo.Exterior.CMD.tempDir.resolvePath($nombre + "/call.bat");
			
			$configProceso.workingDirectory = $batchDir.parent;
			
			$proceso.addEventListener("standardOutputData"/*ProgressEvent.STANDARD_OUTPUT_DATA*/, on$procesoStandardOutputData);
			$proceso.addEventListener("standardErrorData"/*ProgressEvent.STANDARD_ERROR_DATA*/, on$procesoStandardErrorData);
			$proceso.addEventListener("exit"/*NativeProcessExitEvent.EXIT*/, on$procesoExit);
			$fs.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, on$fsOutputProgress);
			
			agregar(String("CD \"" + $trabajoDir.nativePath + "\""));
		}
		
		/// Aqui se almacenan las lineas agregadas
		private const $lineas:Vector.<String> = new Vector.<String>();
		
		/**
		 * Obtiene la linea especificada del conjunto de sentencias.
		 * 
		 * @param	i El indice de la linea que se quiere recuperar.
		 * 
		 * @return	Devuelve el contenido de la linea.
		 */
		public function getLinea(i:uint):String 
		{
			if (i >= $lineas.length) 
			{
				return $lineas[$lineas.length - 1];
			}
			else if (i <= 1) 
			{
				return $lineas[1];
			}
			else 
			{
				return $lineas[i];
			}
			return null;
		}
		
		/**
		 * Establece una linea existente o agrega una al final de la secuencia de comandos a ejecutar.
		 * 
		 * @param	i El indice de la linea quer se quiere editar, si el indice aun no esta definido entonces se agregara una nueva linea. En caso de que el indice sea menor o igual a 1 se editara la primera linea.
		 * @param	sentencia El contenido nuevo por el que se reemplazara el actual.
		 * 
		 * @return Devuelve la cadena que ha sido agregada, si el valor pasado en el parametro sentencia tenia saltos de linea el valor devuelto por esta funcion sera el contenido de la cadena de texto antes del primer salto de linea.
		 */
		public function setLinea(i:uint, sentencia:String):String
		{
			// Se le quitan los saltos de linea a la sentencia
			var comando:String = quitarSaltosLinea(sentencia);
			
			// Se mira si el indice es de una linea aun no existente
			if (i >= ($lineas.length)) 
			{
				// como es una linea no existente entonces se agrega una nueva
				$lineas.push(comando);
			}
			// se mira si el indice es menor o igual a 1
			else if (i <= 1) 
			{
				// se establece la linea 1
				$lineas[1] = comando;
			}
			// se paso un indice valido
			else
			{
				// se cambia la linea
				$lineas[i] = comando;
			}
			
			return comando;
		}
		
		/**
		 * Agrega una linea al archivo .bat que sera ejecutado.
		 * <span>Es posible agregar varias sentencias con un solo llamado a este metodo separandolas por saltos de linea <code>\n</code>.</span>
		 * 
		 * @param	sentencia La sentencia o grupo de sentencias que seran agregados al archivo batch. No se permiten saltos de linea, si se encuentra un salto de linea todo despues de el sera ignorado.
		 */
		public function agregar(sentencia:String):void 
		{
			$lineas.push(quitarSaltosLinea(sentencia));
		}
		
		/// Guarda el valor de la variable disponible
		private var $disponible:Boolean = false;
		
		/**
		 * Indica si esta disponible el archivo del comando.
		 * <span>En caso de estar disponible se podra ejecutar el CMD, de lo contrario no se podra ejecutar.</span>
		 * 
		 * <p>Si usted construyo el CMD pero esta propiedad sigue devolviendo <code>false</code> es porque una aplicacion externa a liminado el archivo asignado a este CMD, si esto ocurre se debe de generar otra instancia.</p>
		 */
		public function get disponible():Boolean 
		{
			return ($disponible && $batchDir.exists && $executableDir.exists);
		}
		
		/**
		 * Construye el archivo con todas las lineas que han sido agregadas.
		 * 
		 * <p>No se recomienda llamar varias veces seguidas a esta funcion ya que puede generar un error en la escritura, llamela solo si ha realizado cambios en las lineas de comandos o si desa construir el archivo por primera vez.</p>
		 */
		public function construir():void
		{
			$disponible = false;
				
			$data.clear();
			
			for each (var i:String in $lineas) 
			{
				$data.writeUTFBytes(i);
				$data.writeByte(0x0a);
			}
			
			if ($batchDir.exists) 
			{
				eliminarArchivoBatch();
			}
			
			$fs.openAsync($batchDir, "write");
			$fs.writeBytes($data);
		}
		
		/// elimina el archivo del batch
		private function eliminarArchivoBatch():void 
		{
			try 
			{
				$batchDir.deleteFile();
			}
			catch (err:Error)
			{
				eliminarArchivoBatch();
			}
		}
		
		/// se ejecuta cuando se escribe la data ene l archivo
		private function on$fsOutputProgress(e:OutputProgressEvent):void 
		{
			if (e.bytesPending && $batchDir.size != $data.length) 
			{
				return;
			}
			$fs.close();
				
			$disponible = true;
				
			dispatchEvent(new EventoCMD(EventoCMD.CONSTRUIDO));
		}
		
		/// Valida que el comando no tenga saltos de linea, en caso de tenerlos los quita
		private function quitarSaltosLinea(sentencia:String):String
		{
			var comando:String = sentencia;
			if (sentencia.search(/\n/) >= 0)
			{
				comando = sentencia.substring(0, sentencia.search(/\n/));
			}
			return comando;
		}
		
		//
		// A partir de aqui es la zona de comunicacion con el cmd
		//
		
		/**
		 * Indica si el CMD se esta ejecutando en este momento.
		 */
		public function get ejecutando():Boolean 
		{
			return $proceso.running;
		}
		
		/// El proceso que iniciara al cmd
		private const $proceso:NativeProcess = new NativeProcess();
		
		
		/// la configuracion del proceso
		private const $configProceso:NativeProcessStartupInfo = new NativeProcessStartupInfo();
		
		/// El error que es disparado cuando ya se esta ejecutando el archivo
		private const $errorEjecutando:ErrorCMD = new ErrorCMD(ErrorCMD.EJECUTANDO);
		
		/// Error que se dispara cuando no esta disponlibe el CMD
		private const $errorNoDisponible:ErrorCMD = new ErrorCMD(ErrorCMD.NODISPONIBLE);
		
		/**
		 * Ejecuta el archivo por lotes.
		 * 
		 * @throws 
		 */
		public function ejecutar():void
		{
			if (disponible) 
			{
				if ($proceso.running) 
				{
					KYlibError.lanzar($errorEjecutando);
					return;
				}
				if (!existe($configProceso.executable)) 
				{
					$configProceso.executable = $executableDir;
				}
				
				$respuestaFinal = "";
				
				$proceso.start($configProceso);
			}
		}
		
		/**
		 * Cierra el CMD si se esta ejecutando en este momento.
		 * 
		 * @param	forzar Indica si se intentara cerrar el CMD a la fuerza, cuando el valor es <code>false</code> se intenta cerrar el proceso pacificamente por lo que podria no cerrarse en algunos casos. 
		 */
		public function salir(forzar:Boolean = false):void
		{
			if ($proceso.running) 
			{
				$proceso.exit(forzar);
			}
		}
		
		/// se ejecuta cuando se cierra el proceso
		private function on$procesoExit(e:Object/*NativeProcessExitEvent*/):void 
		{			
			dispatchEvent(new EventoCMD(EventoCMD.SALIDA, false, false, $respuestaFinal, e.exitCode));
		}
		
		/// Es la ultima respuesta que se ha obtenido
		private var $respuesta:String;
		
		/// guarda la respuesta final, es decir, la concatenacion de todas las respuestas anteriores
		private var $respuestaFinal:String;
		
		/// se ejecuta cuando hay data por leer en el flujo de error
		private function on$procesoStandardErrorData(e:ProgressEvent):void 
		{
			$respuesta = "ERROR - " + $proceso.standardError.readUTFBytes($proceso.standardError.bytesAvailable);
			
			$respuestaFinal += $respuesta;
		}
		
		/// se ejecuta cuando se obtiene una respuesta del proceso
		private function on$procesoStandardOutputData(e:ProgressEvent):void 
		{			
			$respuesta = $proceso.standardOutput.readUTFBytes($proceso.standardOutput.bytesAvailable);
			
			$respuestaFinal += $respuesta;
			
			dispatchEvent(new EventoCMD(EventoCMD.RESPUESTA, false, false, $respuesta));
		}
	}
}