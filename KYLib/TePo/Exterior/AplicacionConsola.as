﻿/*
 * Copyright (C) 2020 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Exterior 
{

	import KYLib.C.Obj.*;
	import KYLib.C.Utils.*;
	import KYLib.TePo.Archivos.*;
	import KYLib.TePo.Exterior.Admistradores.*;
	import fl.controls.*;
	import flash.desktop.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filesystem.*;
	import flash.text.*;
	import flash.ui.*;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase AplicacionConsola representa la clase base para programas que quieres ser solo por consola, al heredar esta clase puede aggregar nuevos comandos o simplemente quitar los que tiene la aplicacion por defecto
	 *
	 * @langversion 3.0
	 *
	 * @playerversion AIR 32
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC 2020
	 *
	 * @author Juan Pablo Calle
	 */
	public dynamic class AplicacionConsola extends Sprite
	{
		//{ region variables
		//
		// Variables de almacenamiento, son las que guardan todo, comandos, funciones y variables
		//
		/// Guarda todos los comandos de la aplicacion
		private const $comandos:Object = { };
		/// Guarda todas las funciones definidas
		private const $funciones:Object = { };
		/// Guarda todas las variables
		private const $variables:Object = { };
		
		//
		// Campos de texto
		//
		/// Campo de salida de texto
		public const $output:TextArea = new TextArea();
		/// Campo de entrada de texto
		public const $input:TextInput = new TextInput();
		
		//
		// Variables de configuracion, pueden ser todas cambiadas con el comando
		//
		///Indica si al escribir un comando este sera tambien escrito en la pantalla de salida
		protected var $mostrarComando:Boolean = true;
		
		private var $tempParche:Parche;
		
		//
		// Expresiones regulares para buscar comandos y demas
		//
		/// regExp usada para separar entre cadenas y no cadenas al ingrsar un comando
		private const $parametros:RegExp = /"((?!").)+"|\S+\(.+\)|\S+/;
		/// regExp usada para verificar si un parametro es una cadena y ademas para obtener su valor sin las comillas
		private const esCadena:RegExp = /^"(.+)"$/
		/// regExp usada para varificar si una cadena corresponde a un numero
		private const esNumero:RegExp = /^\d+\.?\d*$/;
		/// regExp usada para mirar si un parametro es una funcion
		private const regFuncion:RegExp = /\S+\(.*\)/;
		/// regExp que se usa en las funciones para eliminar la funcion y los paraentesis para que solo queden los parametros
		private const sacarNombreFuncion:RegExp = /^\S+?\(|\)$/g;
		/// regExp usada para obtener el nombre de una funcion
		private const nombreFuncion = /(^((?!\()\S)+)\(/;
		
		//
		// Variables modificables
		//
		/// Es el mensaje que se encuantra en la parte superior de la consola
		protected var mensajeInicial:String = "Aplicacion de consola\n";
		/// El caracter o conjunto de caracteres que esta siempre al comienzo de una linea de comando
		protected var prefix:String = "$";
		
		/// directorio de trabajo de la aplicación
		protected var directorio:Parche = new Parche(File.applicationDirectory.nativePath, false);
		
		private static var $opcionesVentana:OpcionesVentana = new OpcionesVentana({
			clase:AplicacionConsola,
			calidad:StageQuality.LOW
		});
			
		public static function get padre():Ventana
		{
			return $opcionesVentana.padre;
		}
		
		public static function set padre(value:Ventana):void
		{
			$opcionesVentana.padre = value;
		}
		
		//} endregion
		
		//
		// {region funciones principales
		//
		
		/**
		 * Crea una nueva aplicacion de consolacon los comandos por defecto.
		 */
		public function AplicacionConsola() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onLoad);
		}
		
		/**
		 * Metodo principal de configuración, agrega los comandos y funciones iniciales y configura la parte visual
		 */
		private function config():void 
		{
			registrarFuncion("trace", outrace);
			registrarFuncion(">", mayorque);
			
			registrarComando("exit", cerrarPrograma);
			registrarComando("close", cerrarVentana);
			registrarComando("salir", cerrarPrograma);
			registrarComando("cerrar", cerrarVentana);
			registrarComando("say", say);
			registrarComando("decir",say);
			registrarComando("var", variable);
			registrarComando("dir", dir);
			registrarComando("create", crear);
			registrarComando("crear", crear);
			registrarComando("asc",asc);
			
			$input.y = stage.stageHeight - $input.height;
			$input.width = stage.stageWidth;
			$output.width = stage.stageWidth;
			$output.height = $input.y;
			$output.editable = false;
			
			var tf:TextFormat = $output.textField.getTextFormat();
			tf.color = 0xffffff;
			$output.setStyle("textFormat", tf);
			$input.setStyle("textFormat", tf);
			
			$input.addEventListener(KeyboardEvent.KEY_DOWN, mandarComando);
			stage.addEventListener(Event.RESIZE, onStageResize)
			
			addChild($output);
			addChild($input);
			out(mensajeInicial);
			
			
		}
		
		private function onStageResize(e:Event):void 
		{
			$input.y = stage.stageHeight - $input.height;
			$input.width = stage.stageWidth;
			$output.width = stage.stageWidth;
			$output.height = $input.y;
		}
		
		private function out(...mensaje):void 
		{
			$output.text += prefix + String(mensaje) + "\n";
			$output.verticalScrollPosition = $output.maxVerticalScrollPosition;
		}
		
		//}endregion
		
		//
		//{ region eventos
		//
		/**
		 * Se ejecuta cuando la consola se carga en pantalla
		 */
		private function onLoad(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onLoad);
			
			config();
		}
		
		//} endregion
		
		//
		// {region Comandos
		//
		
		private function cerrarVentana():void 
		{
			AdminVentanas.deNativa(stage.nativeWindow).cerrar();
		}
		
		private function asc():void 
		{
			AdminVentanas.crear($opcionesVentana).visible = true;
		}
		
		/**
		 * Comando var, crea una variable
		 * Tambien es posble crear variables con la funcion set()
		 */
		public function variable(nombre:String,tipo:String,valor:String):void
		{
			var clase:Class;
			if (tipo == "new") 
			{
				clase = clases.deCadena(valor);
				if (existe(clase)) 
				{
					$variables[nombre] = new clase();
				}
				else
				{
					throw 'La clase especificada no existe por lo que la variable no fue creada';
				}
			}
			else 
			{
				clase = clases.deCadena(tipo);
				if (tipo == "null")  
				{
					$variables[nombre] = calcularVariable(valor);
				}
				else if (existe(clase))
				{
					if (clase == Boolean) 
					{
						$variables[nombre] = existe(calcularVariable(valor));
					}
					else 
					{
						$variables[nombre] = clase(calcularVariable(valor));
					}
				}
				else
				{
					throw 'La clase especificada no existe por lo que la variable no fue creada';
				}
			}
		}
		
		private function dir():void 
		{
			out(directorio.parcheNativo);
		}
		
		private function say(...toSay):void 
		{
			out(calcularVariables(toSay));
		}
		
		private function crear(tipo:String,nombre:String):void 
		{
			nombre = esCadena.test(nombre) ? esCadena.exec(nombre)[1] : nombre;
			try 
			{
				switch (tipo) 
				{
					case "dir":
					{
						this["tempParche"]=directorio.resolverParche(nombre);
						this["tempParche"].createDirectory();
						delete this["tempParche"];
						break;
					}
					default:
					{
						throw 'El primer parametro "'+tipo+'" no es un parametro aceptado';
					}
				}
			}
			catch (err:SecurityError)
			{
				out("Error: No se tienen permisos de escribir en el directorio");
			}
		}
		
		// } endregion
		
		//
		// {region Funciones
		//
		/**
		 * funcion trace(), escribe lo que se le pase en la pantalla
		 */
		private function outrace(...args):String 
		{
			return String(calcularVariables(args));
		}
		
		private function mayorque(numero1:Number,numero2:Number):Boolean 
		{
			return numero1 > numero2;
		}
		
		//}enderion
		
		//
		// {region control comandos
		//
		
		/**
		 * Se ejecuta cuando el usuario manda el comando pulsando la tecla enter
		 */
		private function mandarComando(e:KeyboardEvent):void 
		{
			try 
			{
				if (e.keyCode == Keyboard.ENTER) 
				{
					if ($mostrarComando) 
					{
						out("? "+$input.text);
					}
					var args:Array = [];
					var regRes:Array;
					var texto:String = $input.text;
					$input.text = "";
					while (existe(regRes = $parametros.exec(texto))) 
					{
						args.push(regRes[0]);
						texto = texto.replace(regRes[0],"");
					}
					
					var nombreComando:String = args.shift();
					if (!esFuncion(nombreComando)) 
					{
						for(var i in args) 
						{
							if (esFuncion(args[i])) 
							{
								args[i] = calcularFuncion(args[i]);
							}
						}
						comando(nombreComando,args);
					}
					else
					{
						if (args.length > 0) 
						{
							throw 'Para pasar parametros a una función debe de hacerlo dentro de los parentesis, no coloque nada luego de el llamado a la función';
						}
						else
						{
							throw calcularFuncion(nombreComando);
						}
					}
				}
			}
			catch (err)
			{
				out(err);
			}
		}
		
		/**
		 * Ejecuta un comando cuyo nombre es el primer argumento de un arreglo de parametros
		 * 
		 * @param	argumentos El arreglo que contiene el nombre del comando y sus parametros
		 */
		protected function comando(nombreComando:String,argumentos:Array):void
		{
			if (!existe(nombreComando)) 
			{
				return;
			}
			
			if (existe($comandos[nombreComando]))
			{
				$comandos[nombreComando].apply(this,argumentos);
			}
			else 
			{
				throw 'El comando "'+nombreComando+'" no esta definido o esta mal escrito, recuerda usar solo minusculas';
			}
		}
		
		/**
		 * Registra un comando nuevo
		 * 
		 * @param	nombre el nombre del comando, sera convertido en minusculas siempre
		 * @param	funcion la funcion que ejecuta el comando, debe ser una función que reciva un parametro de tipo ...rest
		 */
		protected function registrarComando(nombre:String, funcion:Function):void 
		{
			$comandos[nombre.toLowerCase()] = funcion;
		}
		
		/**
		 * Borra un comando de la lista de comandos
		 * @param	nombre El nombre del comando a borrar
		 */
		protected function borrarComando(nombre:String):void 
		{
			$comandos[nombre.toLowerCase()] = null;
		}
		
		//}endregion
		
		//
		// {region control funciones
		//
		
		/**
		 * Calcula el valor de una funcion, si la funcion tiene funciones como parametros entonces las ejecuta recursivamente hasta poder continuar con las funcion principal
		 * 
		 * @param	cadena la funcion y sus parametros, esta cadena es parseada por el metodo mandarComando
		 * @return
		 */
		private function calcularFuncion(cadena:String):*
		{
			var args:Array = [];
			var regRes:Array;
			var texto:String = cadena.replace(sacarNombreFuncion ,"").replace(/,/g," ");
			while (existe(regRes = $parametros.exec(texto))) 
			{
				args.push(regRes[0]);
				texto = texto.replace(regRes[0],"");
			}
			
			for(var i in args) 
			{
				if (esFuncion(args[i]))
				{
					args[i] = calcularFuncion(args[i]);
				}
			}
			var funName:String = nombreFuncion.exec(cadena)[1];
			if (existe($funciones[funName]))
			{
				return $funciones[funName].apply(this, args);
			}
			else 
			{
				throw 'La función "'+funName+'" no esta definida o esta mal escrita, recuerda usar solo minusculas';
			}
		}
		
		/**
		 * Registra una función
		 * 
		 * @param	nombre el nombre de la función, sera convertido en minusculas siempre
		 * @param	funcion la funcion que se ejecutara en as3, debe ser una función que reciva un parametro de tipo ...rest
		 */
		protected function registrarFuncion(nombre:String, funcion:Function):void 
		{
			$funciones[nombre.toLowerCase()] = funcion;
		}
		
		/**
		 * Borra una función de la lista de funciones
		 * 
		 * @param	nombre El nombre de la función a borrar
		 */
		protected function borrarFuncion(nombre:String):void 
		{
			$funciones[nombre.toLowerCase()] = null;
		}
		
		private function esFuncion(i:String):Boolean 
		{
			return regFuncion.test(i);
		}
		
		//}endregion
		
		//
		// {region control variables
		//
		
		private function calcularVariable(variable:String):* 
		{
			if (regFuncion.test(variable))
			{
				return calcularFuncion(variable);
			}
			if (esCadena.test(variable)) 
			{
				return variable.replace(esCadena,"$1");
			}
			if (!existe(variable) || variable == "null")
			{
				return null;
			}
			if (esNumero.test(variable)) 
			{
				return Number(variable);
			}
			if (variable == "true" || variable == "false") 
			{
				return variable == "true" ? true : false;
			}
			if (existe($variables[variable]))
			{
				return $variables[variable];
			}
			else 
			{
				throw 'Acceso a una propiedad "'+variable+'" no definida';
			}
		}
		
		protected function calcularVariables(varArray:Array):Array 
		{
			var dev:Array = [];
			for (var i in varArray) 
			{
				dev[i] = calcularVariable(varArray[i]);
			}
			return dev;
		}
		
		private function esVariable(j:String):Boolean 
		{
			return !esFuncion(j);
		}
		
		//}endregion	
	}
}