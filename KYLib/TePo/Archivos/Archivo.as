﻿/*
 * Copyright (C) 2019 Juan Pablo Calle Grafe
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Archivos 
{
	import KYLib.C.*;
	import KYLib.C.Obj.existe;

	import KYLib.C.Mat.*;
	
	import KYLib.Errores.*;
	import KYLib.Errores.ErroresClases.*;
	import KYLib.Eventos.*;
	import KYLib.Eventos.EventosCargas.*;
	import KYLib.Interfaces.Extendidas.*;
	import KYLib.Signals.ErrorSignal;
	import KYLib.Signals.EventSignal;
	import KYLib.Signals.ProgressSignal;
	import KYLib.TePo.Archivos.*;
	import KYLib.TePo.Archivos.ArchivosUtil.*;
	import KYLib.TePo.Archivos.Constantes.*;
	import KYLib.TePo.ByteLevel.*;
	import KYLib.TePo.ByteLevel.Codificacion.*;
	import KYLib.TePo.ByteLevel.Constantes.*;
	import KYLib.TePo.Carga.*;
	import KYLib.TePo.Exterior.*;
	import flash.events.*;
	import flash.utils.*;
	import KYLib.NameSpaces.nsAttribArchivo;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import org.osflash.signals.Signal;
	
	use namespace nsAttribArchivo;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase Archivo es muy parecida la clase File del paquete flash.filesystem ya que tambien almacena una referencia a la ubicación de un archivo, pero tambien tiene metodos para escribir y leer datos de un archivo, es decir, esta clases es como una compinacion entre la clase File y la clase FileStream.
	 *
	 * @langversion 3.0
	 *
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 */
	public class Archivo
	{
		/// El directorio temporal
		private static var $tempDir:Parche;
		
		/// Guarda la data de un archivo
		private const $data:ConjuntoBytes = new ConjuntoBytes();
		
		/// el file stream
		private const $fs:FileStream = new FileStream();
		
		/// el parche que se usa localmente
		private var $parche:Parche;
		
		/// la instancia que se usa para leer datos
		private var $lector:LectorDatos;
		
		/// crea el directorio temporal
		private static function initDir():void 
		{
			if (!existe($tempDir)) 
			{
				//se guarda la ubicacion del archivo temporal
				$tempDir = esencial.tempDir.resolverParche("KYLib/TePo/Archivos/Archivo.as/temp_files");
			}
		}
		
		public function Archivo(archivo:Parche) 
		{
			//se agrega como calse d solo AIR
			ErrorClaseAIR.agregar(this);
			super();
			//se inicia el directorio temporal si no existe
			Archivo.initDir();
			
			//se guarda la referencia al archivo
			$parche = archivo;
			
			//se crea el nuevo lector
			$lector = new LectorDatos($parche);
			
			//se le agregan listners al filestream
			$fs.addEventListener(Event.COMPLETE, on$fsComplete);
			$fs.addEventListener(IOErrorEvent.IO_ERROR, on$fsIoError);
			$fs.addEventListener(ProgressEvent.PROGRESS, on$fsProgress);
			$fs.addEventListener(OutputProgressEvent.OUTPUT_PROGRESS, on$fsOutputProgress);
			//se le agregan listeners al lector de datos
			$lector.onIoError.add(on$lectorIoError);
			$lector.onLecturaCompleta.add(on$fsComplete);
			$lector.onProgresoLectura.add(on$lectorProgreso);
			
			//se pone que la data guardada tenga los mismos configs que el stream
			$data.endian = $fs.endian;
			$data.objectEncoding = $fs.objectEncoding;
		}
		
		/// los datos han sido escritos en el archivo
		private function on$fsOutputProgress(e:OutputProgressEvent):void 
		{
			//se mira que no hayan mas datos por escribir
			if (!e.bytesPending) 
			{
				//se dispara señal de todos los datos escritos
				onDatosEscritos.dispatch();
			}
		}
		
		/// data del stream es cargada
		private function on$fsProgress(e:ProgressEvent):void 
		{
			onProgresoCarga.dispatch(num.fixed(e.bytesLoaded/e.bytesTotal,2));
		}
		
		/// Error de escritura o lectura de datos del archivo
		private function on$fsIoError(e:IOErrorEvent):void 
		{
			onIoError.dispatch(e.text);
		}
		
		/// cuando se completa el stream, osa cuando se termina de abrir un archivo de forma asincronica
		private function on$fsComplete(e:Event = null):void 
		{
			$abriendo = false;
			$abierto = true;
			onAbierto.dispatch();
		}
		
		/// Se ejecuta cuando ocurre un error en el lector de datos
		private function on$lectorIoError(e:ErrorSignal):void 
		{
			onIoError.dispatch(e.mensaje);
		}
		
		private function on$lectorProgreso(e:ProgressSignal):void 
		{
			onProgresoCarga.dispatch(e.bytesCargados);
		}
		
		/**
		 * abre el archivo de forma sincronica
		 */
		private function abrirSincronico(modo:String):void 
		{
			$fs.open($parche.$file, modo);
			$abriendo = false;
			$abierto = true;
			onAbierto.dispatch();
		}
		
		/// Indica si hay un archivo abierto actualmente
		private var $abierto:Boolean = false;
		
		///indica si el archivo esta abriendose
		private var $abriendo:Boolean = false;
		
		/**
		 * Señal lanzada cuando ocurre un error en la lectura/escritura de datos de un archivo..
		 */
		public const onIoError:ErrorSignal = new ErrorSignal();
		
		/**
		 * Señal lanzada cuando se abre el archivo.
		 */
		public const onAbierto:EventSignal = new EventSignal(this);
		
		/**
		 * Señal lanzada cuando se terminan de escribir los datos en un archivo
		 */
		public const onEscrituraCompleta:EventSignal = new EventSignal(this);
		
		/**
		 * Señal lanzada cuando todos los datos han sido escritos en el archivo
		 */
		public const onDatosEscritos:EventSignal = new EventSignal(this);
		
		/**
		 * Señal lanzada cuando ocurre un avance en el progreso de carga del archivo(solo para modos asincronicos).
		 */
		public const onProgresoCarga:ProgressSignal = new ProgressSignal(100);
		 
		/**
		 * Guarda el modo en que fue abierto.
		 * <p>0- No abierto
		 * 1- Normal Escritura
		 * 2- Normal Lectura
		 * 3- Normal Lectura,Escritura
		 * 4- Parcial
		 * 5- Normal Asincronico</p>
		 */
		private var $modoAbierto:uint = 0;
		
		/**
		 * Abre un archivo para leer o escribir datos en el.
		 * 
		 * @param	modo El modo en que va a ser abierto el archivo, definido por las constantes estaticas de la clase ModoArchivo.
		 * @param	limiteCarga Es el limite de datos en MegaBytes que van a ser cargados, este parametros solo es tomado por el modo ModoArchivo.LECTURA_PARCIAL y el modo ModoArchivo.NORMAL, en este segundo no indica el limite de datos a cargar si no el limite de peso para abrir un archivo en modo asincronico. Poner el valor 0 en el modo de lectura pasrical causara que no se cargue ningun dato, si quiere que en este modo se carguen todos los datos use la constante infinity
		 * @param	Direccion Este parametro solo es usado por el modo ModoArchivo.LECTURA_PARCIAL y indica la direccion apartir de la cual se empiezan a leer datos.
		 */
		public function abrir(modo:String, limiteCarga:uint = 10, Direccion:uint = 0):void
		{
			//si el archivo ya esta abierto o se esta abriendo entonces se sale
			if ($abierto || $abriendo) 
			{
				return;
			}
			
			//se pone que se esta abriendo
			$abriendo = true;
			
			//se mira el modo
			switch (modo) 
			{
				case ModoArchivo.ANEXO:
				case ModoArchivo.ESCRITURA:
				{
					$modoAbierto = 1;
					abrirSincronico(modo);
					break;
				}
				case ModoArchivo.LECTURA:
				{
					$modoAbierto = 2;
					abrirSincronico(modo);
					break;
				}
				case ModoArchivo.NORMAL:
				{
					//se el tamaño del archivo supera el limite de carga entonces se abre de manera sincronica
					if ($parche.$file.size > limiteCarga * 1e+6) 
					{
						$modoAbierto = 3;
						abrirSincronico(modo);
					}
					else 
					{
						$modoAbierto = 5;
						$fs.openAsync($parche.$file, modo);
					}
					break;
				}
				case ModoArchivo.LECTURA_PARCIAL:
				{
					$modoAbierto = 4;
					$lector.obtenerDatos($data, Direccion, limiteCarga*1e+6);
					break;
				}
			}
		}
		
		/**
		 * Cierra el archivo y elimina los datos cargados en la memoria fisica.
		 */
		public function cerrar():void
		{
			// se pone que no esta abierto
			$modoAbierto = 0;
			$abierto = false;
			//se cierra la conexion
			$fs.close();
			//se borra la data cargada si existe
			$data.clear();
		}
		
		/* DELEGATE flash.filesystem.FileStream */
		
		public function get bytesDisponibles():uint 
		{
			return $modoAbierto == 4 || $modoAbierto == 0 ? $data.bytesAvailable : $fs.bytesAvailable;
		}
		
		public function get endian():String 
		{
			return $fs.endian;
		}
		
		public function set endian(value:String):void 
		{
			$fs.endian = value;
			$data.endian = value;
		}
		
		public function get objectEncoding():uint 
		{
			return $fs.objectEncoding;
		}
		
		public function set objectEncoding(value:uint):void 
		{
			$fs.objectEncoding = value;
			$data.objectEncoding = value;
		}
		
		public function get posicion():Number 
		{
			return $modoAbierto == 4 || $modoAbierto == 0 ? $data.position : $fs.position;
		}
		
		public function set posicion(value:Number):void
		{
			$fs.position = value;
			$data.position = value;
		}
		
		public function readBytes(bytes:ConjuntoBytes, offset:uint = 0, length:uint = 0):void
		{
			switch ($modoAbierto) 
			{
				case 2:
				case 3:
				case 5:
				{
					$fs.readBytes(bytes, offset, length);
					break;
				}
				case 4:
				{
					$data.readBytes(bytes, offset, length);
					break;
				}
			}
		}
		
		public function readObject():* 
		{
			switch ($modoAbierto) 
			{
				case 2:
				case 3:
				case 5:
				{
					$fs.readObject();
					break;
				}
				case 4:
				{
					$data.readObject();
					break;
				}
			}
			return null;
		}
		
		public function readUTFBytes(length:uint):String 
		{
			switch ($modoAbierto) 
			{
				case 2:
				case 3:
				case 5:
				{
					$fs.readUTFBytes(length);
					break;
				}
				case 4:
				{
					$data.readUTFBytes(length);
					break;
				}
			}
			return null;
		}
		
		public function truncar():void
		{
			$fs.truncate();
		}
		
		
		
		public function writeBytes(bytes:ConjuntoBytes, offset:uint = 0, length:uint = 0):void 
		{
			switch ($modoAbierto) 
			{
				case 1:
				case 3:
				{
					$fs.writeBytes(bytes, offset, length);
					onDatosEscritos.dispatch();
					break;
				}
				case 5:
				{
					$fs.writeBytes(bytes, offset, length);
					break;
				}
			}
		}
		
		public function writeObject(object:*):void 
		{
			switch ($modoAbierto) 
			{
				case 1:
				case 3:
				{
					$fs.writeObject(object);
					onDatosEscritos.dispatch();
					break;
				}
				case 5:
				{
					$fs.writeObject(object);
					break;
				}
			}
		}
		
		/**
		 * Escribe una cadena de texto dentro del archivo.
		 * 
		 * @param	value La cadena de texto que quiere ser escrita.
		 */
		public function writeUTFBytes(value:String):void 
		{
			// se mira en que modo esta abierto
			switch ($modoAbierto) 
			{
				// en los modo 1 y 2 se escriben los datos de forma sincronica
				case 1:
				case 3:
				{
					$fs.writeUTFBytes(value);
					onDatosEscritos.dispatch();
					break;
				}
				// en el modo 5 se escriben los datos de forma asincronica
				case 5:
				{
					$fs.writeUTFBytes(value);
					break;
				}
			}
		}
	}
}