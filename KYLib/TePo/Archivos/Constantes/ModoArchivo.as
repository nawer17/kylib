﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Archivos.Constantes 
{
	import KYLib.Errores.ErroresClases.ErrorClaseAbstracta;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ModoArchivo define constantes para el metodo <code>abrir()</code> de la clase Archivo.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 * 
	 * @see KYLib.TePo.Archivos.Archivo#abrir() Metodo <code>abrir()</code> de la clase Archivo.
	 */
	public final class ModoArchivo
	{
		
		/**
		 * @private
		 */
		public function ModoArchivo() 
		{
			ErrorClaseAbstracta.agregar(this);
		}
		
		/**
		 * Indica que el archivo sera abierto en modo escritura, cuando se abre de este modo puede cambiar la posicion en la que quiere escribir datos.
		 * 
		 * <p>Este modo funciona de forma sincronica.</p>
		 */
		public static function get ESCRITURA():String 
		{
			return "write";
		}
		
		/**
		 * En este modo se abre el archivo para solo lectura de los datos.
		 * 
		 * <p>Este modo funciona de forma sincronica.</p>
		 */
		public static function get LECTURA():String 
		{
			return "read";
		}
		
		/**
		 * En esto modo se abre el archivo solo para obtener una porcion de datos especificada en una ubicacion especifica. Este modo no abre el archivo en si lo cual hace que este disponible para la apertura de otras aplicaciones.
		 * 
		 * <p>Este modo funciona de forma asincronica.</p>
		 */
		public static function get LECTURA_PARCIAL():String 
		{
			return "partialread";
		}
		
		/**
		 * Este modo de archivo se usa para escribir datos en un archivo pero no puede modificar la posición de escritura si no que solo puede escribir datos al final del archivo.
		 * 
		 * <p>Este modo funciona de forma sincronica.</p>
		 */
		public static function get ANEXO():String 
		{
			return "append";
		}
		
		/**
		 * Este modo se usa para abrir archivos en modo de lectura y escritura.
		 * 
		 * <p>Este modo funciona de forma asincronica siempre y cuando el tamaño del archivo no exceda el limite de memoria establecido, cuando este limite es excedido el archivo trabaja de forma sincronica para no ocupar memoria fisica.</p>
		 */
		public static function get NORMAL():String 
		{
			return "update";
		}
		
	}
}