﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */

package KYLib.TePo.Archivos
{
	import KYLib.C.Obj.existe;
	import KYLib.Errores.ErroresClases.ErrorClaseAIR;
	import KYLib.Errores.ErroresClases.*;
	import KYLib.Eventos.TePo.EventosExteriores.EventoCMD;
	import KYLib.TePo.Archivos.ArchivosUtil.AtributosArchivo;
	import KYLib.TePo.Exterior.CMD;
	import flash.desktop.Icon;
	import flash.filesystem.File;
	import flash.filesystem.*;
	import KYLib.NameSpaces.nsAttribArchivo;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	
	//use namespace nsAttribArchivo;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase Parche se usa para
	 *
	 * @langversion 3.0
	 *
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class Parche
	{
		/// El archivo que se usa para aputar a esa direccion
		internal var $file:File;
		
		///guarda una cadena de los atributos
		private var $atributos:AtributosArchivo;
		
		/**
		 * Crea una nueva instancia de la clase Parche que apunta a un directorio o archivo especifico.
		 *
		 * @param ruta Es la direccion del parche, la separacion de directorios debe realizarse con el caracter "/" o "\\"(solo windows), puede usar notación de URL pero solo se aceptan: "app://" para apuntar al directorio de la aplicación, "app-storage://" para apuntar al directorio de almacenamiento de datos asignado a la aplicación y "file:///" para apuntar a la raiz del sistema de archivos del dispositivo.
		 */
		public function Parche(ruta:String = null, ObtenerAtributos:Boolean = false,GenerarPadre:Boolean = true)
		{
			ErrorClaseAIR.agregar(this);
			super();
			
			$file = new File(ruta);
			
			if (GenerarPadre) 
			{
				generarPadre();
			}
			
			if (ObtenerAtributos) 
			{
				$atributos = new AtributosArchivo(this);
				//$atributos.nsAttribArchivo::actualizarAtributos();
				$atributos.nsAttribArchivo::agregarAtributos(AtributosArchivo.SinConexion,AtributosArchivo.NoIndexado,AtributosArchivo.Anclado,AtributosArchivo.Desanclado);
			}
		}
		
		/**
		 * Indica si se hace referencia a un directorio. El valor es <code>true</code> si el objeto Parche apunta a un directorio; en caso contrario, el valor es <code>false</code>.
		 */
		public function get directorio():Boolean
		{
			return $file.isDirectory;
		}
		
		/**
		 * Indica si el archivo o directorio al que se hace referencia está "oculto". El valor es <code>true</code> si el archivo o directorio al que se hace referencia está oculto; en caso contrario, el valor es <code>false</code>.
		 */
		public function get oculto():Boolean
		{
			return $file.isHidden;
		}
		
		/**
		 * La ruta completa de la representación del sistema operativo. 
		 *
		 * <p>Antes de escribir código para definir la propiedad parcheNativo directamente, analice si hacerlo requerirá escribir código específico de alguna plataforma. Por ejemplo, una ruta nativa como "C:\\Documents and Settings\\pedro\\Escritorio" sólo es válida en Windows. Es mucho mejor utilizar las siguientes propiedades estáticas que representan lo directorios más utilizados y que son válidas en todas las plataformas:</p>
		 * <ul>
		 * <li>Parche.directorioAplicacion</li>
		 * <li>Parche.directorioAlmacenamientoAplicacion</li>
		 * <li>Parche.directorioCache</li>
		 * <li>Parche.directorioDocumentos</li>
		 * <li>Parche.directorioUsuario</li>
		 * </ul>
		 * 
		 * <span>Puede utilizar el método resolverParche() para obtener una ruta relativa a estos directorios.</span>
		 */
		public function get parcheNativo():String
		{
			return $file.nativePath;
		}
		
		/// Es el directorio en el que se encuentra el parche actual
		private var $padre:Parche = null;
		
		/**
		 * El directorio que contiene el archivo o el directorio al que hace referencia este objeto Parche.
		 * 
		 * <span>Si el archivo o directorio no existe, la propiedad padre sigue devolviendo el objeto Parche que apunta al directorio que lo contiene (incluso si dicho directorio no existe).<span>
		 * 
		 * <p>Esta propiedad es idéntica al valor devuelto por resolverParche("..") con la salvedad de que el directorio principal de un directorio raíz es <code>null</code>.</p>
		 */
		public function get padre():Parche
		{
			return $padre;
		}
		
		/// Genera el parche que apunta al directorio padre
		private function generarPadre():void
		{
			$padre = existe($file.parent) ? new Parche($file.parent.nativePath) : null;
		}
		
		/**
		 * Crea un nuevo objeto Parche con una ruta relativa a la ruta del objeto Parche actual. Se basa en el parámetro ruta (una cadena).
		 * 
		 * <p>Puede utilizar una ruta relativa o una ruta absoluta como parámetro ruta.</p>
		 * 
		 * <p>Si especifica una ruta relativa, el parámetro path dado se "añade" a la ruta del objeto File. No obstante, el uso de ".." en el parámetro path puede devolver una ruta resultante que no dependa del objeto File. La referencia resultante no necesita hacer referencia a una ubicación real de sistema de archivos.</p>
		 * 
		 * <p>Si especifica una referencia absoluta de archivo, el método devuelve el objeto File que apunta a dicha ruta. La referencia absoluta de archivo debe utilizar la sintaxis nativa válida de rutas del sistema operativo del usuario (por ejemplo, "C:\\test" en Windows). No utilice una URL (como "file:///c:/test") como parámetro path.</p>
		 * 
		 * <p>Todas las rutas resultantes se normalizan del siguiente modo:</p>
		 * <ul>
		 * <li>Todo "." se omite.</li>
		 * <li>Todo ".." consume su entrada principal.</li>
		 * <li>Todo ".." que llega a la raíz del sistema de archivos o la raíz de almacenamiento persistente de la aplicación pasa dicho nodo; se omite.</li>
		 * </ul>
		 * <p>Siempre se debe utilizar el carácter de barra inclinada ( / ) como separador de ruta. En Windows, también puede utilizar el carácter de barra diagonal invertida (\), pero no se recomienda. Usar el carácter de barra diagonal invertida puede hacer que las aplicaciones no funcionen en otras plataformas.</p>
		 * 
		 * <p>En Linux, los nombres de archivos y de directorios distinguen entre mayúsculas y minúsculas.</p>
		 * 
		 * @param	parche
		 * @return
		 */
		public function resolverParche(ruta:String):Parche
		{
			return new Parche($file.resolvePath(ruta).nativePath);
		}
		
		/**
		 * URL de esta ruta de archivo.
		 * 
		 * <p>Si es una referencia a una ruta del directorio de almacenamiento de la aplicación, el esquema URL es "app-storage"; si se trata de una referencia a una ruta en el directorio de aplicaciones, el esquema URL es "app"; en cualquier otro caso, el esquema es "file".</p>
		 */
		public function get url():String
		{
			return $file.url;
		}
		
		/**
		 * La fecha de creación del archivo en el disco local.
		 * <span>Si aún no se ha llenado el objeto , una llamada para obtener el valor de esta propiedad devolverá <code>null</code>.</span>
		 * 
		 * <p>Nota: Si un sistema operativo no tiene <code>creationDate</code> como su propiedad, en estos casos, <code>fechaCreacion</code> es igual a <code>fechaUltimaModificacion</code>.</p>
		 */
		public function get fechaCreacion():Date
		{
			return $file.creationDate;
		}
		
		/**
		 * Extensión del nombre de archivo.
		 * <span>La extensión de un archivo es la parte del nombre situada a la derecha del punto final ("."), sin incluirlo. Si el nombre de archivo no tiene ningún punto, la extensión es <code>null</code>.</span>
		 */
		public function get extension():String
		{
			return $file.extension;
		}
		
		/**
		 * El nombre del archivo en el disco local.
		 */
		public function get name():String
		{
			return $file.name;
		}
		
		/**
		 * El tamaño del archivo en el disco local, expresado en bytes.
		 */
		public function get peso():Number
		{
			return $file.size;
		}
		
		/**
		 * Indica si existe el archivo o directorio al que se hace referencia. 
		 * <span>El valor es <code>true</code> si el objeto Parche apunta a un archivo o directorio existente; en caso contrario, el valor es <code>false</code>.</span>
		 */
		public function get existente():Boolean
		{
			return $file.exists;
		}
		
		/* DELEGATE flash.filesystem.File */
		
		public function browseForDirectory(title:String):void 
		{
			$file.browseForDirectory(title);
		}
		
		public function browseForOpen(title:String, typeFilter:Array = null):void 
		{
			$file.browseForOpen(title, typeFilter);
		}
		
		public function browseForOpenMultiple(title:String, typeFilter:Array = null):void 
		{
			$file.browseForOpenMultiple(title, typeFilter);
		}
		
		public function browseForSave(title:String):void 
		{
			$file.browseForSave(title);
		}
		
		public function cancel():void 
		{
			$file.cancel();
		}
		
		public function canonicalize():void 
		{
			$file.canonicalize();
		}
		
		public function clone():flash.filesystem.File 
		{
			return $file.clone();
		}
		
		public function copyTo(newLocation:FileReference, overwrite:Boolean = false):void 
		{
			$file.copyTo(newLocation, overwrite);
		}
		
		public function copyToAsync(newLocation:FileReference, overwrite:Boolean = false):void 
		{
			$file.copyToAsync(newLocation, overwrite);
		}
		
		public function createDirectory():void 
		{
			$file.createDirectory();
		}
		
		public function deleteDirectory(deleteDirectoryContents:Boolean = false):void 
		{
			$file.deleteDirectory(deleteDirectoryContents);
		}
		
		public function deleteDirectoryAsync(deleteDirectoryContents:Boolean = false):void 
		{
			$file.deleteDirectoryAsync(deleteDirectoryContents);
		}
		
		public function deleteFile():void 
		{
			$file.deleteFile();
		}
		
		public function deleteFileAsync():void 
		{
			$file.deleteFileAsync();
		}
		
		public function getDirectoryListing():Array 
		{
			return $file.getDirectoryListing();
		}
		
		public function getDirectoryListingAsync():void 
		{
			$file.getDirectoryListingAsync();
		}
		
		public function getRelativePath(ref:FileReference, useDotDot:Boolean = false):String 
		{
			return $file.getRelativePath(ref, useDotDot);
		}
		
		public function get icon():flash.desktop.Icon 
		{
			return $file.icon;
		}
		
		public function get isPackage():Boolean 
		{
			return $file.isPackage;
		}
		
		public function get isSymbolicLink():Boolean 
		{
			return $file.isSymbolicLink;
		}
		
		public function moveTo(newLocation:FileReference, overwrite:Boolean = false):void 
		{
			$file.moveTo(newLocation, overwrite);
		}
		
		public function moveToAsync(newLocation:FileReference, overwrite:Boolean = false):void 
		{
			$file.moveToAsync(newLocation, overwrite);
		}
		
		public function moveToTrash():void 
		{
			$file.moveToTrash();
		}
		
		public function moveToTrashAsync():void 
		{
			$file.moveToTrashAsync();
		}
		
		public function openWithDefaultApplication():void 
		{
			$file.openWithDefaultApplication();
		}
		
		public function get preventBackup():Boolean 
		{
			return $file.preventBackup;
		}
		
		public function set preventBackup(value:Boolean):void 
		{
			$file.preventBackup = value;
		}
		
		public function requestPermission():void 
		{
			$file.requestPermission();
		}
		
		public function get spaceAvailable():Number 
		{
			return $file.spaceAvailable;
		}
		
		public function toString():String 
		{
			return $file.toString();
		}
		
		public function browse(typeFilter:Array = null):Boolean 
		{
			return $file.browse(typeFilter);
		}
		
		public function get creationDate():Date 
		{
			return $file.creationDate;
		}
		
		public function get creator():String 
		{
			return $file.creator;
		}
		
		public function download(request:URLRequest, defaultFileName:String = null):void 
		{
			$file.download(request, defaultFileName);
		}
		
		public function load():void 
		{
			$file.load();
		}
		
		public function get modificationDate():Date 
		{
			return $file.modificationDate;
		}
		
		public function save(data:*, defaultFileName:String = null):void 
		{
			$file.save(data, defaultFileName);
		}
		
		public function get size():Number 
		{
			return $file.size;
		}
		
		public function get type():String 
		{
			return $file.type;
		}
		
		public function upload(request:URLRequest, uploadDataFieldName:String = "Filedata", testUpload:Boolean = false):void 
		{
			$file.upload(request, uploadDataFieldName, testUpload);
		}
		
		public function uploadUnencoded(request:URLRequest):void 
		{
			$file.uploadUnencoded(request);
		}
	}
}