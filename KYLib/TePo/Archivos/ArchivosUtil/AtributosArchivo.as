﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Archivos.ArchivosUtil 
{
	import KYLib.C.Obj.existe;
	import KYLib.Eventos.TePo.EventosExteriores.EventoCMD;
	import KYLib.TePo.Archivos.Parche;
	import KYLib.TePo.ByteLevel.hex;
	import KYLib.TePo.Exterior.CMD;
	import flash.events.IEventDispatcher;
	import KYLib.Eventos.DisparadorEventos;
	import KYLib.NameSpaces.nsAttribArchivo;
	
	
	use namespace nsAttribArchivo;
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase AtributosArchivo se usa para almacenar los atributos de un archivo.
	 * <span>Los atributos soportados estan definidos con las constantes estaticas de esta clase.</span>
	 * 
	 * <span>Los metodos <code>agregarAtributo()</code> y <code>elminarAtributo()</code> funcionan de forma asincronica por lo que llamarlos no causara que los atributos de un archivo cambien inmediatamente.</span>
	 * 
	 * <p>Por defecto todos los atributos estan establecidos en <code>false</code> hasta que se actualizen, esto no se puede hacer de manera normal si no que debe de hacerlo creando una instancia de la clase Parche y accediendo a sus propiedad <code>atributos</code> para poder obtener los valores verdaderos.</p>
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class AtributosArchivo extends DisparadorEventos 
	{
		/// guarda los valores originales de los atributos
		private const $atributos:Object = {
			"soloLectura":false,
			"almacenamiento":false,
			"sistema":false,
			"oculto":false,
			"sinConexion":false,
			"sinIndexadoContenido":false,
			"sinLimpieza":false,
			"integridad":false,
			"anclado":false,
			"desanclado":false,
			"blobSMR":false
		};
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta de solo lectura.
		 */
		public static const SoloLectura:String = "R";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta de almacenamiento y que esta listo para archivarse, de manera normal todos los archivos tienen este atributo.
		 */
		public static const Almacenamiento:String = "A";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta del sistema.
		 * <span>A los archivos/carpeta con este atributo no se les pueden cambiar los demas atributos, si desea cambiarlos establezca este atributo en <code>false</code>.</span>
		 */
		public static const Sistema:String = "S";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta que se encuentra en modo oculto.
		 * <span>A los archivos/carpeta con este atributo no se les pueden cambiar los demas atributos, si desea cambiarlos establezca este atributo en <code>false</code> sin emabrgo es posible poner el atributo "SoloLectura".</span>
		 */
		public static const Oculto:String = "H";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta en modo sin conexión.
		 */
		public static const SinConexion:String = "O";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta cuyo contenido no esta indexado, generalmente esta propiedad siempre es <code>false</code> debido a que el contenido de los archivos siempre esta indexado por defecto.
		 */
		public static const NoIndexado:String = "I";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta sin limpieza.
		 */
		public static const NoLimpieza:String = "X";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta de integridad.
		 * 
		 * <p>Este atributo no puede ser alterado.</p>
		 */
		private static const Integridad:String = "V";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta que esta en mod anclado.
		 * <span>Este atributo es la contra parte del atributo "desanclado", no pueden estar los dos al mismo tiempo en un archivo pero no significa que un archivo tenga que tener alguno de los dos necesariamente.</span>
		 */
		public static const Anclado:String = "P";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta que esta en mod desanclado.
		 * <span>Este atributo es la contra parte del atributo "anclado", no pueden estar los dos al mismo tiempo en un archivo pero no significa que un archivo tenga que tener alguno de los dos necesariamente.</span>
		 */
		public static const Desanclado:String = "U";
		
		/**
		 * Atributo de archivo que cuando es <code>true</code> indica que es un archivo/carpeta de blob SMR.
		 * 
		 * <p>Este atributo no puede ser alterado.</p>
		 */
		private static const BlobSMR:String = "B";
		
		/// Este CMD se usa para poder obtener los atributos de un archivo
		private static var $cmd:CMD;
		
		/// guarda los atributos que si pueden ser agregados
		private static const $regExp:RegExp = /[ARUPXIOHS]/;
		
		/// crea el cmd
		private static function init():void 
		{
			if (existe($cmd)) 
			{
				return;
			}
			$cmd = new CMD();
			$cmd.addEventListener(EventoCMD.CONSTRUIDO, on$cmdConstruido);
			$cmd.addEventListener(EventoCMD.SALIDA, on$cmdSalida);
		}
		
		///lista de objetos a obtener atributos
		private static const $cola:Vector.<AtributosArchivo> = new Vector.<AtributosArchivo>();
		
		/// Añade a la cola un archivo para obtner sus atributos
		static private function ejecutarComando(Instancia:AtributosArchivo):void 
		{
			if (!Instancia.$parche.existente)
			{
				return;
			}
			$cola.push(Instancia);
			
			if ($cola.length == 1 && !$cmd.ejecutando) 
			{
				procesarSiguiente();
			}
		}
		
		/**
		 * Procesa el siguiente parche
		 */
		static private function procesarSiguiente():void
		{
			if (!$cola.length) 
			{
				return;
			}
			$cmd.setLinea(0x0, $cola[0].$comando);
			$cmd.construir();
		}
		
		///cuando se termina de construir el cmd
		private static function on$cmdConstruido(e:EventoCMD):void 
		{
			$cmd.ejecutar();
		}
		
		/// cuando se cierra el cmd
		static private function on$cmdSalida(e:EventoCMD):void 
		{
			$cola.shift().parseAtrributos(e.respuesta);
			procesarSiguiente();
		}
		
		/// el parche con el que trabaja
		private var $parche:Parche;
		
		/// el comando interno que se usara actualmente
		private var $comando:String;
		
		///guarda una cadena de los atributos
		private var $attribs:String;
		
		/**
		 * Crea una nueva instancia que guarda los atributos de un archivo en especifico
		 */
		public function AtributosArchivo(archivo:Parche) 
		{
			$parche = archivo;
			AtributosArchivo.init();
		}
		
		/// se usa para leer los atributos obtenidos
		private function parseAtrributos(respuesta:String):void 
		{
			$attribs = respuesta.replace($parche.parcheNativo, "");
			trace($attribs);
			
			$atributos.soloLectura = $attribs.search("R") != -1 ? true : false ;
			$atributos.almacenamiento = $attribs.search("A") != -1 ? true : false ;
			$atributos.sistema = $attribs.search("S") != -1 ? true : false ;
			$atributos.oculto = $attribs.search("H") != -1 ? true : false ;
			$atributos.sinConexion = $attribs.search("O") != -1 ? true : false ;
			$atributos.sinIndexadoContenido = $attribs.search("I") != -1 ? true : false ;
			$atributos.sinLimpieza = $attribs.search("X") != -1 ? true : false ;
			$atributos.integridad = $attribs.search("V") != -1 ? true : false ;
			$atributos.anclado = $attribs.search("P") != -1 ? true : false ;
			$atributos.desanclado = $attribs.search("U") != -1 ? true : false ;
			$atributos.blobSMR = $attribs.search("B") != -1 ? true : false ;
			
			/*for (var i:String in $atributos) 
			{
				trace(i +":\t\t\t" + $atributos[i]);
			}*/
		}
		
		/**
		 * Actualiza los valores de los atributos, esto dispara un evento cuando se terminan de actualizar.
		 * @private
		 */
		nsAttribArchivo function actualizarAtributos():Boolean 
		{
			// se mira si el elemento ya esta en la cola de ejecucuion de comandos
			if (AtributosArchivo.$cola.indexOf(this) != -1) 
			{
				//se devuelve false porque no se puede ejecutar el comando
				return false;
			}
			//se cambia el comando
			$comando = 'ATTRIB "' + $parche.parcheNativo + '"';
			//se manda a ejecutar
			AtributosArchivo.ejecutarComando(this);
			//se devuelve true
			return true;
		}
		
		nsAttribArchivo function agregarAtributos(...atributos):Boolean 
		{
			// se mira si el elemento ya esta en la cola de ejecucuion de comandos
			if (AtributosArchivo.$cola.indexOf(this) != -1) 
			{
				//se devuelve false porque no se puede ejecutar el comando
				return false;
			}
			$comando = 'ATTRIB';
			
			for each (var i:String in atributos)
			{
				if (AtributosArchivo.$regExp.exec(i))
				{
					switch (AtributosArchivo.$regExp.exec(i)[0]) 
					{
						case AtributosArchivo.Anclado:
						{
							if (hex.deCadena($comando).search(hex.deCadena("+" + AtributosArchivo.Anclado).replace("0x","")) != -1) 
							{
								continue;
							}
							if (hex.deCadena($comando).search(hex.deCadena("+" + i).replace("0x","")) != -1) 
							{
								continue;
							}
							$comando += " +" + i;
							break;
						}
						case AtributosArchivo.Desanclado:
						{
							if (hex.deCadena($comando).search(hex.deCadena("+" + AtributosArchivo.Anclado).replace("0x","")) != -1) 
							{
								continue;
							}
							if (hex.deCadena($comando).search(hex.deCadena("+" + i).replace("0x","")) != -1) 
							{
								continue;
							}
							$comando += " +" + i;
							break;
						}
						default:
						{
							if (hex.deCadena($comando).search(hex.deCadena("+" + i).replace("0x","")) != -1) 
							{
								continue;
							}
							$comando += " +" + i;
						}
					}
				}
			}
			$comando += ' "' + $parche.parcheNativo + '"';
			
			AtributosArchivo.ejecutarComando(this);
			return true;
		}
		
		nsAttribArchivo function eliminarAtributos(...atributos):Boolean 
		{
			// se mira si el elemento ya esta en la cola de ejecucuion de comandos
			if (AtributosArchivo.$cola.indexOf(this) != -1) 
			{
				//se devuelve false porque no se puede ejecutar el comando
				return false;
			}
			$comando = 'ATTRIB "' + $parche.parcheNativo + '"';
			AtributosArchivo.ejecutarComando(this);
			return true;
		}
	}
}