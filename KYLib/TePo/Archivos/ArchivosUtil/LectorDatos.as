﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.TePo.Archivos.ArchivosUtil 
{
	
		import KYLib.C.Mat.*;
		import KYLib.C.Utils.clases;
		import KYLib.Errores.*;
		import KYLib.Eventos.*;
		import KYLib.Signals.*;
		import KYLib.TePo.Archivos.*;
		import KYLib.TePo.ByteLevel.*;
		import flash.events.*;
		import flash.net.*;
		import org.osflash.signals.Signal;
		
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase LectorDatos se usa para leer una fraccion de datos de un archivo local o de internet sin necesidad de cargarlo todo lo cual ahorra el uso de memoria fisica,debe tener en cuenta que aun asi podria tardar la busqueda de datos en archivos muy grandes es por eso que se recomienda usar controladores de eventos para saber cuando se cargaron ya los datos.
	 * 
	 * <p>Para archivos de internet solo se soportan los mismos protocolos que soporta AIR y FP (HTTP, HTTPS).</p>
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public final class LectorDatos
	{
		/// guarda el tamaño de fragmentado, en bytes
		private var $tamaño:uint = 25;
		
		/// indica desde donde se leeran los archivos
		private var $inicio:uint = 0;
		
		/// el objeto que se usa para cargar los datos
		private const $us:URLStream = new URLStream();
		
		/// la url que se carga
		private var $url:URLRequest;
		
		/// el conjunto de bytes local
		private var $cb:ConjuntoBytes;
		
		/// indica cuantos bytes se han cargado hasta el momento
		private var $cargado:uint = 0;
		
		/// guarda el porcentaje del progreso para hayar los datos
		private var $progreso:Number = 0;
		
		/// indica si se esta cargando en este momento datos
		private var $cargando:Boolean = false;
		
		/**
		 * Crea un nuevo lector de datos.
		 * 
		 * @param archivo La direccion del archivo del cual se leeran los datos.
		 */
		public function LectorDatos(Direccion:Object)
		{
			super();
			//Se mira si se paso una cadena como direccion
			if (Direccion is String) 
			{
				//se crea la url con esa cadena
				$url = new URLRequest(Direccion as String);
			}
			//se mira si se paso directamente una URLRequest y esa sera la usada
			else if (Direccion is URLRequest) 
			{
				//se usa la request pasada
				$url = Direccion as URLRequest;
			}
			//se mira se paso un parche a un archivo local
			else if (Direccion is Parche) 
			{
				// se guarda la url
				$url = new URLRequest(Direccion.url);
			}
			else 
			{
				//Se lanza un error
				KYlibError.lanzar(new ArgumentError('Los objetos de tipo "' + clases.nombreClaseObjeto(Direccion) + '" no son un tipo de dato admitido como dirección a un archivo.', 1214));
				return;
			}
			
			//se agregan listeners
			$us.addEventListener(ProgressEvent.PROGRESS, on$usProgress);
			$us.addEventListener(Event.COMPLETE, on$usComplete);
			$us.addEventListener(IOErrorEvent.IO_ERROR, on$usIoError);
		}
		
		/**
		 * Señal distribuida cuando ocurre un error en la lectura de datos.
		 * <span>Esto suele ocurrir cuando cuando un archivo local no existe o no se puede acceder a la url dada.</span>
		 */
		public const onIoError:ErrorSignal = new ErrorSignal();
		
		/// se dispara cuando ocurre un error de lectura, generalmente ocurre cuando el archivo no existe
		private function on$usIoError(e:IOErrorEvent):void 
		{
			//se pone que ya no se estan cargando datos
			$cargando = false;
			// se dispara la señal de error de lectura de datos
			onIoError.dispatch('No ha sido posible leer datos de la url "' + $url.url + '"');
		}
		
		/**
		 * Señal distribuida cuando ya se han cargado todos los datos del archivo o url.
		 */
		public const onLecturaCompleta:Signal = new Signal();
		
		/// se ejecuta cuando se han cargado todos los datos ya, o cuando se llama explicitamente por los metodos cargadores para que se cierre el stream y no descargar mas datos
		private function on$usComplete(e:Event):void 
		{
			if ($cb.length == 0) 
			{
				validar();
			}
			//se cierra el stream para ya no cargar mas datos
			$us.close();
			//se dispara la señal de progreso
			onProgresoLectura.dispatch(100);
			//se dispara la señal de completado
			onLecturaCompleta.dispatch();
			//se pone que ya no se estan cargando datos
			$cargando = false;
		}
		
		/// valida si los hay datos por escribirse y los escribe
		private function validar():void 
		{
			if ($us.bytesAvailable) 
			{
				if ($inicio) 
				{
					if ($inicio > $us.bytesAvailable+$cargado) 
					{
						return;
					}
					//se leen todos los datos disponibles en el stream
					$us.readBytes($cb, 0, $inicio-$cargado);
					//se borran los datos del inicio
					$cb.clear();
					//se leen todos los datos disponibles en el stream
					$us.readBytes($cb);
				}
				else 
				{
					//se leen todos los datos disponibles
					$us.readBytes($cb);
				}
			}
		}
		
		/**
		 * Señal distribuida cuando ocurre un progreso en la lectura de los datos de una url.
		 * <span>Esta señal no pasa el numero exacto de bytes que se han descargado si no que pasa el porcentaje de bytes descargados.</span>
		 */
		public const onProgresoLectura:ProgressSignal = new ProgressSignal(100);
		
		/// se ejecuta cuando se cargan nuevos bytes del archivo
		private function on$usProgress(e:ProgressEvent):void 
		{
			//cuando ya se cargaron los datos sale
			if (!$cargando) 
			{
				return;
			}
			//se pone el tamaño
			$tamaño = $tamaño == 0 ? uint.MAX_VALUE : $tamaño;
			
			
			//se actualiza la variable de progreso
			$progreso = 
			//1/se mira si el tamaño mas el inicio es menor al tamaño total de bytes del archivo
			($inicio + $tamaño) < e.bytesTotal 
			//1//en caso se ser menor
			? (
			//se toma como 100% a la suma del tamaño mas el inicio
			num.fixed((e.bytesLoaded / ($inicio + $tamaño)) * 100, 2) < 100 ? num.fixed((e.bytesLoaded / ($inicio + $tamaño)) * 100, 2) : 99.99
			)
			//1//en caso de ser mayor
			: (
			//se toma como 100% al tamaño del archivo
			num.fixed((e.bytesLoaded / e.bytesTotal) * 100, 2) < 100 ? num.fixed((e.bytesLoaded / e.bytesTotal) * 100, 2) : 99.99
			);
			//se dispara el evento de progreso
			onProgresoLectura.dispatch($progreso);
			
			
			// se mira si el valor de inicio es igual o distinto a cero
			if (!$inicio) 
			{
				// si es 0 se cargan los datos sin tener en cuenta el inicio
				cargarDeCero(e);
			}
			else 
			{
				//si es distinto a cero si se tiene en cuenta el inicio
				cargarDeInicio(e);
			}
		}
		
		/// se usa para cargar daros de un archivo desde la direccion dada
		private function cargarDeInicio(e:ProgressEvent):void 
		{
			//aqui se mira si los bytes cargados actuamente son menores a la direccion de inicio, si esto es true se eliminan esos datos ya que son innecesarios
			if (e.bytesLoaded < $inicio) 
			{
				//se leen los datos disponibles en el stream
				$us.readBytes($cb);
				//se suman los datos eliminados a la variable que guarda los bytes cargados
				$cargado += $cb.length;
				//se borran los datos
				$cb.clear();
			}
			// aqui se mira si ya se cargaron los datos necesarios
			else if (e.bytesLoaded >= $inicio + $tamaño)
			{
				//se mira si sobran bytes al principio del los bytes cargados
				if (($us.bytesAvailable > $tamaño) && ($inicio - $cargado)) 
				{
					//se leen los datos que sobran
					$us.readBytes($cb, 0, $inicio - $cargado);
					//se borran los datos
					$cb.clear();
				}
				//se lee la cantidad de datos que se requiere
				$us.readBytes($cb, 0, $tamaño);
				//se ejecuta la funcion de terminación
				on$usComplete(null);
				return;
			}
		}
		/// Carga los datos de un stream cuando el inicio es igual a 0
		private function cargarDeCero(e:ProgressEvent):void 
		{
			// se mira si la cantidad de bytes cargados es mayor al tamaño requerido
			if (e.bytesLoaded >= $tamaño)
			{
				//se leen $tamaño datos del stream
				$us.readBytes($cb, 0, $tamaño);
				//se manda a cerrar el stream
				on$usComplete(null);
			}
		}
		
		/**
		 * Empieza a cargar la cantidad especifica de datos en un conjunto de bytes.
		 * 
		 * @param	bytes El objeto ConjuntoBytes donde se escribiran los datos.
		 * @param	inicio El tamaño en bytes de datos que seran leidos.
		 * @param	tamaño La posición inicial en bytes a partir de la cual se leeran los datos.
		 */
		public function obtenerDatos(bytes:ConjuntoBytes, inicio:uint = 0 , tamaño:uint = 0):void
		{
			//si se estan cargando datos se sale
			if ($cargando) 
			{
				return;
			}
			//se establece los datos
			$cb = bytes;
			//se limpian los datos actuales del $cb
			$cb.clear();
			//se establece el tamaño a descargar
			$tamaño = tamaño;
			//se reinicia el progreso
			$progreso = 0;
			//se establece el inicio de busqueda
			$inicio = inicio;
			//se reinicia los datos cargados
			$cargado = 0;
			//se pone que se estan cargando datos
			$cargando = true;
			//se empiezan a cargar los datos
			$us.load($url);
		}
		
	}
}