﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Signals 
{
	import KYLib.C.Obj.existe;
	
	import KYLib.Errores.KYlibError;
	import org.osflash.signals.Signal;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ErrorSignal se usa para representar un equivalente a la clase ErrorEvent pero con las señales.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public class ErrorSignal extends Signal 
	{
		
		/**
		 * Crea una nueva ErrorSignal con un mensaje de error.
		 * 
		 * @param MensajeError El mensaje descriptivo del error, este no es parametro obligatorio y puede ser definido al momento de llamar la funcion <code>dispatch()</code> o con la propiedad <code>mensaje</code>.
		 */
		public function ErrorSignal(MensajeError:String = "")
		{
			super(ErrorSignal);
			mensaje = MensajeError;
			$error  = new KYlibError("Señal de error sin funciones agregadas:\n" + mensaje, "ErrorSignal", 125);
		}
		
		/**
		 * Mensaje que describe brevemente al error ocacionado.
		 */
		public var mensaje:String = "";
		
		/**
		 * Dispara la señal para que se ejecuten las funciones.
		 * 
		 * @param	...valueObjects En este arreglo solo es tomado el primer parametro y este debe ser una cadena de texto con el mensaje del error.
		 */
		override public function dispatch(...valueObjects):void
		{
			mensaje = existe(valueObjects[0]) ? valueObjects[0] : mensaje;
			
			if (numListeners) 
			{
				super.dispatch(this);
			}
			else 
			{
				KYlibError.lanzar(getErrorNoFunciones());
			}
		}
		
		/// el error que se usa cuando una señal de error no tierne funciones
		private static var $error:KYlibError;
		
		/// Devuelve el error por que se usa para indica
		private function getErrorNoFunciones():KYlibError
		{
			if ($error.message != mensaje) 
			{
				$error.message  = ("Señal de error sin funciones agregadas:\n" + mensaje);
			}
			return $error;
		}
	}
}