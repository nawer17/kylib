﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Signals 
{
		import KYLib.C.Obj.existe;
		
		import org.osflash.signals.Signal;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase ProgressSignal se usa para representar un equivalente a la clase ProgressEvent pero con las señales.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public class ProgressSignal extends Signal 
	{
		
		/**
		 * Crea una nueva señal de progreso.
		 * 
		 * @param	BytesTotales Representa el valor de datos que se descargaran, puede ser cambiado despues pero debe poner algun valor inicial, puede ser 0.
		 */
		public function ProgressSignal(BytesTotales:Number) 
		{
			super(ProgressSignal);
			
			//se guarda el valor maximo inicia
			BytesTotales = BytesTotales;
		}
		
		/// variable interna
		private var $bytesCargados:Number = 0;
		
		/**
		 * Representa el numero de bytes que han sido cargados hasta el momento.
		 */
		public function get bytesCargados():Number 
		{
			return $bytesCargados;
		}
		
		/**
		 * @private Setter
		 */
		public function set bytesCargados(value:Number):void 
		{
			$bytesCargados = value;
		}
		
		/// variable interna
		private var $bytesTotales:Number = 0;
		
		/**
		 * Representa el numero de bytes total que seran cargados.
		 */
		public function get bytesTotales():Number 
		{
			return $bytesTotales;
		}
		
		/**
		 * @private Setter
		 */
		public function set bytesTotales(value:Number):void 
		{
			$bytesTotales = value;
		}
		
		/**
		 * Dispara la señal.
		 * 
		 * @param ...valueObjects Este parametro admite dos valores, el primero se usa para indicar el numero de bytes cargados actualmente y el segundo se usa para sobreescribir el numero de bytes totales a cargar. Estos parametros no es obligatorio ponerlos y se pueden actualizar los valores actualizando las propiedades respectivas.
		 */
		override public function dispatch(...valueObjects):void 
		{
			//se mira si se puso un valor
			if (existe(valueObjects[0])) 
			{
				//se establece el numero de bytes cargados
				$bytesCargados = valueObjects[0];
				
				//se mira se si puso un segundo valor
				if (existe(valueObjects[1])) 
				{
					//se guarda el segundo valor como el numero total de bytes a cargar
					$bytesTotales = valueObjects[1];
				}
			}
			
			// se dispara  la señal
			super.dispatch(this);
		}
	}
}