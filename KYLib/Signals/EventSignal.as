﻿/*
 * Copyright (C) 2019 Juan Pablo Calle
 *
 * Este software se proporciona 'tal cual', sin ninguna garantía expresa o implícita.
 * En ningún caso, los autores serán responsables de los daños que se deriven del
 * uso de este software.
 *
 * Se concede permiso a cualquier persona para utilizar este software para
 * cualquier propósito, incluidas las aplicaciones comerciales, y para modificarlo y
 * redistribuirlo libremente, sujeto a las siguientes restricciones:
 *
 * 1. El origen de este software no debe ser tergiversado; No debes reclamar
 *    que escribiste el software original. Si utiliza este software en un producto,
 *    se agradecería un reconocimiento en la documentación del producto, pero no
 *    es obligatorio.
 * 2. Las versiones fuente modificadas deben estar claramente marcadas como
 *     tales y no deben tergiversarse como si fueran el software original.
 * 3. Este aviso no puede ser eliminado ni alterado de ninguna distribución.
 */
 
package KYLib.Signals 
{
		import org.osflash.signals.Signal;
	
	
	//--------------------------------------
	//  Descripcion de la clase
	//--------------------------------------
	/**
	 * La clase EventSignal es una señal comun y corriente solo que no tiene valueClases y en las funciones agregadas se debe esperar un solo parametros, del cual es de tipo EventSignal o de alguna de sus clases heredadas dependiendo de la señal trabajada, esto significa que las funciones deben ser iguales a las que se usan con los controladores de eventos normales de flash.
	 *
	 * @langversion 3.0
	 *
	 * @playerversion Flash 12
	 *  @playerversion AIR 30
	 *
	 * @productversion Flash CS6
	 * @productversion Animate CC
	 *
	 * @author Juan Pablo Calle
	 */
	public dynamic class EventSignal extends Signal 
	{
		
		/**
		 * Crea una nueva EventSignal.
		 * 
		 * @param Contenedor Este es el objeto que contiene la señal, al momento de crear una señl de este tipo hagalo de esta forma: <code>new EventSignal(this)</code>.
		 */
		public function EventSignal(Contenedor:Object)
		{
			super();
			$target = Contenedor;
		}
		
		/**
		 * Obtiene al objeto contenedor de la señal, es decir, el objeto que esta disparando las señales.
		 */
		public function get target():Object 
		{
			return $target;
		}
		private var $target:Object;
		
		/**
		 * Dispara la señal.
		 * 
		 * @param ...valueObjects Estos parametros son ingnorados.
		 */
		override public function dispatch(...valueObjects):void 
		{
			super.dispatch(this);
		}
		
		/**
		 * Un objeto almacenado que guarda alguna informacion referente a la señal lanzada, puede ser cualquier tipo de dato.
		 */
		public var data:Object;
		
	}
}